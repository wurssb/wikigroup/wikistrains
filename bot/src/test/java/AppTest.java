import nl.munlock.wiki.App;
import org.junit.Test;

public class AppTest {

    @Test
    public void genome() throws Exception {
        String[] args = {
                "-wiki", "http://nvme1.wurnet.nl/w/api.php",
                "-username", "admin",
                "-password", "change-this-password",
                "-sync",
                "-genome",
//                "-taxonomy",
//                "-bacdive",
                "-e", "http://nvme1.wurnet.nl:8834/proxy/wdqs/bigdata/namespace/wdq/sparql"};
        while (true) {
            try {
                App.main(args);
                break;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void sync() throws Exception {
        String[] args = {
                "-wiki", "http://localhost:8181/w/api.php",
                "-username", "WikibaseAdmin",
                "-password", "WikibaseDockerAdminPass",
                "-sync",
                "-e", "http://localhost:8282/proxy/wdqs/bigdata/namespace/wdq/sparql"};
        App.main(args);
    }
}
