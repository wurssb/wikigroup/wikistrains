import nl.munlock.wiki.App;
import org.junit.Test;

public class BacDiveTest {
    @Test
    public void bacdive() throws Exception {
        String[] args = {
                "-wiki", "http://nvme1.wurnet.nl/w/api.php",
                "-username", "admin",
                "-password", "change-this-password",
                "-endpoint", "http://nvme1.wurnet.nl:8834/proxy/wdqs/bigdata/namespace/wdq/sparql",
                "-bacdive",
//                "-sync",
        };
        App.main(args);
    }
}
