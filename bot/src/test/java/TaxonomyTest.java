import nl.munlock.wiki.App;
import org.junit.Test;

public class TaxonomyTest {
    @Test
    public void taxonomy() throws Exception {
        String[] args = {
                "-wiki", "http://nvme1.wurnet.nl/w/api.php",
                "-username", "admin",
                "-password", "change-this-password",
                "-sync",
//                "-genome",
                "-taxonomy",
//                "-bacdive",
                "-e", "http://nvme1.wurnet.nl:8834/proxy/wdqs/bigdata/namespace/wdq/sparql"};
        while (true) {
            try {
                App.main(args);
                break;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
