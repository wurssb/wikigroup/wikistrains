package nl.munlock.wiki;

import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

public class Generic {
    private static Logger logger = Logger.getLogger(Generic.class);

    public static void downloadURL(String url, String path) {
        logger.debug(url + " to " + path);
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream());
             FileOutputStream fileOS = new FileOutputStream(path)) {
            byte data[] = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
        } catch (IOException e) {
            // handles IO exceptions
            logger.info("Exception in download: " + e.getMessage());
        }
    }
}
