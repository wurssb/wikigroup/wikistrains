package nl.munlock.wiki;

import static nl.munlock.wiki.sync.genetics.GenomeSync.genomeLookup;
import static nl.munlock.wiki.sync.genetics.GenomeSync.getGenomes;

public class PhenotypeSync {
    public PhenotypeSync(CommandOptions commandOptions) {
        // Get genomes
        if (genomeLookup == null) {
            getGenomes();
        }
    }
}
