package nl.munlock.wiki;

import nl.munlock.wiki.sync.bacdive.BacDiveSync;
import nl.munlock.wiki.sync.genetics.GenomeSync;
import nl.munlock.wiki.sync.wikibase.Synchronise;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikidata.wdtk.wikibaseapi.BasicApiConnection;
import org.wikidata.wdtk.wikibaseapi.WbEditingAction;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataEditor;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;

import static nl.munlock.wiki.iRODS.connect;
import static nl.munlock.wiki.wikibase.General.getLocalItems;
import static nl.munlock.wiki.wikibase.General.getWikiBaseProperties;

/**
 * Project initializer which converts a metadata excel sheet obtained from
 *
 */
public class App {
    public static BasicApiConnection basicApiConnection;
    public static WbEditingAction wbea;
    public static String PREFIX = ""; // +
//            "PREFIX wd:<http://strain.wiki.opencura.com/entity/>\n" +
//            "PREFIX wdt:<http://strain.wiki.opencura.com/prop/direct/>\n";
    private static Logger logger = LoggerFactory.getLogger(App.class);
    public static CommandOptions commandOptions;
    public static WikibaseDataEditor wbde;
    public static WikibaseDataFetcher wbdf;

    public static void main(String[] args) throws Exception {
        // iRODS unlock connection
        connect("unlock-icat.irods.surfsara.nl", 1247, "anonymous", "", "unlock");
        // command options parsing
        commandOptions = new CommandOptions(args);

        // Logger test
        org.apache.log4j.Logger.getLogger("org").setLevel(Level.OFF);

        // Wikibase connection
        basicApiConnection = new BasicApiConnection(commandOptions.wiki);
        basicApiConnection.login(commandOptions.username, commandOptions.password);
        wbde = new WikibaseDataEditor(App.basicApiConnection, App.commandOptions.wiki);
        wbdf = new WikibaseDataFetcher(App.basicApiConnection, App.commandOptions.wiki);
        wbea = new WbEditingAction(App.basicApiConnection, App.commandOptions.wiki);
        wbde.setEditAsBot(true);
        wbde.setAverageTimePerEdit(1);

        basicApiConnection.checkCredentials();

        // Obtain wikibase content
        getWikiBaseProperties(commandOptions.endpoint);
        getLocalItems(commandOptions.endpoint);

        // Synchronise wikibase properties
        if (commandOptions.sync) {
            Synchronise.sync(commandOptions);
        }

        // Synchronise genome pages
        if (commandOptions.genome) {
             new GenomeSync(commandOptions);
        }

        if (commandOptions.bacdive) {
             new BacDiveSync(commandOptions);
//            new PhenotypeSync(commandOptions);
        }


//        EditOnlineDataExample.main(commandOptions);
//        BasicConfigurator.configure();
//        PropertyConfigurator.configure("src/main/resources/log4j.properties");
//
//        logger.info("INFO");
//        logger.debug("DEBUG");
//        CommandOptions commandOptions = new CommandOptions(args);
//
//        CreateRemoteRepository.main(commandOptions);
//        HashSet<String> rdfFiles = Download.findRDFFiles(commandOptions);
//        Load.rdf(commandOptions, rdfFiles);
    }
}
