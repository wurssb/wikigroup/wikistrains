package nl.munlock.wiki;

import org.wikidata.wdtk.datamodel.helpers.Datamodel;
import org.wikidata.wdtk.datamodel.helpers.ReferenceBuilder;
import org.wikidata.wdtk.datamodel.helpers.StatementBuilder;
import org.wikidata.wdtk.datamodel.interfaces.*;

import java.math.BigDecimal;

import static nl.munlock.wiki.wikibase.General.propertyLookup;

public class Statements {
    public static Statement create(ItemIdValue itemIdValue, String propertyID, BigDecimal value) {
        // Get property value of genome size
        PropertyIdValue genomeSizeProperty = Datamodel.makePropertyIdValue(propertyLookup.get(propertyID).replaceAll(".*/", ""), App.commandOptions.wiki);
        // Set value of genome size to a quantity
        QuantityValue genomeSizeValue = Datamodel.makeQuantityValue(value);
        // Create the reference
//        PropertyIdValue statedIn = Datamodel.makePropertyIdValue(propertyLookup.get("stated in").replaceAll(".*/", ""), App.commandOptions.wiki);
//        Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(referencePage, App.commandOptions.wiki)).build();
        // Create the genome size statement
        Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, genomeSizeProperty).withValue(genomeSizeValue).build();
        // Return the statement
        return statement;
    }

    public static Statement create(ItemIdValue itemIdValue, String propertyID, BigDecimal value, String referencePage) {
        // Get property value of genome size
        PropertyIdValue genomeSizeProperty = Datamodel.makePropertyIdValue(propertyLookup.get(propertyID).replaceAll(".*/", ""), App.commandOptions.wiki);
        // Set value of genome size to a quantity
        QuantityValue genomeSizeValue = Datamodel.makeQuantityValue(value);
        // Create the reference
        String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
        PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
        Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(referencePage, App.commandOptions.wiki)).build();
        // Create the genome size statement with reference
        Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, genomeSizeProperty).withValue(genomeSizeValue).withReference(reference).build(); // .withReference(reference)

        // Return the statement
        return statement;
    }

    public static Statement create(ItemIdValue itemIdValue, String propertyID, String value, String referencePage) {
        // Get property value
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get(propertyID).replaceAll(".*/", ""), App.commandOptions.wiki);
        // Set string to string value
        if (value.matches("Q[0-9]+")) {
            ItemIdValue itemIdValueTarget = Datamodel.makeItemIdValue(value, App.commandOptions.wiki);
            // Create the reference
            String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
            PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
            Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(referencePage, App.commandOptions.wiki)).build();
            // Create the genome size statement with reference
            Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, propertyIdValue).withValue(itemIdValueTarget).withReference(reference).build();
            // Return the statement
            return statement;
        } else {
            StringValue stringValue = Datamodel.makeStringValue(value);
            // Create the reference
            String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
            PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
            Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(referencePage, App.commandOptions.wiki)).build();
            // Create the genome size statement with reference
            Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, propertyIdValue).withValue(stringValue).withReference(reference).build();
            // Return the statement
            return statement;
        }
    }
}
