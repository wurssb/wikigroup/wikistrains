package nl.munlock.wiki;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class ExecCommand {
    private Semaphore outputSem;
    private String output;
    private Semaphore errorSem;
    private String error;
    private int exit;
    private Process p;
    private boolean printOutput;

    public ExecCommand(String command, String input) {
        try {
            this.p = Runtime.getRuntime().exec(this.makeArray(command));
            (new ExecCommand.InputWriter(input)).start();
            (new ExecCommand.OutputReader()).start();
            (new ExecCommand.ErrorReader()).start();
            this.p.waitFor();
        } catch (IOException var4) {
            var4.printStackTrace();
        } catch (InterruptedException var5) {
            var5.printStackTrace();
        }

    }

    public ExecCommand(String command) {
        this(command, true);
    }

    public ExecCommand(String command, boolean printOutput) {
        try {
            this.p = Runtime.getRuntime().exec(this.makeArray(command));
            (new ExecCommand.OutputReader()).start();
            (new ExecCommand.ErrorReader()).start();
            this.p.waitFor();
        } catch (IOException var4) {
            var4.printStackTrace();
        } catch (InterruptedException var5) {
            var5.printStackTrace();
        }

        this.exit = this.p.exitValue();
    }

    public String getOutput() {
        try {
            this.outputSem.acquire();
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }

        String value = this.output;
        this.outputSem.release();
        return value;
    }

    public String getError() {
        try {
            this.errorSem.acquire();
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }

        String value = this.error;
        this.errorSem.release();
        return value;
    }

    public int getExit() {
        return this.exit;
    }

    private String[] makeArray(String command) {
        ArrayList<String> commandArray = new ArrayList();
        String buff = "";
        boolean lookForEnd = false;

        for(int i = 0; i < command.length(); ++i) {
            if (lookForEnd) {
                if (command.charAt(i) == '"') {
                    if (buff.length() > 0) {
                        commandArray.add(buff);
                    }

                    buff = "";
                    lookForEnd = false;
                } else {
                    buff = buff + command.charAt(i);
                }
            } else if (command.charAt(i) == '"') {
                lookForEnd = true;
            } else if (command.charAt(i) == ' ') {
                if (buff.length() > 0) {
                    commandArray.add(buff);
                }

                buff = "";
            } else {
                buff = buff + command.charAt(i);
            }
        }

        if (buff.length() > 0) {
            commandArray.add(buff);
        }

        String[] array = new String[commandArray.size()];

        for(int i = 0; i < commandArray.size(); ++i) {
            array[i] = (String)commandArray.get(i);
        }

        return array;
    }

    private class ErrorReader extends Thread {
        public ErrorReader() {
            try {
                ExecCommand.this.errorSem = new Semaphore(1);
                ExecCommand.this.errorSem.acquire();
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }

        }

        public void run() {
            try {
                StringBuffer readBuffer = new StringBuffer();
                BufferedReader isr = new BufferedReader(new InputStreamReader(ExecCommand.this.p.getErrorStream()));
                new String();

                String buff;
                while((buff = isr.readLine()) != null) {
                    readBuffer.append(buff);
                }

                ExecCommand.this.error = readBuffer.toString();
                ExecCommand.this.errorSem.release();
            } catch (IOException var4) {
                var4.printStackTrace();
            }

            if (ExecCommand.this.error.length() > 0) {
                System.out.println(ExecCommand.this.error);
            }

        }
    }

    private class OutputReader extends Thread {
        public OutputReader() {
            try {
                ExecCommand.this.outputSem = new Semaphore(1);
                ExecCommand.this.outputSem.acquire();
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }

        }

        public void run() {
            try {
                StringBuffer readBuffer = new StringBuffer();
                BufferedReader isr = new BufferedReader(new InputStreamReader(ExecCommand.this.p.getInputStream()));
                new String();

                String buff;
                while((buff = isr.readLine()) != null) {
                    readBuffer.append(buff);
                    if (ExecCommand.this.printOutput) {
                        System.out.println(buff);
                    }
                }

                ExecCommand.this.output = readBuffer.toString();
                ExecCommand.this.outputSem.release();
            } catch (IOException var4) {
                var4.printStackTrace();
            }

        }
    }

    private class InputWriter extends Thread {
        private String input;

        public InputWriter(String input) {
            this.input = input;
        }

        public void run() {
            PrintWriter pw = new PrintWriter(ExecCommand.this.p.getOutputStream());
            pw.println(this.input);
            pw.flush();
        }
    }
}
