package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaboliteProduction{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("Chebi-ID")
    public int getChebiID() {
        return this.chebiID; }
    public void setChebiID(int chebiID) {
        this.chebiID = chebiID; }
    int chebiID;
    @JsonProperty("metabolite")
    public String getMetabolite() {
        return this.metabolite; }
    public void setMetabolite(String metabolite) {
        this.metabolite = metabolite; }
    String metabolite;
    @JsonProperty("production")
    public String getProduction() {
        return this.production; }
    public void setProduction(String production) {
        this.production = production; }
    String production;

    @JsonProperty("excretion")
    public String getExcretion() {
        return this.excretion; }
    public void setExcretion(String excretion) {
        this.excretion = excretion; }
    String excretion;

}

