package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APILIST{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("DIM")
    public String getDIM() {
        return this.dIM; }
    public void setDIM(String dIM) {
        this.dIM = dIM; }
    String dIM;
    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC; }
    public void setESC(String eSC) {
        this.eSC = eSC; }
    String eSC;
    @JsonProperty("alpha MAN")
    public String getAlphaMAN() {
        return this.alphaMAN; }
    public void setAlphaMAN(String alphaMAN) {
        this.alphaMAN = alphaMAN; }
    String alphaMAN;
    @JsonProperty("DARL")
    public String getDARL() {
        return this.dARL; }
    public void setDARL(String dARL) {
        this.dARL = dARL; }
    String dARL;
    @JsonProperty("XYL")
    public String getXYL() {
        return this.xYL; }
    public void setXYL(String xYL) {
        this.xYL = xYL; }
    String xYL;
    @JsonProperty("RHA")
    public String getRHA() {
        return this.rHA; }
    public void setRHA(String rHA) {
        this.rHA = rHA; }
    String rHA;
    @JsonProperty("MDG")
    public String getMDG() {
        return this.mDG; }
    public void setMDG(String mDG) {
        this.mDG = mDG; }
    String mDG;
    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB; }
    public void setRIB(String rIB) {
        this.rIB = rIB; }
    String rIB;
    @JsonProperty("G1P")
    public String getG1P() {
        return this.g1P; }
    public void setG1P(String g1P) {
        this.g1P = g1P; }
    String g1P;
    @JsonProperty("TAG")
    public String getTAG() {
        return this.tAG; }
    public void setTAG(String tAG) {
        this.tAG = tAG; }
    String tAG;
    @JsonProperty("beta HEM")
    public String getBetaHEM() {
        return this.betaHEM; }
    public void setBetaHEM(String betaHEM) {
        this.betaHEM = betaHEM; }
    String betaHEM;
}