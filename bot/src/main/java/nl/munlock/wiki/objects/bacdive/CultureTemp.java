package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CultureTemp{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("growth") 
    public String getGrowth() { 
		 return this.growth; } 
    public void setGrowth(String growth) { 
		 this.growth = growth; } 
    String growth;
    @JsonProperty("type") 
    public String getType() { 
		 return this.type; } 
    public void setType(String type) { 
		 this.type = type; } 
    String type;
    @JsonProperty("temperature") 
    public String getTemperature() { 
		 return this.temperature; } 
    public void setTemperature(String temperature) { 
		 this.temperature = temperature; } 
    String temperature;
    @JsonProperty("range") 
    public String getRange() { 
		 return this.range; } 
    public void setRange(String range) { 
		 this.range = range; } 
    String range;
}
