package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class IDENTIFIERS{
    @JsonProperty("PRIMARY_ID")
    public String getPRIMARY_ID() { 
		 return this.pRIMARY_ID; } 
    public void setPRIMARY_ID(String pRIMARY_ID) { 
		 this.pRIMARY_ID = pRIMARY_ID; } 
    String pRIMARY_ID;
    @JsonProperty("SUBMITTER_ID") 
    public SUBMITTERID getSUBMITTER_ID() { 
		 return this.sUBMITTER_ID; } 
    public void setSUBMITTER_ID(SUBMITTERID sUBMITTER_ID) { 
		 this.sUBMITTER_ID = sUBMITTER_ID; } 
    SUBMITTERID sUBMITTER_ID;
    @JsonProperty("SECONDARY_ID")
    public List<String> getSECONDARY_ID() {
        return this.sECONDARY_ID; }
    public void setSECONDARY_ID(List<String> sECONDARY_ID) {
        this.sECONDARY_ID = sECONDARY_ID; }
    List<String> sECONDARY_ID;
}
