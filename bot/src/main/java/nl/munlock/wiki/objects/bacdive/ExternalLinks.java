package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ExternalLinks{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("culture collection no.") 
    public String getCultureCollectionNo() { 
		 return this.cultureCollectionNo; } 
    public void setCultureCollectionNo(String cultureCollectionNo) { 
		 this.cultureCollectionNo = cultureCollectionNo; } 
    String cultureCollectionNo;
    @JsonProperty("straininfo link") 
    public List<StraininfoLink> getStraininfoLink() {
		 return this.straininfoLink; } 
    public void setStraininfoLink(List<StraininfoLink> straininfoLink) { 
		 this.straininfoLink = straininfoLink; } 
    List<StraininfoLink> straininfoLink;
}
