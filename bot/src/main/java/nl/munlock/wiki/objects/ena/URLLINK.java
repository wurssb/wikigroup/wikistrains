package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class URLLINK{
    @JsonProperty("LABEL")
    public String getLABEL() { 
		 return this.lABEL; } 
    public void setLABEL(String lABEL) { 
		 this.lABEL = lABEL; } 
    String lABEL;
    @JsonProperty("URL") 
    public String getURL() { 
		 return this.uRL; } 
    public void setURL(String uRL) { 
		 this.uRL = uRL; } 
    String uRL;
}
