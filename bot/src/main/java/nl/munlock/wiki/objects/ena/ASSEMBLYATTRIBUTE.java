package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ASSEMBLYATTRIBUTE{
    @JsonProperty("TAG")
    public String getTAG() { 
		 return this.tAG; } 
    public void setTAG(String tAG) { 
		 this.tAG = tAG; } 
    String tAG;
    @JsonProperty("VALUE") 
    public Object getVALUE() { 
		 return this.vALUE; } 
    public void setVALUE(Object vALUE) { 
		 this.vALUE = vALUE; } 
    Object vALUE;
}
