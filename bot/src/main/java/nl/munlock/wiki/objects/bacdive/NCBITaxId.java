package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NCBITaxId {
    @JsonProperty("NCBI tax id")
    public int getNCBITaxId() {
        return this.nCBITaxId;
    }

    public void setNCBITaxId(int nCBITaxId) {
        this.nCBITaxId = nCBITaxId;
    }

    int nCBITaxId;

    @JsonProperty("Matching level")
    public String getMatchingLevel() {
        return this.matchingLevel;
    }

    public void setMatchingLevel(String matchingLevel) {
        this.matchingLevel = matchingLevel;
    }

    String matchingLevel;
}
