package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SequenceInformation{
    @JsonProperty("16S sequences")
    public List<_16SSequences> get_16SSequences() { 
		 return this._16SSequences; } 
    public void set_16SSequences(List<_16SSequences> _16SSequences) { 
		 this._16SSequences = _16SSequences; } 
    List<_16SSequences> _16SSequences = new ArrayList<>();
    @JsonProperty("Genome sequences") 
    public List<GenomeSequence> getGenomeSequences() {
		 return this.genomeSequences; } 
    public void setGenomeSequences(List<GenomeSequence> genomeSequences) { 
		 this.genomeSequences = genomeSequences; } 
    List<GenomeSequence> genomeSequences = new ArrayList<>();
    @JsonProperty("GC content")
    public List<GCContent> getGCContent() {
        return this.gCContent; }
    public void setGCContent(List<GCContent> gCContent) {
        this.gCContent = gCContent; }
    List<GCContent> gCContent;


}
