package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Reference{
    @JsonProperty("@id")
    public int getId() { 
		 return this.id; } 
    public void setId(int id) { 
		 this.id = id; } 
    int id;
    @JsonProperty("authors") 
    public String getAuthors() { 
		 return this.authors; } 
    public void setAuthors(String authors) { 
		 this.authors = authors; } 
    String authors;
    @JsonProperty("catalogue") 
    public String getCatalogue() { 
		 return this.catalogue; } 
    public void setCatalogue(String catalogue) { 
		 this.catalogue = catalogue; } 
    String catalogue;
    @JsonProperty("doi/url") 
    public String getDoiUrl() { 
		 return this.doiUrl; } 
    public void setDoiUrl(String doiUrl) { 
		 this.doiUrl = doiUrl; } 
    String doiUrl;
    @JsonProperty("title") 
    public String getTitle() { 
		 return this.title; } 
    public void setTitle(String title) { 
		 this.title = title; } 
    String title;
    @JsonProperty("journal") 
    public String getJournal() { 
		 return this.journal; } 
    public void setJournal(String journal) { 
		 this.journal = journal; } 
    String journal;
    @JsonProperty("pubmed") 
    public int getPubmed() { 
		 return this.pubmed; } 
    public void setPubmed(int pubmed) { 
		 this.pubmed = pubmed; } 
    int pubmed;

    @JsonProperty("ID_cross_reference")
    public int getID_cross_reference() {
        return this.iD_cross_reference; }
    public void setID_cross_reference(int iD_cross_reference) {
        this.iD_cross_reference = iD_cross_reference; }
    int iD_cross_reference;

}
