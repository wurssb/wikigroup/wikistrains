package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiskAssessment{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("pathogenicity animal")
    public String getPathogenicityAnimal() {
        return this.pathogenicityAnimal; }
    public void setPathogenicityAnimal(String pathogenicityAnimal) {
        this.pathogenicityAnimal = pathogenicityAnimal; }
    String pathogenicityAnimal;
    @JsonProperty("pathogenicity plant")
    public String getPathogenicityPlant() {
        return this.pathogenicityPlant; }
    public void setPathogenicityPlant(String pathogenicityPlant) {
        this.pathogenicityPlant = pathogenicityPlant; }
    String pathogenicityPlant;

    @JsonProperty("biosafety level")
    public String getBiosafetyLevel() {
        return this.biosafetyLevel; }
    public void setBiosafetyLevel(String biosafetyLevel) {
        this.biosafetyLevel = biosafetyLevel; }
    String biosafetyLevel;
    @JsonProperty("biosafety level comment")
    public String getBiosafetyLevelComment() {
        return this.biosafetyLevelComment; }
    public void setBiosafetyLevelComment(String biosafetyLevelComment) {
        this.biosafetyLevelComment = biosafetyLevelComment; }
    String biosafetyLevelComment;

    @JsonProperty("pathogenicity human")
    public String getPathogenicityHuman() {
        return this.pathogenicityHuman; }
    public void setPathogenicityHuman(String pathogenicityHuman) {
        this.pathogenicityHuman = pathogenicityHuman; }
    String pathogenicityHuman;

}




