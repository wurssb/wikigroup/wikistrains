package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class STUDYREF{
    @JsonProperty("IDENTIFIERS")
    public IDENTIFIERS getIDENTIFIERS() {
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) { 
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
}
