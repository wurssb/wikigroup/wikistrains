package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CultureMedium{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("name") 
    public String getName() { 
		 return this.name; } 
    public void setName(String name) { 
		 this.name = name; } 
    String name;
    @JsonProperty("growth") 
    public String getGrowth() { 
		 return this.growth; } 
    public void setGrowth(String growth) { 
		 this.growth = growth; } 
    String growth;
    @JsonProperty("composition") 
    public String getComposition() { 
		 return this.composition; } 
    public void setComposition(String composition) { 
		 this.composition = composition; } 
    String composition;
    @JsonProperty("link") 
    public String getLink() { 
		 return this.link; } 
    public void setLink(String link) { 
		 this.link = link; } 
    String link;
}
