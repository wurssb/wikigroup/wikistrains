package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaboliteUtilization{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("Chebi-ID")
    public int getChebiID() {
        return this.chebiID; }
    public void setChebiID(int chebiID) {
        this.chebiID = chebiID; }
    int chebiID;
    @JsonProperty("metabolite")
    public String getMetabolite() {
        return this.metabolite; }
    public void setMetabolite(String metabolite) {
        this.metabolite = metabolite; }
    String metabolite;
    @JsonProperty("utilization activity")
    public String getUtilizationActivity() {
        return this.utilizationActivity; }
    public void setUtilizationActivity(String utilizationActivity) {
        this.utilizationActivity = utilizationActivity; }
    String utilizationActivity;
    @JsonProperty("kind of utilization tested")
    public String getKindOfUtilizationTested() {
        return this.kindOfUtilizationTested; }
    public void setKindOfUtilizationTested(String kindOfUtilizationTested) {
        this.kindOfUtilizationTested = kindOfUtilizationTested; }
    String kindOfUtilizationTested;


}

