package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pigmentation {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }
    public void setRef(int ref) {
        this.ref = ref;
    }
    int ref;

    @JsonProperty("production")
    public String getProduction() {
        return this.production;
    }
    public void setProduction(String production) {
        this.production = production;
    }
    String production;

    @JsonProperty("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;

    @JsonProperty("color")
    public String getColor() {
        return this.color; }
    public void setColor(String color) {
        this.color = color; }
    String color;

}
