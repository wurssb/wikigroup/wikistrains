package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SporeFormation {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("spore formation")
    public List<String> getSporeFormation() {
        return this.sporeFormation;
    }

    public void setSporeFormation(List<String> sporeFormation) {
        this.sporeFormation = sporeFormation;
    }

    List<String> sporeFormation;

    @JsonProperty("type of spore")
    public String getTypeOfSpore() {
        return this.typeOfSpore; }
    public void setTypeOfSpore(String typeOfSpore) {
        this.typeOfSpore = typeOfSpore; }
    String typeOfSpore;

    @JsonProperty("spore description")
    public String getSporeDescription() {
        return this.sporeDescription; }
    public void setSporeDescription(String sporeDescription) {
        this.sporeDescription = sporeDescription; }
    String sporeDescription;

}
