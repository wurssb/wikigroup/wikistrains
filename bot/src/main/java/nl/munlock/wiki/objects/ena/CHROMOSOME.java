package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CHROMOSOME{
    @JsonProperty("accession")
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("TYPE") 
    public String getTYPE() { 
		 return this.tYPE; } 
    public void setTYPE(String tYPE) { 
		 this.tYPE = tYPE; } 
    String tYPE;
    @JsonProperty("NAME") 
    public Object getNAME() { 
		 return this.nAME; } 
    public void setNAME(Object nAME) { 
		 this.nAME = nAME; } 
    Object nAME;
}
