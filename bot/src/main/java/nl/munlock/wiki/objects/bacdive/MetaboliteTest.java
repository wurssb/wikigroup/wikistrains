package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetaboliteTest{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("Chebi-ID")
    public int getChebiID() {
        return this.chebiID; }
    public void setChebiID(int chebiID) {
        this.chebiID = chebiID; }
    int chebiID;
    @JsonProperty("metabolite")
    public String getMetabolite() {
        return this.metabolite; }
    public void setMetabolite(String metabolite) {
        this.metabolite = metabolite; }
    String metabolite;
    @JsonProperty("voges-proskauer-test")
    public String getVogesProskauerTest() {
        return this.vogesProskauerTest; }
    public void setVogesProskauerTest(String vogesProskauerTest) {
        this.vogesProskauerTest = vogesProskauerTest; }
    String vogesProskauerTest;
    @JsonProperty("methylred-test")
    public String getMethylredTest() {
        return this.methylredTest; }
    public void setMethylredTest(String methylredTest) {
        this.methylredTest = methylredTest; }
    String methylredTest;

    @JsonProperty("indole test")
    public String getIndoleTest() {
        return this.indoleTest; }
    public void setIndoleTest(String indoleTest) {
        this.indoleTest = indoleTest; }
    String indoleTest;

    @JsonProperty("citrate test")
    public String getCitrateTest() {
        return this.citrateTest; }
    public void setCitrateTest(String citrateTest) {
        this.citrateTest = citrateTest; }
    String citrateTest;

}