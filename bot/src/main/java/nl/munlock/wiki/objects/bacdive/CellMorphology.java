package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CellMorphology{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("gram stain")
    public String getGramStain() {
        return this.gramStain; }
    public void setGramStain(String gramStain) {
        this.gramStain = gramStain; }
    String gramStain;
    @JsonProperty("cell length")
    public String getCellLength() {
        return this.cellLength; }
    public void setCellLength(String cellLength) {
        this.cellLength = cellLength; }
    String cellLength;
    @JsonProperty("cell width")
    public String getCellWidth() {
        return this.cellWidth; }
    public void setCellWidth(String cellWidth) {
        this.cellWidth = cellWidth; }
    String cellWidth;
    @JsonProperty("cell shape")
    public String getCellShape() {
        return this.cellShape; }
    public void setCellShape(String cellShape) {
        this.cellShape = cellShape; }
    String cellShape;
    @JsonProperty("motility")
    public String getMotility() {
        return this.motility; }
    public void setMotility(String motility) {
        this.motility = motility; }
    String motility;
    @JsonProperty("flagellum arrangement")
    public String getFlagellumArrangement() {
        return this.flagellumArrangement; }
    public void setFlagellumArrangement(String flagellumArrangement) {
        this.flagellumArrangement = flagellumArrangement; }
    String flagellumArrangement;

}