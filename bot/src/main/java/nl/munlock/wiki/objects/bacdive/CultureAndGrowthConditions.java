package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CultureAndGrowthConditions {
    @JsonProperty("culture medium")
    public List<CultureMedium> getCultureMedium() {
        return this.cultureMedium;
    }

    // @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public void setCultureMedium(List<CultureMedium> cultureMedium) {
        this.cultureMedium = cultureMedium;
    }

//    @JsonIgnore
//    public void setCultureMedium(CultureMedium cultureMedium) {
//        this.cultureMedium.add(cultureMedium);
//    }


    List<CultureMedium> cultureMedium = new ArrayList<>();

    @JsonProperty("culture temp")
    public List<CultureTemp> getCultureTemp() {
        return this.cultureTemp;
    }

    public void setCultureTemp(List<CultureTemp> cultureTemp) {
        this.cultureTemp = cultureTemp;
    }

    List<CultureTemp> cultureTemp = new ArrayList<>();

    @JsonProperty("culture pH")
    public List<CulturePH> getCulturePH() {
        return this.culturePH; }
    public void setCulturePH(List<CulturePH> culturePH) {
        this.culturePH = culturePH; }
    List<CulturePH> culturePH = new ArrayList<>();

}
