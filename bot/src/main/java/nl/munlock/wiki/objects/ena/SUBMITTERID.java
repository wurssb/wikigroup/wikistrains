package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SUBMITTERID{
    @JsonProperty("namespace")
    public String getNamespace() { 
		 return this.namespace; } 
    public void setNamespace(String namespace) { 
		 this.namespace = namespace; } 
    String namespace;
    @JsonProperty("content") 
    public String getContent() { 
		 return this.content; } 
    public void setContent(String content) { 
		 this.content = content; } 
    String content;
}
