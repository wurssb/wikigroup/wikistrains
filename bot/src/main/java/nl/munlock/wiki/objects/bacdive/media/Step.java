package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Step {
    @JsonProperty("step")
    public String getStep() {
        return this.step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    String step;

    @JsonProperty("recipe No.")
    public int getRecipeNo() {
        return this.recipeNo; }
    public void setRecipeNo(int recipeNo) {
        this.recipeNo = recipeNo; }
    int recipeNo;

}
