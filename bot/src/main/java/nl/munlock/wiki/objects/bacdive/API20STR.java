package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class API20STR{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("VP")
    public String getVP() {
        return this.vP; }
    public void setVP(String vP) {
        this.vP = vP; }
    String vP;
    @JsonProperty("HIP")
    public String getHIP() {
        return this.hIP; }
    public void setHIP(String hIP) {
        this.hIP = hIP; }
    String hIP;
    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC; }
    public void setESC(String eSC) {
        this.eSC = eSC; }
    String eSC;
    @JsonProperty("PYRA")
    public String getPYRA() {
        return this.pYRA; }
    public void setPYRA(String pYRA) {
        this.pYRA = pYRA; }
    String pYRA;
    @JsonProperty("alpha GAL")
    public String getAlphaGAL() {
        return this.alphaGAL; }
    public void setAlphaGAL(String alphaGAL) {
        this.alphaGAL = alphaGAL; }
    String alphaGAL;
    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR; }
    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR; }
    String betaGUR;
    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL; }
    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL; }
    String betaGAL;
    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL; }
    public void setPAL(String pAL) {
        this.pAL = pAL; }
    String pAL;
    @JsonProperty("LAP")
    public String getLAP() {
        return this.lAP; }
    public void setLAP(String lAP) {
        this.lAP = lAP; }
    String lAP;
    @JsonProperty("ADH")
    public String getADH() {
        return this.aDH; }
    public void setADH(String aDH) {
        this.aDH = aDH; }
    String aDH;
    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB; }
    public void setRIB(String rIB) {
        this.rIB = rIB; }
    String rIB;
    @JsonProperty("ARA")
    public String getARA() {
        return this.aRA; }
    public void setARA(String aRA) {
        this.aRA = aRA; }
    String aRA;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("SOR")
    public String getSOR() {
        return this.sOR; }
    public void setSOR(String sOR) {
        this.sOR = sOR; }
    String sOR;
    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC; }
    public void setLAC(String lAC) {
        this.lAC = lAC; }
    String lAC;
    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE; }
    public void setTRE(String tRE) {
        this.tRE = tRE; }
    String tRE;
    @JsonProperty("INU")
    public String getINU() {
        return this.iNU; }
    public void setINU(String iNU) {
        this.iNU = iNU; }
    String iNU;
    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF; }
    public void setRAF(String rAF) {
        this.rAF = rAF; }
    String rAF;
    @JsonProperty("AMD")
    public String getAMD() {
        return this.aMD; }
    public void setAMD(String aMD) {
        this.aMD = aMD; }
    String aMD;
    @JsonProperty("GLYG")
    public String getGLYG() {
        return this.gLYG; }
    public void setGLYG(String gLYG) {
        this.gLYG = gLYG; }
    String gLYG;
    @JsonProperty("beta HEM")
    public String getBetaHEM() {
        return this.betaHEM; }
    public void setBetaHEM(String betaHEM) {
        this.betaHEM = betaHEM; }
    String betaHEM;
}