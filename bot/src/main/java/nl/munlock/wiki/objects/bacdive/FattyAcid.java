package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FattyAcid{

    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("fatty acid")
    public String getFattyAcid() {
        return this.fattyAcid; }
    public void setFattyAcid(String fattyAcid) {
        this.fattyAcid = fattyAcid; }
    String fattyAcid;
    @JsonProperty("percentage")
    public double getPercentage() {
        return this.percentage; }
    public void setPercentage(double percentage) {
        this.percentage = percentage; }
    double percentage;
    @JsonProperty("ECL")
    public double getECL() {
        return this.eCL; }
    public void setECL(double eCL) {
        this.eCL = eCL; }
    double eCL;

    @JsonProperty("SD")
    public double getSD() {
        return this.sD; }
    public void setSD(double sD) {
        this.sD = sD; }
    double sD;

}
