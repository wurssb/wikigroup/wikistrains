package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CHROMOSOMES{
    @JsonProperty("CHROMOSOME")
    public List<CHROMOSOME> getCHROMOSOME() {
		 return this.cHROMOSOME; } 
    public void setCHROMOSOME(List<CHROMOSOME> cHROMOSOME) { 
		 this.cHROMOSOME = cHROMOSOME; } 
    List<CHROMOSOME> cHROMOSOME;
}
