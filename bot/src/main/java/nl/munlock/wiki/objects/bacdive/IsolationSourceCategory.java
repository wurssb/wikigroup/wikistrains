package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IsolationSourceCategory {
    @JsonProperty("Cat1")
    public String getCat1() {
        return this.cat1;
    }

    public void setCat1(String cat1) {
        this.cat1 = cat1;
    }

    String cat1;

    @JsonProperty("Cat2")
    public String getCat2() {
        return this.cat2;
    }

    public void setCat2(String cat2) {
        this.cat2 = cat2;
    }

    String cat2;

    @JsonProperty("Cat3")
    public String getCat3() {
        return this.cat3;
    }

    public void setCat3(String cat3) {
        this.cat3 = cat3;
    }

    String cat3;

}
