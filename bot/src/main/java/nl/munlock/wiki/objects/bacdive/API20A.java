package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class API20A{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("IND")
    public String getIND() {
        return this.iND; }
    public void setIND(String iND) {
        this.iND = iND; }
    String iND;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC; }
    public void setLAC(String lAC) {
        this.lAC = lAC; }
    String lAC;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("SAL")
    public String getSAL() {
        return this.sAL; }
    public void setSAL(String sAL) {
        this.sAL = sAL; }
    String sAL;
    @JsonProperty("XYL")
    public String getXYL() {
        return this.xYL; }
    public void setXYL(String xYL) {
        this.xYL = xYL; }
    String xYL;
    @JsonProperty("ARA")
    public String getARA() {
        return this.aRA; }
    public void setARA(String aRA) {
        this.aRA = aRA; }
    String aRA;
    @JsonProperty("GEL")
    public String getGEL() {
        return this.gEL; }
    public void setGEL(String gEL) {
        this.gEL = gEL; }
    String gEL;
    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC; }
    public void setESC(String eSC) {
        this.eSC = eSC; }
    String eSC;
    @JsonProperty("GLY")
    public String getGLY() {
        return this.gLY; }
    public void setGLY(String gLY) {
        this.gLY = gLY; }
    String gLY;
    @JsonProperty("CEL")
    public String getCEL() {
        return this.cEL; }
    public void setCEL(String cEL) {
        this.cEL = cEL; }
    String cEL;
    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE; }
    public void setMNE(String mNE) {
        this.mNE = mNE; }
    String mNE;
    @JsonProperty("MLZ")
    public String getMLZ() {
        return this.mLZ; }
    public void setMLZ(String mLZ) {
        this.mLZ = mLZ; }
    String mLZ;
    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF; }
    public void setRAF(String rAF) {
        this.rAF = rAF; }
    String rAF;
    @JsonProperty("SOR")
    public String getSOR() {
        return this.sOR; }
    public void setSOR(String sOR) {
        this.sOR = sOR; }
    String sOR;
    @JsonProperty("RHA")
    public String getRHA() {
        return this.rHA; }
    public void setRHA(String rHA) {
        this.rHA = rHA; }
    String rHA;
    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE; }
    public void setTRE(String tRE) {
        this.tRE = tRE; }
    String tRE;
    @JsonProperty("CAT")
    public String getCAT() {
        return this.cAT; }
    public void setCAT(String cAT) {
        this.cAT = cAT; }
    String cAT;

    @JsonProperty("SPOR")
    public String getSPOR() {
        return this.sPOR; }
    public void setSPOR(String sPOR) {
        this.sPOR = sPOR; }
    String sPOR;
    @JsonProperty("GRAM")
    public String getGRAM() {
        return this.gRAM; }
    public void setGRAM(String gRAM) {
        this.gRAM = gRAM; }
    String gRAM;
    @JsonProperty("COCC")
    public String getCOCC() {
        return this.cOCC; }
    public void setCOCC(String cOCC) {
        this.cOCC = cOCC; }
    String cOCC;

}
