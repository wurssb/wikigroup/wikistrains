package nl.munlock.wiki.objects;

import java.util.HashMap;
import java.util.HashSet;

public class WDPage {
    private String URL;
    private String label;
    private String description;
    private String type;
    private String wikiDataURL;
    private final HashMap<String, String> statement = new HashMap<>();

    public void setURL(String url) {
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setLabel(String label) {
        this.label = label;
        if (this.getDescription() != null && this.getLabel().equals(this.getDescription())) {
            this.setDescription(this.getDescription() + ".");
        }
    }

    public String getLabel() {
        return label;
    }

    public void setDescription(String description) {
        if (description.length() > 249) {
            this.description = description.substring(0,240) + "...";
        } else {
            this.description = description;
        }
        if (this.getLabel() != null && this.getLabel().equals(this.description)) {
            this.description = this.description + ".";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setWikiDataURL(String wikiDataURL) {
        this.wikiDataURL = wikiDataURL;
    }

    public String getWikiDataURL() {
        return wikiDataURL;
    }

    public void addStatement(String key, String value) {
        this.statement.put(key, value);
    }

    public HashMap<String, String> getStatement() {
        return this.statement;
    }
}
