package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Isolation {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("sample type")
    public String getSampleType() {
        return this.sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    String sampleType;

    @JsonProperty("sampling date")
    public String getSamplingDate() {
        return this.samplingDate;
    }

    public void setSamplingDate(String samplingDate) {
        this.samplingDate = samplingDate;
    }

    String samplingDate;

    @JsonProperty("host species")
    public String getHostSpecies() {
        return this.hostSpecies;
    }

    public void setHostSpecies(String hostSpecies) {
        this.hostSpecies = hostSpecies;
    }

    String hostSpecies;

    @JsonProperty("geographic location")
    public String getGeographicLocation() {
        return this.geographicLocation;
    }

    public void setGeographicLocation(String geographicLocation) {
        this.geographicLocation = geographicLocation;
    }

    String geographicLocation;

    @JsonProperty("country")
    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    String country;

    @JsonProperty("origin.country")
    public String getOriginCountry() {
        return this.originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    String originCountry;

    @JsonProperty("continent")
    public String getContinent() {
        return this.continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    String continent;

    @JsonProperty("latitude")
    public double getLatitude() {
        return this.latitude; }
    public void setLatitude(double latitude) {
        this.latitude = latitude; }
    double latitude;
    @JsonProperty("longitude")
    public double getLongitude() {
        return this.longitude; }
    public void setLongitude(double longitude) {
        this.longitude = longitude; }
    double longitude;

    @JsonProperty("enrichment culture")
    public String getEnrichmentCulture() {
        return this.enrichmentCulture; }
    public void setEnrichmentCulture(String enrichmentCulture) {
        this.enrichmentCulture = enrichmentCulture; }
    String enrichmentCulture;

    @JsonProperty("enrichment culture duration")
    public String getEnrichmentCultureDuration() {
        return this.enrichmentCultureDuration; }
    public void setEnrichmentCultureDuration(String enrichmentCultureDuration) {
        this.enrichmentCultureDuration = enrichmentCultureDuration; }
    String enrichmentCultureDuration;
    @JsonProperty("enrichment culture temperature")
    public String getEnrichmentCultureTemperature() {
        return this.enrichmentCultureTemperature; }
    public void setEnrichmentCultureTemperature(String enrichmentCultureTemperature) {
        this.enrichmentCultureTemperature = enrichmentCultureTemperature; }
    String enrichmentCultureTemperature;

    @JsonProperty("isolation procedure")
    public String getIsolationProcedure() {
        return this.isolationProcedure; }
    public void setIsolationProcedure(String isolationProcedure) {
        this.isolationProcedure = isolationProcedure; }
    String isolationProcedure;

    @JsonProperty("enrichment culture composition")
    public String getEnrichmentCultureComposition() {
        return this.enrichmentCultureComposition; }
    public void setEnrichmentCultureComposition(String enrichmentCultureComposition) {
        this.enrichmentCultureComposition = enrichmentCultureComposition; }
    String enrichmentCultureComposition;

    @JsonProperty("API 20A")
    public List<API20A> getAPI20A() {
        return this.aPI20A; }
    public void setAPI20A(List<API20A> aPI20A) {
        this.aPI20A = aPI20A; }
    List<API20A> aPI20A;

    @JsonProperty("isolation date")
    public String getIsolationDate() {
        return this.isolationDate; }
    public void setIsolationDate(String isolationDate) {
        this.isolationDate = isolationDate; }
    String isolationDate;

}
