package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Antibiogram {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("medium")
    public String getMedium() {
        return this.medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    String medium;

    @JsonProperty("incubation temperature")
    public int getIncubationTemperature() {
        return this.incubationTemperature;
    }

    public void setIncubationTemperature(int incubationTemperature) {
        this.incubationTemperature = incubationTemperature;
    }

    int incubationTemperature;

    @JsonProperty("incubation time")
    public int getIncubationTime() {
        return this.incubationTime;
    }

    public void setIncubationTime(int incubationTime) {
        this.incubationTime = incubationTime;
    }

    int incubationTime;

    @JsonProperty("oxygen condition")
    public String getOxygenCondition() {
        return this.oxygenCondition;
    }

    public void setOxygenCondition(String oxygenCondition) {
        this.oxygenCondition = oxygenCondition;
    }

    String oxygenCondition;

    @JsonProperty("Penicillin G")
    public String getPenicillinG() {
        return this.penicillinG;
    }

    public void setPenicillinG(String penicillinG) {
        this.penicillinG = penicillinG;
    }

    String penicillinG;

    @JsonProperty("oxacillin")
    public String getOxacillin() {
        return this.oxacillin;
    }

    public void setOxacillin(String oxacillin) {
        this.oxacillin = oxacillin;
    }

    String oxacillin;

    @JsonProperty("ampicillin")
    public String getAmpicillin() {
        return this.ampicillin;
    }

    public void setAmpicillin(String ampicillin) {
        this.ampicillin = ampicillin;
    }

    String ampicillin;

    @JsonProperty("ticarcillin")
    public String getTicarcillin() {
        return this.ticarcillin;
    }

    public void setTicarcillin(String ticarcillin) {
        this.ticarcillin = ticarcillin;
    }

    String ticarcillin;

    @JsonProperty("mezlocillin")
    public String getMezlocillin() {
        return this.mezlocillin;
    }

    public void setMezlocillin(String mezlocillin) {
        this.mezlocillin = mezlocillin;
    }

    String mezlocillin;

    @JsonProperty("cefalotin")
    public String getCefalotin() {
        return this.cefalotin;
    }

    public void setCefalotin(String cefalotin) {
        this.cefalotin = cefalotin;
    }

    String cefalotin;

    @JsonProperty("cefazolin")
    public String getCefazolin() {
        return this.cefazolin;
    }

    public void setCefazolin(String cefazolin) {
        this.cefazolin = cefazolin;
    }

    String cefazolin;

    @JsonProperty("cefotaxime")
    public String getCefotaxime() {
        return this.cefotaxime;
    }

    public void setCefotaxime(String cefotaxime) {
        this.cefotaxime = cefotaxime;
    }

    String cefotaxime;

    @JsonProperty("aztreonam")
    public String getAztreonam() {
        return this.aztreonam;
    }

    public void setAztreonam(String aztreonam) {
        this.aztreonam = aztreonam;
    }

    String aztreonam;

    @JsonProperty("imipenem")
    public String getImipenem() {
        return this.imipenem;
    }

    public void setImipenem(String imipenem) {
        this.imipenem = imipenem;
    }

    String imipenem;

    @JsonProperty("tetracycline")
    public String getTetracycline() {
        return this.tetracycline;
    }

    public void setTetracycline(String tetracycline) {
        this.tetracycline = tetracycline;
    }

    String tetracycline;

    @JsonProperty("chloramphenicol")
    public String getChloramphenicol() {
        return this.chloramphenicol;
    }

    public void setChloramphenicol(String chloramphenicol) {
        this.chloramphenicol = chloramphenicol;
    }

    String chloramphenicol;

    @JsonProperty("gentamycin")
    public String getGentamycin() {
        return this.gentamycin;
    }

    public void setGentamycin(String gentamycin) {
        this.gentamycin = gentamycin;
    }

    String gentamycin;

    @JsonProperty("amikacin")
    public String getAmikacin() {
        return this.amikacin;
    }

    public void setAmikacin(String amikacin) {
        this.amikacin = amikacin;
    }

    String amikacin;

    @JsonProperty("vancomycin")
    public String getVancomycin() {
        return this.vancomycin;
    }

    public void setVancomycin(String vancomycin) {
        this.vancomycin = vancomycin;
    }

    String vancomycin;

    @JsonProperty("erythromycin")
    public String getErythromycin() {
        return this.erythromycin;
    }

    public void setErythromycin(String erythromycin) {
        this.erythromycin = erythromycin;
    }

    String erythromycin;

    @JsonProperty("lincomycin")
    public String getLincomycin() {
        return this.lincomycin;
    }

    public void setLincomycin(String lincomycin) {
        this.lincomycin = lincomycin;
    }

    String lincomycin;

    @JsonProperty("ofloxacin")
    public String getOfloxacin() {
        return this.ofloxacin;
    }

    public void setOfloxacin(String ofloxacin) {
        this.ofloxacin = ofloxacin;
    }

    String ofloxacin;

    @JsonProperty("norfloxacin")
    public String getNorfloxacin() {
        return this.norfloxacin;
    }

    public void setNorfloxacin(String norfloxacin) {
        this.norfloxacin = norfloxacin;
    }

    String norfloxacin;

    @JsonProperty("colistin")
    public String getColistin() {
        return this.colistin;
    }

    public void setColistin(String colistin) {
        this.colistin = colistin;
    }

    String colistin;

    @JsonProperty("pipemidic acid")
    public String getPipemidicAcid() {
        return this.pipemidicAcid;
    }

    public void setPipemidicAcid(String pipemidicAcid) {
        this.pipemidicAcid = pipemidicAcid;
    }

    String pipemidicAcid;

    @JsonProperty("nitrofurantoin")
    public String getNitrofurantoin() {
        return this.nitrofurantoin;
    }

    public void setNitrofurantoin(String nitrofurantoin) {
        this.nitrofurantoin = nitrofurantoin;
    }

    String nitrofurantoin;

    @JsonProperty("bacitracin")
    public String getBacitracin() {
        return this.bacitracin;
    }

    public void setBacitracin(String bacitracin) {
        this.bacitracin = bacitracin;
    }

    String bacitracin;

    @JsonProperty("polymyxin b")
    public String getPolymyxinB() {
        return this.polymyxinB;
    }

    public void setPolymyxinB(String polymyxinB) {
        this.polymyxinB = polymyxinB;
    }

    String polymyxinB;

    @JsonProperty("kanamycin")
    public String getKanamycin() {
        return this.kanamycin;
    }

    public void setKanamycin(String kanamycin) {
        this.kanamycin = kanamycin;
    }

    String kanamycin;

    @JsonProperty("neomycin")
    public String getNeomycin() {
        return this.neomycin;
    }

    public void setNeomycin(String neomycin) {
        this.neomycin = neomycin;
    }

    String neomycin;

    @JsonProperty("doxycycline")
    public String getDoxycycline() {
        return this.doxycycline;
    }

    public void setDoxycycline(String doxycycline) {
        this.doxycycline = doxycycline;
    }

    String doxycycline;

    @JsonProperty("ceftriaxone")
    public String getCeftriaxone() {
        return this.ceftriaxone;
    }

    public void setCeftriaxone(String ceftriaxone) {
        this.ceftriaxone = ceftriaxone;
    }

    String ceftriaxone;

    @JsonProperty("clindamycin")
    public String getClindamycin() {
        return this.clindamycin;
    }

    public void setClindamycin(String clindamycin) {
        this.clindamycin = clindamycin;
    }

    String clindamycin;

    @JsonProperty("fosfomycin")
    public String getFosfomycin() {
        return this.fosfomycin;
    }

    public void setFosfomycin(String fosfomycin) {
        this.fosfomycin = fosfomycin;
    }

    String fosfomycin;

    @JsonProperty("moxifloxacin")
    public String getMoxifloxacin() {
        return this.moxifloxacin;
    }

    public void setMoxifloxacin(String moxifloxacin) {
        this.moxifloxacin = moxifloxacin;
    }

    String moxifloxacin;

    @JsonProperty("linezolid")
    public String getLinezolid() {
        return this.linezolid;
    }

    public void setLinezolid(String linezolid) {
        this.linezolid = linezolid;
    }

    String linezolid;

    @JsonProperty("nystatin")
    public String getNystatin() {
        return this.nystatin;
    }

    public void setNystatin(String nystatin) {
        this.nystatin = nystatin;
    }

    String nystatin;

    @JsonProperty("quinupristin/dalfopristin")
    public String getQuinupristinDalfopristin() {
        return this.quinupristinDalfopristin;
    }

    public void setQuinupristinDalfopristin(String quinupristinDalfopristin) {
        this.quinupristinDalfopristin = quinupristinDalfopristin;
    }

    String quinupristinDalfopristin;

    @JsonProperty("teicoplanin")
    public String getTeicoplanin() {
        return this.teicoplanin;
    }

    public void setTeicoplanin(String teicoplanin) {
        this.teicoplanin = teicoplanin;
    }

    String teicoplanin;

    @JsonProperty("piperacillin/tazobactam")
    public String getPiperacillinTazobactam() {
        return this.piperacillinTazobactam;
    }

    public void setPiperacillinTazobactam(String piperacillinTazobactam) {
        this.piperacillinTazobactam = piperacillinTazobactam;
    }

    String piperacillinTazobactam;
}
