package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FattyAcidProfile{
    @JsonProperty("fatty acids")
    public List<FattyAcid> getFattyAcids() {
        return this.fattyAcids; }
    public void setFattyAcids(List<FattyAcid> fattyAcids) {
        this.fattyAcids = fattyAcids; }
    List<FattyAcid> fattyAcids;
    @JsonProperty("type of FA analysis")
    public String getTypeOfFAAnalysis() {
        return this.typeOfFAAnalysis; }
    public void setTypeOfFAAnalysis(String typeOfFAAnalysis) {
        this.typeOfFAAnalysis = typeOfFAAnalysis; }
    String typeOfFAAnalysis;
    @JsonProperty("method/protocol")
    public String getMethodProtocol() {
        return this.methodProtocol; }
    public void setMethodProtocol(String methodProtocol) {
        this.methodProtocol = methodProtocol; }
    String methodProtocol;
    @JsonProperty("API LIST")
    public List<APILIST> getAPILIST() {
        return this.aPILIST; }
    public void setAPILIST(List<APILIST> aPILIST) {
        this.aPILIST = aPILIST; }
    List<APILIST> aPILIST;
    @JsonProperty("incubation medium")
    public String getIncubationMedium() {
        return this.incubationMedium; }
    public void setIncubationMedium(String incubationMedium) {
        this.incubationMedium = incubationMedium; }
    String incubationMedium;
    @JsonProperty("agar/liquid")
    public String getAgarLiquid() {
        return this.agarLiquid; }
    public void setAgarLiquid(String agarLiquid) {
        this.agarLiquid = agarLiquid; }
    String agarLiquid;
    @JsonProperty("incubation temperature")
    public String getIncubationTemperature() {
        return this.incubationTemperature; }
    public void setIncubationTemperature(String incubationTemperature) {
        this.incubationTemperature = incubationTemperature; }
    String incubationTemperature;
    @JsonProperty("incubation time")
    public String getIncubationTime() {
        return this.incubationTime; }
    public void setIncubationTime(String incubationTime) {
        this.incubationTime = incubationTime; }
    String incubationTime;
    @JsonProperty("incubation_oxygen")
    public String getIncubation_oxygen() {
        return this.incubation_oxygen; }
    public void setIncubation_oxygen(String incubation_oxygen) {
        this.incubation_oxygen = incubation_oxygen; }
    String incubation_oxygen;
    @JsonProperty("software version")
    public String getSoftwareVersion() {
        return this.softwareVersion; }
    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion; }
    String softwareVersion;
    @JsonProperty("library/peak naming table")
    public String getLibraryPeakNamingTable() {
        return this.libraryPeakNamingTable; }
    public void setLibraryPeakNamingTable(String libraryPeakNamingTable) {
        this.libraryPeakNamingTable = libraryPeakNamingTable; }
    String libraryPeakNamingTable;
    @JsonProperty("system")
    public String getSystem() {
        return this.system; }
    public void setSystem(String system) {
        this.system = system; }
    String system;
    @JsonProperty("cutoff value")
    public Object getCutoffValue() {
        return this.cutoffValue; }
    public void setCutoffValue(Object cutoffValue) {
        this.cutoffValue = cutoffValue; }
    Object cutoffValue;

    @JsonProperty("instrument")
    public String getInstrument() {
        return this.instrument; }
    public void setInstrument(String instrument) {
        this.instrument = instrument; }
    String instrument;

    @JsonProperty("incubation pH")
    public String getIncubationPH() {
        return this.incubationPH; }
    public void setIncubationPH(String incubationPH) {
        this.incubationPH = incubationPH; }
    String incubationPH;

    @JsonProperty("treatment")
    public String getTreatment() {
        return this.treatment; }
    public void setTreatment(String treatment) {
        this.treatment = treatment; }
    String treatment;

}

