package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PhysiologyAndMetabolism {

    @JsonProperty("metabolite utilization")
    public List<MetaboliteUtilization> getMetaboliteUtilization() {
        return this.metaboliteUtilization;
    }

    public void setMetaboliteUtilization(List<MetaboliteUtilization> metaboliteUtilization) {
        this.metaboliteUtilization = metaboliteUtilization;
    }

    List<MetaboliteUtilization> metaboliteUtilization;

    @JsonProperty("API coryne")
    public List<APICoryne> getAPICoryne() {
        return this.aPICoryne;
    }

    public void setAPICoryne(List<APICoryne> aPICoryne) {
        this.aPICoryne = aPICoryne;
    }

    List<APICoryne> aPICoryne;

    @JsonProperty("API zym")
    public List<APIZym> getAPIZym() {
        return this.aPIZym;
    }

    public void setAPIZym(List<APIZym> aPIZym) {
        this.aPIZym = aPIZym;
    }

    List<APIZym> aPIZym;

    @JsonProperty("API NH")
    public List<APINH> getAPINH() {
        return this.aPINH;
    }

    public void setAPINH(List<APINH> aPINH) {
        this.aPINH = aPINH;
    }

    List<APINH> aPINH;

    @JsonProperty("oxygen tolerance")
    public List<OxygenTolerance> getOxygenTolerance() {
        return this.oxygenTolerance;
    }

    public void setOxygenTolerance(List<OxygenTolerance> oxygenTolerance) {
        this.oxygenTolerance = oxygenTolerance;
    }

    List<OxygenTolerance> oxygenTolerance;

    @JsonProperty("murein")
    public List<Murein> getMurein() {
        return this.murein;
    }

    public void setMurein(List<Murein> murein) {
        this.murein = murein;
    }

    List<Murein> murein;

    @JsonProperty("API 50CHac")
    public List<API50CHac> getAPI50CHac() {
        return this.aPI50CHac;
    }

    public void setAPI50CHac(List<API50CHac> aPI50CHac) {
        this.aPI50CHac = aPI50CHac;
    }

    List<API50CHac> aPI50CHac;

    @JsonProperty("spore formation")
    public List<SporeFormation> getSporeFormation() {
        return this.sporeFormation;
    }

    public void setSporeFormation(List<SporeFormation> sporeFormation) {
        this.sporeFormation = sporeFormation;
    }

    List<SporeFormation> sporeFormation;

    @JsonProperty("halophily")
    public List<Halophily> getHalophily() {
        return this.halophily;
    }

    public void setHalophily(List<Halophily> halophily) {
        this.halophily = halophily;
    }

    List<Halophily> halophily;

    @JsonProperty("API rID32STR")
    public List<APIRID32STR> getAPIRID32STR() {
        return this.aPIRID32STR;
    }

    public void setAPIRID32STR(List<APIRID32STR> aPIRID32STR) {
        this.aPIRID32STR = aPIRID32STR;
    }

    List<APIRID32STR> aPIRID32STR;


    @JsonProperty("enzymes")
    public List<Enzyme> getEnzymes() {
        return this.enzymes;
    }

    public void setEnzymes(List<Enzyme> enzymes) {
        this.enzymes = enzymes;
    }

    List<Enzyme> enzymes;

    @JsonProperty("API CAM")
    public List<APICAM> getAPICAM() {
        return this.aPICAM;
    }

    public void setAPICAM(List<APICAM> aPICAM) {
        this.aPICAM = aPICAM;
    }

    List<APICAM> aPICAM;

    @JsonProperty("observation")
    public List<Observation> getObservation() {
        return this.observation;
    }

    public void setObservation(List<Observation> observation) {
        this.observation = observation;
    }

    List<Observation> observation;

    @JsonProperty("API 20E")
    public List<API20E> getAPI20E() {
        return this.aPI20E;
    }

    public void setAPI20E(List<API20E> aPI20E) {
        this.aPI20E = aPI20E;
    }

    List<API20E> aPI20E;

    @JsonProperty("metabolite production")
    public List<MetaboliteProduction> getMetaboliteProduction() {
        return this.metaboliteProduction;
    }

    public void setMetaboliteProduction(List<MetaboliteProduction> metaboliteProduction) {
        this.metaboliteProduction = metaboliteProduction;
    }

    List<MetaboliteProduction> metaboliteProduction;

    @JsonProperty("API 20NE")
    public List<API20NE> getAPI20NE() {
        return this.aPI20NE;
    }

    public void setAPI20NE(List<API20NE> aPI20NE) {
        this.aPI20NE = aPI20NE;
    }

    List<API20NE> aPI20NE;

    @JsonProperty("metabolite tests")
    public List<MetaboliteTest> getMetaboliteTests() {
        return this.metaboliteTests;
    }

    public void setMetaboliteTests(List<MetaboliteTest> metaboliteTests) {
        this.metaboliteTests = metaboliteTests;
    }

    List<MetaboliteTest> metaboliteTests;

    @JsonProperty("fatty acid profile")
    public List<FattyAcidProfile> getFattyAcidProfile() {
        return this.fattyAcidProfile;
    }

    public void setFattyAcidProfile(List<FattyAcidProfile> fattyAcidProfile) {
        this.fattyAcidProfile = fattyAcidProfile;
    }

    List<FattyAcidProfile> fattyAcidProfile;

    @JsonProperty("API 20STR")
    public List<API20STR> getAPI20STR() {
        return this.aPI20STR;
    }

    public void setAPI20STR(List<API20STR> aPI20STR) {
        this.aPI20STR = aPI20STR;
    }

    List<API20STR> aPI20STR;

    @JsonProperty("tolerance")
    public List<Tolerance> getTolerance() {
        return this.tolerance;
    }

    public void setTolerance(List<Tolerance> tolerance) {
        this.tolerance = tolerance;
    }

    List<Tolerance> tolerance;

    @JsonProperty("compound production")
    public List<CompoundProduction> getCompoundProduction() {
        return this.compoundProduction;
    }

    public void setCompoundProduction(List<CompoundProduction> compoundProduction) {
        this.compoundProduction = compoundProduction;
    }

    List<CompoundProduction> compoundProduction;

    @JsonProperty("API LIST")
    public List<APILIST> getAPILIST() {
        return this.aPILIST;
    }

    public void setAPILIST(List<APILIST> aPILIST) {
        this.aPILIST = aPILIST;
    }

    List<APILIST> aPILIST;

    @JsonProperty("API ID32STA")
    public List<APIID32STA> getAPIID32STA() {
        return this.aPIID32STA;
    }

    public void setAPIID32STA(List<APIID32STA> aPIID32STA) {
        this.aPIID32STA = aPIID32STA;
    }

    List<APIID32STA> aPIID32STA;

    @JsonProperty("nutrition type")
    public List<NutritionType> getNutritionType() {
        return this.nutritionType;
    }

    public void setNutritionType(List<NutritionType> nutritionType) {
        this.nutritionType = nutritionType;
    }

    List<NutritionType> nutritionType;

    @JsonProperty("API ID32E")
    public List<APIID32E> getAPIID32E() {
        return this.aPIID32E;
    }

    public void setAPIID32E(List<APIID32E> aPIID32E) {
        this.aPIID32E = aPIID32E;
    }

    List<APIID32E> aPIID32E;

    @JsonProperty("antibiotic resistance")
    public List<AntibioticResistance> getAntibioticResistance() {
        return this.antibioticResistance;
    }

    public void setAntibioticResistance(List<AntibioticResistance> antibioticResistance) {
        this.antibioticResistance = antibioticResistance;
    }

    List<AntibioticResistance> antibioticResistance;

    @JsonProperty("API rID32A")
    public List<APIRID32A> getAPIRID32A() {
        return this.aPIRID32A;
    }

    public void setAPIRID32A(List<APIRID32A> aPIRID32A) {
        this.aPIRID32A = aPIRID32A;
    }

    List<APIRID32A> aPIRID32A;

    @JsonProperty("API 20A")
    public List<API20A> getAPI20A() {
        return this.aPI20A;
    }

    public void setAPI20A(List<API20A> aPI20A) {
        this.aPI20A = aPI20A;
    }

    List<API20A> aPI20A;

    @JsonProperty("antibiogram")
    public List<Antibiogram> getAntibiogram() {
        return this.antibiogram;
    }

    public void setAntibiogram(List<Antibiogram> antibiogram) {
        this.antibiogram = antibiogram;
    }

    List<Antibiogram> antibiogram;

    @JsonProperty("API STA")
    public List<APISTA> getAPISTA() {
        return this.aPISTA; }
    public void setAPISTA(List<APISTA> aPISTA) {
        this.aPISTA = aPISTA; }
    List<APISTA> aPISTA;

    @JsonProperty("API 50CHas")
    public API50CHas getAPI50CHas() {
        return this.aPI50CHas; }
    public void setAPI50CHas(API50CHas aPI50CHas) {
        this.aPI50CHas = aPI50CHas; }
    API50CHas aPI50CHas;

}


