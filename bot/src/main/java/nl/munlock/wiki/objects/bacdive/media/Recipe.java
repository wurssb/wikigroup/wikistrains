package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recipe {
    @JsonProperty("recipe No.")
    public int getRecipeNo() {
        return this.recipeNo;
    }

    public void setRecipeNo(int recipeNo) {
        this.recipeNo = recipeNo;
    }

    int recipeNo;

    @JsonProperty("compound")
    public String getCompound() {
        return this.compound;
    }

    public void setCompound(String compound) {
        this.compound = compound;
    }

    String compound;

    @JsonProperty("compound_id")
    public String getCompound_id() {
        return this.compound_id;
    }

    public void setCompound_id(String compound_id) {
        this.compound_id = compound_id;
    }

    String compound_id;

    @JsonProperty("amount")
    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    int amount;

    @JsonProperty("unit")
    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    String unit;

    @JsonProperty("optional")
    public String getOptional() {
        return this.optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    String optional;

    @JsonProperty("condition")
    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    String condition;

    @JsonProperty("g_l")
    public double getG_l() {
        return this.g_l; }
    public void setG_l(double g_l) {
        this.g_l = g_l; }
    double g_l;

    @JsonProperty("mmol_l")
    public double getMmol_l() {
        return this.mmol_l; }
    public void setMmol_l(double mmol_l) {
        this.mmol_l = mmol_l; }
    double mmol_l;

    @JsonProperty("attribute")
    public String getAttribute() {
        return this.attribute; }
    public void setAttribute(String attribute) {
        this.attribute = attribute; }
    String attribute;

    @JsonProperty("solution")
    public String getSolution() {
        return this.solution; }
    public void setSolution(String solution) {
        this.solution = solution; }

    String solution;
    @JsonProperty("solution_id")
    public int getSolution_id() {
        return this.solution_id; }
    public void setSolution_id(int solution_id) {
        this.solution_id = solution_id; }
    int solution_id;

}
