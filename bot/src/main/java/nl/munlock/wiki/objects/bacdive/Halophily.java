package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Halophily{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("salt")
    public String getSalt() {
        return this.salt; }
    public void setSalt(String salt) {
        this.salt = salt; }
    String salt;
    @JsonProperty("growth")
    public String getGrowth() {
        return this.growth; }
    public void setGrowth(String growth) {
        this.growth = growth; }
    String growth;
    @JsonProperty("tested relation")
    public String getTestedRelation() {
        return this.testedRelation; }
    public void setTestedRelation(String testedRelation) {
        this.testedRelation = testedRelation; }
    String testedRelation;
    @JsonProperty("concentration")
    public String getConcentration() {
        return this.concentration; }
    public void setConcentration(String concentration) {
        this.concentration = concentration; }
    String concentration;
    @JsonProperty("halophily level")
    public String getHalophilyLevel() {
        return this.halophilyLevel; }
    public void setHalophilyLevel(String halophilyLevel) {
        this.halophilyLevel = halophilyLevel; }
    String halophilyLevel;

}