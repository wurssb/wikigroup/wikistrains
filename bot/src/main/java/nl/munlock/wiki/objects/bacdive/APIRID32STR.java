package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APIRID32STR {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg;
    }

    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg;
    }

    String aDHArg;

    @JsonProperty("beta GLU")
    public String getBetaGLU() {
        return this.betaGLU;
    }

    public void setBetaGLU(String betaGLU) {
        this.betaGLU = betaGLU;
    }

    String betaGLU;

    @JsonProperty("beta GAR")
    public String getBetaGAR() {
        return this.betaGAR;
    }

    public void setBetaGAR(String betaGAR) {
        this.betaGAR = betaGAR;
    }

    String betaGAR;

    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR;
    }

    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR;
    }

    String betaGUR;

    @JsonProperty("alpha GAL")
    public String getAlphaGAL() {
        return this.alphaGAL;
    }

    public void setAlphaGAL(String alphaGAL) {
        this.alphaGAL = alphaGAL;
    }

    String alphaGAL;

    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL;
    }

    public void setPAL(String pAL) {
        this.pAL = pAL;
    }

    String pAL;

    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB;
    }

    public void setRIB(String rIB) {
        this.rIB = rIB;
    }

    String rIB;

    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN;
    }

    public void setMAN(String mAN) {
        this.mAN = mAN;
    }

    String mAN;

    @JsonProperty("SOR")
    public String getSOR() {
        return this.sOR;
    }

    public void setSOR(String sOR) {
        this.sOR = sOR;
    }

    String sOR;

    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC;
    }

    public void setLAC(String lAC) {
        this.lAC = lAC;
    }

    String lAC;

    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE;
    }

    public void setTRE(String tRE) {
        this.tRE = tRE;
    }

    String tRE;

    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF;
    }

    public void setRAF(String rAF) {
        this.rAF = rAF;
    }

    String rAF;

    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC;
    }

    public void setSAC(String sAC) {
        this.sAC = sAC;
    }

    String sAC;

    @JsonProperty("LARA")
    public String getLARA() {
        return this.lARA;
    }

    public void setLARA(String lARA) {
        this.lARA = lARA;
    }

    String lARA;

    @JsonProperty("DARL")
    public String getDARL() {
        return this.dARL;
    }

    public void setDARL(String dARL) {
        this.dARL = dARL;
    }

    String dARL;

    @JsonProperty("CDEX")
    public String getCDEX() {
        return this.cDEX;
    }

    public void setCDEX(String cDEX) {
        this.cDEX = cDEX;
    }

    String cDEX;

    @JsonProperty("VP")
    public String getVP() {
        return this.vP;
    }

    public void setVP(String vP) {
        this.vP = vP;
    }

    String vP;

    @JsonProperty("APPA")
    public String getAPPA() {
        return this.aPPA;
    }

    public void setAPPA(String aPPA) {
        this.aPPA = aPPA;
    }

    String aPPA;

    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL;
    }

    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL;
    }

    String betaGAL;

    @JsonProperty("PyrA")
    public String getPyrA() {
        return this.pyrA;
    }

    public void setPyrA(String pyrA) {
        this.pyrA = pyrA;
    }

    String pyrA;

    @JsonProperty("beta NAG")
    public String getBetaNAG() {
        return this.betaNAG;
    }

    public void setBetaNAG(String betaNAG) {
        this.betaNAG = betaNAG;
    }

    String betaNAG;

    @JsonProperty("GTA")
    public String getGTA() {
        return this.gTA;
    }

    public void setGTA(String gTA) {
        this.gTA = gTA;
    }

    String gTA;

    @JsonProperty("HIP")
    public String getHIP() {
        return this.hIP;
    }

    public void setHIP(String hIP) {
        this.hIP = hIP;
    }

    String hIP;

    @JsonProperty("GLYG")
    public String getGLYG() {
        return this.gLYG;
    }

    public void setGLYG(String gLYG) {
        this.gLYG = gLYG;
    }

    String gLYG;

    @JsonProperty("PUL")
    public String getPUL() {
        return this.pUL;
    }

    public void setPUL(String pUL) {
        this.pUL = pUL;
    }

    String pUL;

    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL;
    }

    public void setMAL(String mAL) {
        this.mAL = mAL;
    }

    String mAL;

    @JsonProperty("MEL")
    public String getMEL() {
        return this.mEL;
    }

    public void setMEL(String mEL) {
        this.mEL = mEL;
    }

    String mEL;

    @JsonProperty("MLZ")
    public String getMLZ() {
        return this.mLZ;
    }

    public void setMLZ(String mLZ) {
        this.mLZ = mLZ;
    }

    String mLZ;

    @JsonProperty("Mbeta DG")
    public String getMbetaDG() {
        return this.mbetaDG;
    }

    public void setMbetaDG(String mbetaDG) {
        this.mbetaDG = mbetaDG;
    }

    String mbetaDG;

    @JsonProperty("TAG")
    public String getTAG() {
        return this.tAG;
    }

    public void setTAG(String tAG) {
        this.tAG = tAG;
    }

    String tAG;

    @JsonProperty("beta MAN")
    public String getBetaMAN() {
        return this.betaMAN;
    }

    public void setBetaMAN(String betaMAN) {
        this.betaMAN = betaMAN;
    }

    String betaMAN;

    @JsonProperty("URE")
    public String getURE() {
        return this.uRE;
    }

    public void setURE(String uRE) {
        this.uRE = uRE;
    }

    String uRE;
}
