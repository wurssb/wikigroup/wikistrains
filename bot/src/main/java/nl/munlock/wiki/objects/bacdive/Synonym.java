package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Synonym{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("synonym")
    public String getSynonym() {
        return this.synonym; }
    public void setSynonym(String synonym) {
        this.synonym = synonym; }
    String synonym;
}