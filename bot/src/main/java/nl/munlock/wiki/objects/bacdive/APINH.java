package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APINH{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("PEN")
    public String getPEN() {
        return this.pEN; }
    public void setPEN(String pEN) {
        this.pEN = pEN; }
    String pEN;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("FRU")
    public String getFRU() {
        return this.fRU; }
    public void setFRU(String fRU) {
        this.fRU = fRU; }
    String fRU;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("ODC")
    public String getODC() {
        return this.oDC; }
    public void setODC(String oDC) {
        this.oDC = oDC; }
    String oDC;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("LIP")
    public String getLIP() {
        return this.lIP; }
    public void setLIP(String lIP) {
        this.lIP = lIP; }
    String lIP;
    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL; }
    public void setPAL(String pAL) {
        this.pAL = pAL; }
    String pAL;
    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL; }
    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL; }
    String betaGAL;
    @JsonProperty("ProA")
    public String getProA() {
        return this.proA; }
    public void setProA(String proA) {
        this.proA = proA; }
    String proA;
    @JsonProperty("GGT")
    public String getGGT() {
        return this.gGT; }
    public void setGGT(String gGT) {
        this.gGT = gGT; }
    String gGT;
    @JsonProperty("IND")
    public String getIND() {
        return this.iND; }
    public void setIND(String iND) {
        this.iND = iND; }
    String iND;
}
