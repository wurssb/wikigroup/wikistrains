package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Root {
    @JsonProperty("General")
    public General getGeneral() {
        return this.general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    General general;

    @JsonProperty("Name and taxonomic classification")
    public NameAndTaxonomicClassification getNameAndTaxonomicClassification() {
        return this.nameAndTaxonomicClassification;
    }

    public void setNameAndTaxonomicClassification(NameAndTaxonomicClassification nameAndTaxonomicClassification) {
        this.nameAndTaxonomicClassification = nameAndTaxonomicClassification;
    }

    NameAndTaxonomicClassification nameAndTaxonomicClassification;

    @JsonProperty("Morphology")
    public Morphology getMorphology() {
        return this.morphology;
    }

    public void setMorphology(Morphology morphology) {
        this.morphology = morphology;
    }

    Morphology morphology;

    @JsonProperty("Culture and growth conditions")
    public CultureAndGrowthConditions getCultureAndGrowthConditions() {
        return this.cultureAndGrowthConditions;
    }

    public void setCultureAndGrowthConditions(CultureAndGrowthConditions cultureAndGrowthConditions) {
        this.cultureAndGrowthConditions = cultureAndGrowthConditions;
    }

    CultureAndGrowthConditions cultureAndGrowthConditions;

    @JsonProperty("Physiology and metabolism")
    public PhysiologyAndMetabolism getPhysiologyAndMetabolism() {
        return this.physiologyAndMetabolism;
    }

    public void setPhysiologyAndMetabolism(PhysiologyAndMetabolism physiologyAndMetabolism) {
        this.physiologyAndMetabolism = physiologyAndMetabolism;
    }

    PhysiologyAndMetabolism physiologyAndMetabolism;

    @JsonProperty("Isolation, sampling and environmental information")
    public IsolationSamplingAndEnvironmentalInformation getIsolationSamplingAndEnvironmentalInformation() {
        return this.isolationSamplingAndEnvironmentalInformation;
    }

    public void setIsolationSamplingAndEnvironmentalInformation(IsolationSamplingAndEnvironmentalInformation isolationSamplingAndEnvironmentalInformation) {
        this.isolationSamplingAndEnvironmentalInformation = isolationSamplingAndEnvironmentalInformation;
    }

    IsolationSamplingAndEnvironmentalInformation isolationSamplingAndEnvironmentalInformation;

    @JsonProperty("Safety information")
    public SafetyInformation getSafetyInformation() {
        return this.safetyInformation;
    }

    public void setSafetyInformation(SafetyInformation safetyInformation) {
        this.safetyInformation = safetyInformation;
    }

    SafetyInformation safetyInformation;

    @JsonProperty("Sequence information")
    public SequenceInformation getSequenceInformation() {
        return this.sequenceInformation;
    }

    public void setSequenceInformation(SequenceInformation sequenceInformation) {
        this.sequenceInformation = sequenceInformation;
    }

    SequenceInformation sequenceInformation;

    @JsonProperty("External links")
    public ExternalLinks getExternalLinks() {
        return this.externalLinks;
    }

    public void setExternalLinks(ExternalLinks externalLinks) {
        this.externalLinks = externalLinks;
    }

    ExternalLinks externalLinks;

    @JsonProperty("Reference")
    public List<Reference> getReference() {
        return this.reference;
    }

    public void setReference(List<Reference> reference) {
        this.reference = reference;
    }

    List<Reference> reference;


}
