package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Morphology{
    @JsonProperty("colony morphology")
    public List<ColonyMorphology> getColonyMorphology() {
        return this.colonyMorphology; }
    public void setColonyMorphology(List<ColonyMorphology> colonyMorphology) {
        this.colonyMorphology = colonyMorphology; }
    List<ColonyMorphology> colonyMorphology;
    @JsonProperty("multicellular morphology")
    public List<MulticellularMorphology> getMulticellularMorphology() {
        return this.multicellularMorphology; }
    public void setMulticellularMorphology(List<MulticellularMorphology> multicellularMorphology) {
        this.multicellularMorphology = multicellularMorphology; }
    List<MulticellularMorphology> multicellularMorphology;
    @JsonProperty("multimedia")
    public List<Multimedia> getMultimedia() {
        return this.multimedia; }
    public void setMultimedia(List<Multimedia> multimedia) {
        this.multimedia = multimedia; }
    List<Multimedia> multimedia;

    @JsonProperty("cell morphology")
    public List<CellMorphology> getCellMorphology() {
        return this.cellMorphology; }
    public void setCellMorphology(List<CellMorphology> cellMorphology) {
        this.cellMorphology = cellMorphology; }
    List<CellMorphology> cellMorphology;

    @JsonProperty("pigmentation")
    public List<Pigmentation> getPigmentation() {
        return this.pigmentation; }
    public void setPigmentation(List<Pigmentation> pigmentation) {
        this.pigmentation = pigmentation; }
    List<Pigmentation> pigmentation;

}

