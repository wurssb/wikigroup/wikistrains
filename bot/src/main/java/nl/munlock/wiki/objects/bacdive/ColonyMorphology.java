package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColonyMorphology{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("incubation period") 
    public String getIncubationPeriod() { 
		 return this.incubationPeriod; } 
    public void setIncubationPeriod(String incubationPeriod) { 
		 this.incubationPeriod = incubationPeriod; } 
    String incubationPeriod;
    @JsonProperty("colony color")
    public String getColonyColor() {
        return this.colonyColor; }
    public void setColonyColor(String colonyColor) {
        this.colonyColor = colonyColor; }
    String colonyColor;
    @JsonProperty("type of hemolysis")
    public String getTypeOfHemolysis() {
        return this.typeOfHemolysis; }
    public void setTypeOfHemolysis(String typeOfHemolysis) {
        this.typeOfHemolysis = typeOfHemolysis; }
    String typeOfHemolysis;
    @JsonProperty("colony size")
    public String getColonySize() {
        return this.colonySize; }
    public void setColonySize(String colonySize) {
        this.colonySize = colonySize; }
    String colonySize;

    @JsonProperty("medium used")
    public String getMediumUsed() {
        return this.mediumUsed; }
    public void setMediumUsed(String mediumUsed) {
        this.mediumUsed = mediumUsed; }
    String mediumUsed;
    @JsonProperty("colony shape")
    public String getColonyShape() {
        return this.colonyShape; }
    public void setColonyShape(String colonyShape) {
        this.colonyShape = colonyShape; }
    String colonyShape;
    @JsonProperty("hemolysis ability")
    public String getHemolysisAbility() {
        return this.hemolysisAbility; }
    public void setHemolysisAbility(String hemolysisAbility) {
        this.hemolysisAbility = hemolysisAbility; }
    String hemolysisAbility;

}
