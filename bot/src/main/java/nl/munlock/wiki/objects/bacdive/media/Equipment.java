package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Equipment {
        @JsonProperty("1173")
        public String get_1173() {
            return this._1173; }
        public void set_1173(String _1173) {
            this._1173 = _1173; }
        String _1173;
        @JsonProperty("4687")
        public String get_4687() {
            return this._4687; }
        public void set_4687(String _4687) {
            this._4687 = _4687; }
        String _4687;
        @JsonProperty("4688")
        public String get_4688() {
            return this._4688; }
        public void set_4688(String _4688) {
            this._4688 = _4688; }
        String _4688;
        @JsonProperty("4707")
        public String get_4707() {
            return this._4707; }
        public void set_4707(String _4707) {
            this._4707 = _4707; }
        String _4707;
        @JsonProperty("418")
        public String get_418() {
            return this._418; }
        public void set_418(String _418) {
            this._418 = _418; }
        String _418;
        @JsonProperty("1186")
        public String get_1186() {
            return this._1186; }
        public void set_1186(String _1186) {
            this._1186 = _1186; }
        String _1186;
        @JsonProperty("3234")
        public String get_3234() {
            return this._3234; }
        public void set_3234(String _3234) {
            this._3234 = _3234; }
        String _3234;
        @JsonProperty("2")
        public String get_2() {
            return this._2; }
        public void set_2(String _2) {
            this._2 = _2; }
        String _2;
        @JsonProperty("3130")
        public String get_3130() {
            return this._3130; }
        public void set_3130(String _3130) {
            this._3130 = _3130; }
        String _3130;
        @JsonProperty("5030")
        public String get_5030() {
            return this._5030; }
        public void set_5030(String _5030) {
            this._5030 = _5030; }
        String _5030;
        @JsonProperty("5031")
        public String get_5031() {
            return this._5031; }
        public void set_5031(String _5031) {
            this._5031 = _5031; }
        String _5031;
        @JsonProperty("5032")
        public String get_5032() {
            return this._5032; }
        public void set_5032(String _5032) {
            this._5032 = _5032; }
        String _5032;
        @JsonProperty("5033")
        public String get_5033() {
            return this._5033; }
        public void set_5033(String _5033) {
            this._5033 = _5033; }
        String _5033;
        @JsonProperty("4786")
        public String get_4786() {
            return this._4786; }
        public void set_4786(String _4786) {
            this._4786 = _4786; }
        String _4786;
        @JsonProperty("4787")
        public String get_4787() {
            return this._4787; }
        public void set_4787(String _4787) {
            this._4787 = _4787; }
        String _4787;
        @JsonProperty("4788")
        public String get_4788() {
            return this._4788; }
        public void set_4788(String _4788) {
            this._4788 = _4788; }
        String _4788;
        @JsonProperty("4789")
        public String get_4789() {
            return this._4789; }
        public void set_4789(String _4789) {
            this._4789 = _4789; }
        String _4789;
        @JsonProperty("250")
        public String get_250() {
            return this._250; }
        public void set_250(String _250) {
            this._250 = _250; }
        String _250;
        @JsonProperty("848")
        public String get_848() {
            return this._848; }
        public void set_848(String _848) {
            this._848 = _848; }
        String _848;
        @JsonProperty("2237")
        public String get_2237() {
            return this._2237; }
        public void set_2237(String _2237) {
            this._2237 = _2237; }
        String _2237;
        @JsonProperty("3672")
        public String get_3672() {
            return this._3672; }
        public void set_3672(String _3672) {
            this._3672 = _3672; }
        String _3672;
        @JsonProperty("1743")
        public String get_1743() {
            return this._1743; }
        public void set_1743(String _1743) {
            this._1743 = _1743; }
        String _1743;
        @JsonProperty("3735")
        public String get_3735() {
            return this._3735; }
        public void set_3735(String _3735) {
            this._3735 = _3735; }
        String _3735;
        @JsonProperty("204")
        public String get_204() {
            return this._204; }
        public void set_204(String _204) {
            this._204 = _204; }
        String _204;
        @JsonProperty("2288")
        public String get_2288() {
            return this._2288; }
        public void set_2288(String _2288) {
            this._2288 = _2288; }
        String _2288;
        @JsonProperty("3311")
        public String get_3311() {
            return this._3311; }
        public void set_3311(String _3311) {
            this._3311 = _3311; }
        String _3311;
        @JsonProperty("4253")
        public String get_4253() {
            return this._4253; }
        public void set_4253(String _4253) {
            this._4253 = _4253; }
        String _4253;
        @JsonProperty("1072")
        public String get_1072() {
            return this._1072; }
        public void set_1072(String _1072) {
            this._1072 = _1072; }
        String _1072;
        @JsonProperty("3068")
        public String get_3068() {
            return this._3068; }
        public void set_3068(String _3068) {
            this._3068 = _3068; }
        String _3068;
        @JsonProperty("3874")
        public String get_3874() {
            return this._3874; }
        public void set_3874(String _3874) {
            this._3874 = _3874; }
        String _3874;
        @JsonProperty("4277")
        public String get_4277() {
            return this._4277; }
        public void set_4277(String _4277) {
            this._4277 = _4277; }
        String _4277;
        @JsonProperty("1927")
        public String get_1927() {
            return this._1927; }
        public void set_1927(String _1927) {
            this._1927 = _1927; }
        String _1927;
        @JsonProperty("2731")
        public String get_2731() {
            return this._2731; }
        public void set_2731(String _2731) {
            this._2731 = _2731; }
        String _2731;
        @JsonProperty("3754")
        public String get_3754() {
            return this._3754; }
        public void set_3754(String _3754) {
            this._3754 = _3754; }
        String _3754;
        @JsonProperty("4292")
        public String get_4292() {
            return this._4292; }
        public void set_4292(String _4292) {
            this._4292 = _4292; }
        String _4292;
        @JsonProperty("2672")
        public String get_2672() {
            return this._2672; }
        public void set_2672(String _2672) {
            this._2672 = _2672; }
        String _2672;
        @JsonProperty("2340")
        public String get_2340() {
            return this._2340; }
        public void set_2340(String _2340) {
            this._2340 = _2340; }
        String _2340;
        @JsonProperty("3079")
        public String get_3079() {
            return this._3079; }
        public void set_3079(String _3079) {
            this._3079 = _3079; }
        String _3079;
        @JsonProperty("5069")
        public String get_5069() {
            return this._5069; }
        public void set_5069(String _5069) {
            this._5069 = _5069; }
        String _5069;
        @JsonProperty("5070")
        public String get_5070() {
            return this._5070; }
        public void set_5070(String _5070) {
            this._5070 = _5070; }
        String _5070;
        @JsonProperty("5071")
        public String get_5071() {
            return this._5071; }
        public void set_5071(String _5071) {
            this._5071 = _5071; }
        String _5071;
        @JsonProperty("2296")
        public String get_2296() {
            return this._2296; }
        public void set_2296(String _2296) {
            this._2296 = _2296; }
        String _2296;
        @JsonProperty("1919")
        public String get_1919() {
            return this._1919; }
        public void set_1919(String _1919) {
            this._1919 = _1919; }
        String _1919;
        @JsonProperty("1215")
        public String get_1215() {
            return this._1215; }
        public void set_1215(String _1215) {
            this._1215 = _1215; }
        String _1215;
        @JsonProperty("2366")
        public String get_2366() {
            return this._2366; }
        public void set_2366(String _2366) {
            this._2366 = _2366; }
        String _2366;
        @JsonProperty("1163")
        public String get_1163() {
            return this._1163; }
        public void set_1163(String _1163) {
            this._1163 = _1163; }
        String _1163;
        @JsonProperty("3153")
        public String get_3153() {
            return this._3153; }
        public void set_3153(String _3153) {
            this._3153 = _3153; }
        String _3153;
        @JsonProperty("2559")
        public String get_2559() {
            return this._2559; }
        public void set_2559(String _2559) {
            this._2559 = _2559; }
        String _2559;
        @JsonProperty("2811")
        public String get_2811() {
            return this._2811; }
        public void set_2811(String _2811) {
            this._2811 = _2811; }
        String _2811;
        @JsonProperty("650")
        public String get_650() {
            return this._650; }
        public void set_650(String _650) {
            this._650 = _650; }
        String _650;
        @JsonProperty("2128")
        public String get_2128() {
            return this._2128; }
        public void set_2128(String _2128) {
            this._2128 = _2128; }
        String _2128;
        @JsonProperty("213")
        public String get_213() {
            return this._213; }
        public void set_213(String _213) {
            this._213 = _213; }
        String _213;
        @JsonProperty("2589")
        public String get_2589() {
            return this._2589; }
        public void set_2589(String _2589) {
            this._2589 = _2589; }
        String _2589;
        @JsonProperty("1002")
        public String get_1002() {
            return this._1002; }
        public void set_1002(String _1002) {
            this._1002 = _1002; }
        String _1002;
        @JsonProperty("2351")
        public String get_2351() {
            return this._2351; }
        public void set_2351(String _2351) {
            this._2351 = _2351; }
        String _2351;
        @JsonProperty("4884")
        public String get_4884() {
            return this._4884; }
        public void set_4884(String _4884) {
            this._4884 = _4884; }
        String _4884;
        @JsonProperty("4885")
        public String get_4885() {
            return this._4885; }
        public void set_4885(String _4885) {
            this._4885 = _4885; }
        String _4885;
        @JsonProperty("4886")
        public String get_4886() {
            return this._4886; }
        public void set_4886(String _4886) {
            this._4886 = _4886; }
        String _4886;
        @JsonProperty("4887")
        public String get_4887() {
            return this._4887; }
        public void set_4887(String _4887) {
            this._4887 = _4887; }
        String _4887;
        @JsonProperty("2904")
        public String get_2904() {
            return this._2904; }
        public void set_2904(String _2904) {
            this._2904 = _2904; }
        String _2904;
        @JsonProperty("4324")
        public String get_4324() {
            return this._4324; }
        public void set_4324(String _4324) {
            this._4324 = _4324; }
        String _4324;
        @JsonProperty("4781")
        public String get_4781() {
            return this._4781; }
        public void set_4781(String _4781) {
            this._4781 = _4781; }
        String _4781;
        @JsonProperty("4782")
        public String get_4782() {
            return this._4782; }
        public void set_4782(String _4782) {
            this._4782 = _4782; }
        String _4782;
        @JsonProperty("4554")
        public String get_4554() {
            return this._4554; }
        public void set_4554(String _4554) {
            this._4554 = _4554; }
        String _4554;
        @JsonProperty("655")
        public String get_655() {
            return this._655; }
        public void set_655(String _655) {
            this._655 = _655; }
        String _655;
        @JsonProperty("2953")
        public String get_2953() {
            return this._2953; }
        public void set_2953(String _2953) {
            this._2953 = _2953; }
        String _2953;
        @JsonProperty("1910")
        public String get_1910() {
            return this._1910; }
        public void set_1910(String _1910) {
            this._1910 = _1910; }
        String _1910;
        @JsonProperty("3172")
        public String get_3172() {
            return this._3172; }
        public void set_3172(String _3172) {
            this._3172 = _3172; }
        String _3172;
        @JsonProperty("3430")
        public String get_3430() {
            return this._3430; }
        public void set_3430(String _3430) {
            this._3430 = _3430; }
        String _3430;
        @JsonProperty("3829")
        public String get_3829() {
            return this._3829; }
        public void set_3829(String _3829) {
            this._3829 = _3829; }
        String _3829;
        @JsonProperty("1610")
        public String get_1610() {
            return this._1610; }
        public void set_1610(String _1610) {
            this._1610 = _1610; }
        String _1610;
        @JsonProperty("1894")
        public String get_1894() {
            return this._1894; }
        public void set_1894(String _1894) {
            this._1894 = _1894; }
        String _1894;
        @JsonProperty("3721")
        public String get_3721() {
            return this._3721; }
        public void set_3721(String _3721) {
            this._3721 = _3721; }
        String _3721;
        @JsonProperty("3937")
        public String get_3937() {
            return this._3937; }
        public void set_3937(String _3937) {
            this._3937 = _3937; }
        String _3937;
        @JsonProperty("3938")
        public String get_3938() {
            return this._3938; }
        public void set_3938(String _3938) {
            this._3938 = _3938; }
        String _3938;
        @JsonProperty("3949")
        public String get_3949() {
            return this._3949; }
        public void set_3949(String _3949) {
            this._3949 = _3949; }
        String _3949;
        @JsonProperty("397")
        public String get_397() {
            return this._397; }
        public void set_397(String _397) {
            this._397 = _397; }
        String _397;
        @JsonProperty("3457")
        public String get_3457() {
            return this._3457; }
        public void set_3457(String _3457) {
            this._3457 = _3457; }
        String _3457;
        @JsonProperty("3953")
        public String get_3953() {
            return this._3953; }
        public void set_3953(String _3953) {
            this._3953 = _3953; }
        String _3953;
        @JsonProperty("3954")
        public String get_3954() {
            return this._3954; }
        public void set_3954(String _3954) {
            this._3954 = _3954; }
        String _3954;
        @JsonProperty("3955")
        public String get_3955() {
            return this._3955; }
        public void set_3955(String _3955) {
            this._3955 = _3955; }
        String _3955;
        @JsonProperty("1974")
        public String get_1974() {
            return this._1974; }
        public void set_1974(String _1974) {
            this._1974 = _1974; }
        String _1974;
        @JsonProperty("2776")
        public String get_2776() {
            return this._2776; }
        public void set_2776(String _2776) {
            this._2776 = _2776; }
        String _2776;
        @JsonProperty("429")
        public String get_429() {
            return this._429; }
        public void set_429(String _429) {
            this._429 = _429; }
        String _429;
        @JsonProperty("1480")
        public String get_1480() {
            return this._1480; }
        public void set_1480(String _1480) {
            this._1480 = _1480; }
        String _1480;
        @JsonProperty("3487")
        public String get_3487() {
            return this._3487; }
        public void set_3487(String _3487) {
            this._3487 = _3487; }
        String _3487;
        @JsonProperty("3958")
        public String get_3958() {
            return this._3958; }
        public void set_3958(String _3958) {
            this._3958 = _3958; }
        String _3958;
        @JsonProperty("1875")
        public String get_1875() {
            return this._1875; }
        public void set_1875(String _1875) {
            this._1875 = _1875; }
        String _1875;
        @JsonProperty("277")
        public String get_277() {
            return this._277; }
        public void set_277(String _277) {
            this._277 = _277; }
        String _277;
        @JsonProperty("1401")
        public String get_1401() {
            return this._1401; }
        public void set_1401(String _1401) {
            this._1401 = _1401; }
        String _1401;
        @JsonProperty("3213")
        public String get_3213() {
            return this._3213; }
        public void set_3213(String _3213) {
            this._3213 = _3213; }
        String _3213;
        @JsonProperty("993")
        public String get_993() {
            return this._993; }
        public void set_993(String _993) {
            this._993 = _993; }
        String _993;
        @JsonProperty("1744")
        public String get_1744() {
            return this._1744; }
        public void set_1744(String _1744) {
            this._1744 = _1744; }
        String _1744;
        @JsonProperty("3736")
        public String get_3736() {
            return this._3736; }
        public void set_3736(String _3736) {
            this._3736 = _3736; }
        String _3736;
        @JsonProperty("4021")
        public String get_4021() {
            return this._4021; }
        public void set_4021(String _4021) {
            this._4021 = _4021; }
        String _4021;
        @JsonProperty("601")
        public String get_601() {
            return this._601; }
        public void set_601(String _601) {
            this._601 = _601; }
        String _601;
        @JsonProperty("160")
        public String get_160() {
            return this._160; }
        public void set_160(String _160) {
            this._160 = _160; }
        String _160;
        @JsonProperty("3218")
        public String get_3218() {
            return this._3218; }
        public void set_3218(String _3218) {
            this._3218 = _3218; }
        String _3218;
        @JsonProperty("4451")
        public String get_4451() {
            return this._4451; }
        public void set_4451(String _4451) {
            this._4451 = _4451; }
        String _4451;
        @JsonProperty("36")
        public String get_36() {
            return this._36; }
        public void set_36(String _36) {
            this._36 = _36; }
        String _36;
        @JsonProperty("1111")
        public String get_1111() {
            return this._1111; }
        public void set_1111(String _1111) {
            this._1111 = _1111; }
        String _1111;
        @JsonProperty("1856")
        public String get_1856() {
            return this._1856; }
        public void set_1856(String _1856) {
            this._1856 = _1856; }
        String _1856;
        @JsonProperty("4471")
        public String get_4471() {
            return this._4471; }
        public void set_4471(String _4471) {
            this._4471 = _4471; }
        String _4471;
        @JsonProperty("914")
        public String get_914() {
            return this._914; }
        public void set_914(String _914) {
            this._914 = _914; }
        String _914;
        @JsonProperty("1685")
        public String get_1685() {
            return this._1685; }
        public void set_1685(String _1685) {
            this._1685 = _1685; }
        String _1685;
        @JsonProperty("45")
        public String get_45() {
            return this._45; }
        public void set_45(String _45) {
            this._45 = _45; }
        String _45;
        @JsonProperty("2088")
        public String get_2088() {
            return this._2088; }
        public void set_2088(String _2088) {
            this._2088 = _2088; }
        String _2088;
        @JsonProperty("3601")
        public String get_3601() {
            return this._3601; }
        public void set_3601(String _3601) {
            this._3601 = _3601; }
        String _3601;
        @JsonProperty("4732")
        public String get_4732() {
            return this._4732; }
        public void set_4732(String _4732) {
            this._4732 = _4732; }
        String _4732;
        @JsonProperty("1026")
        public String get_1026() {
            return this._1026; }
        public void set_1026(String _1026) {
            this._1026 = _1026; }
        String _1026;
        @JsonProperty("2536")
        public String get_2536() {
            return this._2536; }
        public void set_2536(String _2536) {
            this._2536 = _2536; }
        String _2536;
        @JsonProperty("594")
        public String get_594() {
            return this._594; }
        public void set_594(String _594) {
            this._594 = _594; }
        String _594;
        @JsonProperty("2641")
        public String get_2641() {
            return this._2641; }
        public void set_2641(String _2641) {
            this._2641 = _2641; }
        String _2641;
        @JsonProperty("3417")
        public String get_3417() {
            return this._3417; }
        public void set_3417(String _3417) {
            this._3417 = _3417; }
        String _3417;
        @JsonProperty("4498")
        public String get_4498() {
            return this._4498; }
        public void set_4498(String _4498) {
            this._4498 = _4498; }
        String _4498;
        @JsonProperty("3647")
        public String get_3647() {
            return this._3647; }
        public void set_3647(String _3647) {
            this._3647 = _3647; }
        String _3647;
        @JsonProperty("3765")
        public String get_3765() {
            return this._3765; }
        public void set_3765(String _3765) {
            this._3765 = _3765; }
        String _3765;

    @JsonProperty("3862")
    public String get_3862() {
        return this._3765; }
    public void set_3862(String _3862) {
        this._3862 = _3862; }
    String _3862;
}
