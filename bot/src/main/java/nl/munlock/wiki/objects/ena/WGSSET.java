package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WGSSET{
    @JsonProperty("PREFIX")
    public String getPREFIX() { 
		 return this.pREFIX; } 
    public void setPREFIX(String pREFIX) { 
		 this.pREFIX = pREFIX; } 
    String pREFIX;
    @JsonProperty("VERSION") 
    public int getVERSION() { 
		 return this.vERSION; } 
    public void setVERSION(int vERSION) { 
		 this.vERSION = vERSION; } 
    int vERSION;
}
