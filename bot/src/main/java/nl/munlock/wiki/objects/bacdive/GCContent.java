package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GCContent{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("GC-content")
    public String getGCContent() {
        return this.gCContent; }
    public void setGCContent(String gCContent) {
        this.gCContent = gCContent; }
    String gCContent;
    @JsonProperty("method")
    public String getMethod() {
        return this.method; }
    public void setMethod(String method) {
        this.method = method; }
    String method;
}