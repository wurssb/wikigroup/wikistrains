package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ASSEMBLYLINK{
    @JsonProperty("URL_LINK")
    public URLLINK getURL_LINK() { 
		 return this.uRL_LINK; } 
    public void setURL_LINK(URLLINK uRL_LINK) { 
		 this.uRL_LINK = uRL_LINK; } 
    URLLINK uRL_LINK;
}
