package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APIZym{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("Alkaline phosphatase")
    public String getAlkalinePhosphatase() {
        return this.alkalinePhosphatase; }
    public void setAlkalinePhosphatase(String alkalinePhosphatase) {
        this.alkalinePhosphatase = alkalinePhosphatase; }
    String alkalinePhosphatase;
    @JsonProperty("Esterase")
    public String getEsterase() {
        return this.esterase; }
    public void setEsterase(String esterase) {
        this.esterase = esterase; }
    String esterase;
    @JsonProperty("Esterase Lipase")
    public String getEsteraseLipase() {
        return this.esteraseLipase; }
    public void setEsteraseLipase(String esteraseLipase) {
        this.esteraseLipase = esteraseLipase; }
    String esteraseLipase;
    @JsonProperty("Lipase")
    public String getLipase() {
        return this.lipase; }
    public void setLipase(String lipase) {
        this.lipase = lipase; }
    String lipase;
    @JsonProperty("Leucine arylamidase")
    public String getLeucineArylamidase() {
        return this.leucineArylamidase; }
    public void setLeucineArylamidase(String leucineArylamidase) {
        this.leucineArylamidase = leucineArylamidase; }
    String leucineArylamidase;
    @JsonProperty("Valine arylamidase")
    public String getValineArylamidase() {
        return this.valineArylamidase; }
    public void setValineArylamidase(String valineArylamidase) {
        this.valineArylamidase = valineArylamidase; }
    String valineArylamidase;
    @JsonProperty("Cystine arylamidase")
    public String getCystineArylamidase() {
        return this.cystineArylamidase; }
    public void setCystineArylamidase(String cystineArylamidase) {
        this.cystineArylamidase = cystineArylamidase; }
    String cystineArylamidase;
    @JsonProperty("Trypsin")
    public String getTrypsin() {
        return this.trypsin; }
    public void setTrypsin(String trypsin) {
        this.trypsin = trypsin; }
    String trypsin;
    @JsonProperty("alpha- Chymotrypsin")
    public String getAlphaChymotrypsin() {
        return this.alphaChymotrypsin; }
    public void setAlphaChymotrypsin(String alphaChymotrypsin) {
        this.alphaChymotrypsin = alphaChymotrypsin; }
    String alphaChymotrypsin;
    @JsonProperty("Acid phosphatase")
    public String getAcidPhosphatase() {
        return this.acidPhosphatase; }
    public void setAcidPhosphatase(String acidPhosphatase) {
        this.acidPhosphatase = acidPhosphatase; }
    String acidPhosphatase;
    @JsonProperty("Naphthol-AS-BI-phosphohydrolase")
    public String getNaphtholASBIPhosphohydrolase() {
        return this.naphtholASBIPhosphohydrolase; }
    public void setNaphtholASBIPhosphohydrolase(String naphtholASBIPhosphohydrolase) {
        this.naphtholASBIPhosphohydrolase = naphtholASBIPhosphohydrolase; }
    String naphtholASBIPhosphohydrolase;
    @JsonProperty("alpha- Galactosidase")
    public String getAlphaGalactosidase() {
        return this.alphaGalactosidase; }
    public void setAlphaGalactosidase(String alphaGalactosidase) {
        this.alphaGalactosidase = alphaGalactosidase; }
    String alphaGalactosidase;
    @JsonProperty("beta- Galactosidase")
    public String getBetaGalactosidase() {
        return this.betaGalactosidase; }
    public void setBetaGalactosidase(String betaGalactosidase) {
        this.betaGalactosidase = betaGalactosidase; }
    String betaGalactosidase;
    @JsonProperty("beta- Glucuronidase")
    public String getBetaGlucuronidase() {
        return this.betaGlucuronidase; }
    public void setBetaGlucuronidase(String betaGlucuronidase) {
        this.betaGlucuronidase = betaGlucuronidase; }
    String betaGlucuronidase;
    @JsonProperty("alpha- Glucosidase")
    public String getAlphaGlucosidase() {
        return this.alphaGlucosidase; }
    public void setAlphaGlucosidase(String alphaGlucosidase) {
        this.alphaGlucosidase = alphaGlucosidase; }
    String alphaGlucosidase;
    @JsonProperty("beta- Glucosidase")
    public String getBetaGlucosidase() {
        return this.betaGlucosidase; }
    public void setBetaGlucosidase(String betaGlucosidase) {
        this.betaGlucosidase = betaGlucosidase; }
    String betaGlucosidase;
    @JsonProperty("N-acetyl-beta- glucosaminidase")
    public String getNAcetylBetaGlucosaminidase() {
        return this.nAcetylBetaGlucosaminidase; }
    public void setNAcetylBetaGlucosaminidase(String nAcetylBetaGlucosaminidase) {
        this.nAcetylBetaGlucosaminidase = nAcetylBetaGlucosaminidase; }
    String nAcetylBetaGlucosaminidase;
    @JsonProperty("alpha- Mannosidase")
    public String getAlphaMannosidase() {
        return this.alphaMannosidase; }
    public void setAlphaMannosidase(String alphaMannosidase) {
        this.alphaMannosidase = alphaMannosidase; }
    String alphaMannosidase;
    @JsonProperty("alpha- Fucosidase")
    public String getAlphaFucosidase() {
        return this.alphaFucosidase; }
    public void setAlphaFucosidase(String alphaFucosidase) {
        this.alphaFucosidase = alphaFucosidase; }
    String alphaFucosidase;
    @JsonProperty("Control")
    public String getControl() {
        return this.control; }
    public void setControl(String control) {
        this.control = control; }
    String control;

}