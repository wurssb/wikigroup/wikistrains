package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ASSEMBLYATTRIBUTES{
    @JsonProperty("ASSEMBLY_ATTRIBUTE")
    public List<ASSEMBLYATTRIBUTE> getASSEMBLY_ATTRIBUTE() {
		 return this.aSSEMBLY_ATTRIBUTE; } 
    public void setASSEMBLY_ATTRIBUTE(List<ASSEMBLYATTRIBUTE> aSSEMBLY_ATTRIBUTE) { 
		 this.aSSEMBLY_ATTRIBUTE = aSSEMBLY_ATTRIBUTE; } 
    List<ASSEMBLYATTRIBUTE> aSSEMBLY_ATTRIBUTE;
}
