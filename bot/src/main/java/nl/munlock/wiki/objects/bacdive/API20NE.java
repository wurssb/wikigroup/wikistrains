package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class API20NE{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("NO3")
    public String getNO3() {
        return this.nO3; }
    public void setNO3(String nO3) {
        this.nO3 = nO3; }
    String nO3;
    @JsonProperty("TRP")
    public String getTRP() {
        return this.tRP; }
    public void setTRP(String tRP) {
        this.tRP = tRP; }
    String tRP;
    @JsonProperty("GLU_ Ferm")
    public String getGLUFerm() {
        return this.gLUFerm; }
    public void setGLUFerm(String gLUFerm) {
        this.gLUFerm = gLUFerm; }
    String gLUFerm;
    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg; }
    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg; }
    String aDHArg;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC; }
    public void setESC(String eSC) {
        this.eSC = eSC; }
    String eSC;
    @JsonProperty("GEL")
    public String getGEL() {
        return this.gEL; }
    public void setGEL(String gEL) {
        this.gEL = gEL; }
    String gEL;
    @JsonProperty("PNPG")
    public String getPNPG() {
        return this.pNPG; }
    public void setPNPG(String pNPG) {
        this.pNPG = pNPG; }
    String pNPG;
    @JsonProperty("GLU_ Assim")
    public String getGLUAssim() {
        return this.gLUAssim; }
    public void setGLUAssim(String gLUAssim) {
        this.gLUAssim = gLUAssim; }
    String gLUAssim;
    @JsonProperty("ARA")
    public String getARA() {
        return this.aRA; }
    public void setARA(String aRA) {
        this.aRA = aRA; }
    String aRA;
    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE; }
    public void setMNE(String mNE) {
        this.mNE = mNE; }
    String mNE;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("NAG")
    public String getNAG() {
        return this.nAG; }
    public void setNAG(String nAG) {
        this.nAG = nAG; }
    String nAG;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("GNT")
    public String getGNT() {
        return this.gNT; }
    public void setGNT(String gNT) {
        this.gNT = gNT; }
    String gNT;
    @JsonProperty("CAP")
    public String getCAP() {
        return this.cAP; }
    public void setCAP(String cAP) {
        this.cAP = cAP; }
    String cAP;
    @JsonProperty("ADI")
    public String getADI() {
        return this.aDI; }
    public void setADI(String aDI) {
        this.aDI = aDI; }
    String aDI;
    @JsonProperty("MLT")
    public String getMLT() {
        return this.mLT; }
    public void setMLT(String mLT) {
        this.mLT = mLT; }
    String mLT;
    @JsonProperty("CIT")
    public String getCIT() {
        return this.cIT; }
    public void setCIT(String cIT) {
        this.cIT = cIT; }
    String cIT;
    @JsonProperty("PAC")
    public String getPAC() {
        return this.pAC; }
    public void setPAC(String pAC) {
        this.pAC = pAC; }
    String pAC;
    @JsonProperty("OX")
    public String getOX() {
        return this.oX; }
    public void setOX(String oX) {
        this.oX = oX; }
    String oX;

}