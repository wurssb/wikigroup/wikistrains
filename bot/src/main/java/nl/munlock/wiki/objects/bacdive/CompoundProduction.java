package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompoundProduction{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("compound")
    public String getCompound() {
        return this.compound; }
    public void setCompound(String compound) {
        this.compound = compound; }
    String compound;
}


