package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StraininfoLink{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("passport") 
    public String getPassport() { 
		 return this.passport; } 
    public void setPassport(String passport) { 
		 this.passport = passport; } 
    String passport;
}
