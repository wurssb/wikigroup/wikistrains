package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OxygenTolerance{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("oxygen tolerance")
    public String getOxygenTolerance() {
        return this.oxygenTolerance; }
    public void setOxygenTolerance(String oxygenTolerance) {
        this.oxygenTolerance = oxygenTolerance; }
    String oxygenTolerance;
}
