package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class API20E{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("ONPG")
    public String getONPG() {
        return this.oNPG; }
    public void setONPG(String oNPG) {
        this.oNPG = oNPG; }
    String oNPG;
    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg; }
    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg; }
    String aDHArg;
    @JsonProperty("LDC Lys")
    public String getLDCLys() {
        return this.lDCLys; }
    public void setLDCLys(String lDCLys) {
        this.lDCLys = lDCLys; }
    String lDCLys;
    @JsonProperty("ODC")
    public String getODC() {
        return this.oDC; }
    public void setODC(String oDC) {
        this.oDC = oDC; }
    String oDC;
    @JsonProperty("CIT")
    public String getCIT() {
        return this.cIT; }
    public void setCIT(String cIT) {
        this.cIT = cIT; }
    String cIT;
    @JsonProperty("H2S")
    public String getH2S() {
        return this.h2S; }
    public void setH2S(String h2S) {
        this.h2S = h2S; }
    String h2S;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("TDA Trp")
    public String getTDATrp() {
        return this.tDATrp; }
    public void setTDATrp(String tDATrp) {
        this.tDATrp = tDATrp; }
    String tDATrp;
    @JsonProperty("IND")
    public String getIND() {
        return this.iND; }
    public void setIND(String iND) {
        this.iND = iND; }
    String iND;
    @JsonProperty("VP")
    public String getVP() {
        return this.vP; }
    public void setVP(String vP) {
        this.vP = vP; }
    String vP;
    @JsonProperty("GEL")
    public String getGEL() {
        return this.gEL; }
    public void setGEL(String gEL) {
        this.gEL = gEL; }
    String gEL;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;

    @JsonProperty("INO")
    public String getINO() {
        return this.iNO; }
    public void setINO(String iNO) {
        this.iNO = iNO; }
    String iNO;
    @JsonProperty("Sor")
    public String getSor() {
        return this.sor; }
    public void setSor(String sor) {
        this.sor = sor; }
    String sor;
    @JsonProperty("RHA")
    public String getRHA() {
        return this.rHA; }
    public void setRHA(String rHA) {
        this.rHA = rHA; }
    String rHA;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("MEL")
    public String getMEL() {
        return this.mEL; }
    public void setMEL(String mEL) {
        this.mEL = mEL; }
    String mEL;
    @JsonProperty("AMY")
    public String getAMY() {
        return this.aMY; }
    public void setAMY(String aMY) {
        this.aMY = aMY; }
    String aMY;
    @JsonProperty("ARA")
    public String getARA() {
        return this.aRA; }
    public void setARA(String aRA) {
        this.aRA = aRA; }
    String aRA;
    @JsonProperty("OX")
    public String getOX() {
        return this.oX; }
    public void setOX(String oX) {
        this.oX = oX; }
    String oX;

    @JsonProperty("NO2")
    public String getNO2() {
        return this.nO2; }
    public void setNO2(String nO2) {
        this.nO2 = nO2; }
    String nO2;
    @JsonProperty("N2")
    public String getN2() {
        return this.n2; }
    public void setN2(String n2) {
        this.n2 = n2; }
    String n2;
    @JsonProperty("MAC")
    public String getMAC() {
        return this.mAC; }
    public void setMAC(String mAC) {
        this.mAC = mAC; }
    String mAC;

    @JsonProperty("OF-O")
    public String getOFO() {
        return this.oFO; }
    public void setOFO(String oFO) {
        this.oFO = oFO; }
    String oFO;

    @JsonProperty("OF-F")
    public String getOFF() {
        return this.oFF; }
    public void setOFF(String oFF) {
        this.oFF = oFF; }
    String oFF;

    @JsonProperty("MOB")
    public String getMOB() {
        return this.mOB;
    }
    public void setMOB(String mOB) {
        this.mOB = mOB; }
    String mOB;
}
