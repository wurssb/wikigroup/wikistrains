package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Root {
    @JsonProperty("medium")
    public Medium getMedium() {
        return this.medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    Medium medium;

    @JsonProperty("solutions")
    public ArrayList<Solution> getSolutions() {
        return this.solutions;
    }

    public void setSolutions(ArrayList<Solution> solutions) {
        this.solutions = solutions;
    }

    ArrayList<Solution> solutions;
}
