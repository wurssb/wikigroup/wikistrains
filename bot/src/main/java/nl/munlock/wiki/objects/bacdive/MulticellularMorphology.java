package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MulticellularMorphology{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("forms multicellular complex")
    public String getFormsMulticellularComplex() {
        return this.formsMulticellularComplex; }
    public void setFormsMulticellularComplex(String formsMulticellularComplex) {
        this.formsMulticellularComplex = formsMulticellularComplex; }
    String formsMulticellularComplex;
    @JsonProperty("complex name")
    public String getComplexName() {
        return this.complexName; }
    public void setComplexName(String complexName) {
        this.complexName = complexName; }
    String complexName;
    @JsonProperty("complex color")
    public String getComplexColor() {
        return this.complexColor; }
    public void setComplexColor(String complexColor) {
        this.complexColor = complexColor; }
    String complexColor;
    @JsonProperty("medium name")
    public String getMediumName() {
        return this.mediumName; }
    public void setMediumName(String mediumName) {
        this.mediumName = mediumName; }
    String mediumName;
    @JsonProperty("further description")
    public String getFurtherDescription() {
        return this.furtherDescription; }
    public void setFurtherDescription(String furtherDescription) {
        this.furtherDescription = furtherDescription; }
    String furtherDescription;

    @JsonProperty("complex size")
    public String getComplexSize() {
        return this.complexSize; }
    public void setComplexSize(String complexSize) {
        this.complexSize = complexSize; }
    String complexSize;
}