package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root{
    @JsonProperty("ASSEMBLY_SET")
    public ASSEMBLYSET getASSEMBLY_SET() {
        return this.aSSEMBLY_SET; }
    public void setASSEMBLY_SET(ASSEMBLYSET aSSEMBLY_SET) {
        this.aSSEMBLY_SET = aSSEMBLY_SET; }
    ASSEMBLYSET aSSEMBLY_SET;
}
