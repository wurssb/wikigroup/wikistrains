package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class APICoryne{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("NIT")
    public String getNIT() {
        return this.nIT; }
    public void setNIT(String nIT) {
        this.nIT = nIT; }
    String nIT;
    @JsonProperty("PYZ")
    public String getPYZ() {
        return this.pYZ; }
    public void setPYZ(String pYZ) {
        this.pYZ = pYZ; }
    String pYZ;
    @JsonProperty("PYRA")
    public String getPYRA() {
        return this.pYRA; }
    public void setPYRA(String pYRA) {
        this.pYRA = pYRA; }
    String pYRA;
    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL; }
    public void setPAL(String pAL) {
        this.pAL = pAL; }
    String pAL;
    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR; }
    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR; }
    String betaGUR;
    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL; }
    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL; }
    String betaGAL;
    @JsonProperty("alpha GLU")
    public String getAlphaGLU() {
        return this.alphaGLU; }
    public void setAlphaGLU(String alphaGLU) {
        this.alphaGLU = alphaGLU; }
    String alphaGLU;
    @JsonProperty("beta NAG")
    public String getBetaNAG() {
        return this.betaNAG; }
    public void setBetaNAG(String betaNAG) {
        this.betaNAG = betaNAG; }
    String betaNAG;
    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC; }
    public void setESC(String eSC) {
        this.eSC = eSC; }
    String eSC;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("GEL")
    public String getGEL() {
        return this.gEL; }
    public void setGEL(String gEL) {
        this.gEL = gEL; }
    String gEL;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB; }
    public void setRIB(String rIB) {
        this.rIB = rIB; }
    String rIB;
    @JsonProperty("XYL")
    public String getXYL() {
        return this.xYL; }
    public void setXYL(String xYL) {
        this.xYL = xYL; }
    String xYL;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC; }
    public void setLAC(String lAC) {
        this.lAC = lAC; }
    String lAC;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("GLYG")
    public String getGLYG() {
        return this.gLYG; }
    public void setGLYG(String gLYG) {
        this.gLYG = gLYG; }
    String gLYG;
    @JsonProperty("Control")
    public String getControl() {
        return this.control; }
    public void setControl(String control) {
        this.control = control; }
    String control;
    @JsonProperty("CAT")
    public String getCAT() {
        return this.cAT; }
    public void setCAT(String cAT) {
        this.cAT = cAT; }
    String cAT;
    @JsonProperty("API 20E")
    public List<API20E> getAPI20E() {
        return this.aPI20E; }
    public void setAPI20E(List<API20E> aPI20E) {
        this.aPI20E = aPI20E; }
    List<API20E> aPI20E;


}