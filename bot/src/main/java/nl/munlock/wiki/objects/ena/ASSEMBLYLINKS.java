package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ASSEMBLYLINKS{
    @JsonProperty("ASSEMBLY_LINK")
    public List<ASSEMBLYLINK> getASSEMBLY_LINK() {
		 return this.aSSEMBLY_LINK; } 
    public void setASSEMBLY_LINK(List<ASSEMBLYLINK> aSSEMBLY_LINK) { 
		 this.aSSEMBLY_LINK = aSSEMBLY_LINK; } 
    List<ASSEMBLYLINK> aSSEMBLY_LINK;
}
