package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class General{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("BacDive-ID") 
    public String getBacDiveID() {
		 return this.bacDiveID; } 
    public void setBacDiveID(String bacDiveID) {
		 this.bacDiveID = bacDiveID; } 
    String bacDiveID;
    @JsonProperty("DSM-Number") 
    public int getDSMNumber() { 
		 return this.dSMNumber; } 
    public void setDSMNumber(int dSMNumber) { 
		 this.dSMNumber = dSMNumber; } 
    int dSMNumber;
    @JsonProperty("keywords") 
    public List<String> getKeywords() {
		 return this.keywords; } 
    public void setKeywords(List<String> keywords) { 
		 this.keywords = keywords; } 
    List<String> keywords;
    @JsonProperty("description") 
    public String getDescription() { 
		 return this.description; } 
    public void setDescription(String description) { 
		 this.description = description; } 
    String description;
    @JsonProperty("strain history") 
    public List<String> getStrainHistory() {
		 return this.strainHistory; } 
    public void setStrainHistory(List<String> strainHistory) {
		 this.strainHistory = strainHistory; } 
    List<String> strainHistory;
    @JsonProperty("doi") 
    public String getDoi() { 
		 return this.doi; } 
    public void setDoi(String doi) { 
		 this.doi = doi; } 
    String doi;

    @JsonProperty("NCBI tax id")
    public ArrayList<NCBITaxId> getNCBITaxId() {
        return this.nCBITaxId; }
    public void setNCBITaxId(ArrayList<NCBITaxId> nCBITaxId) {
        this.nCBITaxId = nCBITaxId; }
    ArrayList<NCBITaxId> nCBITaxId;
}

