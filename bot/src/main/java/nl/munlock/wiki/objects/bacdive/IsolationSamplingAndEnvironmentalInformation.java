package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class IsolationSamplingAndEnvironmentalInformation{
    @JsonProperty("isolation")
    public List<Isolation> getIsolation() {
		 return this.isolation; } 
    public void setIsolation(List<Isolation> isolation) { 
		 this.isolation = isolation; } 
    List<Isolation> isolation;
    @JsonProperty("isolation source categories") 
    public List<IsolationSourceCategory> getIsolationSourceCategories() { 
		 return this.isolationSourceCategories; } 
    public void setIsolationSourceCategories(List<IsolationSourceCategory> isolationSourceCategories) { 
		 this.isolationSourceCategories = isolationSourceCategories; } 
    List<IsolationSourceCategory> isolationSourceCategories;
}
