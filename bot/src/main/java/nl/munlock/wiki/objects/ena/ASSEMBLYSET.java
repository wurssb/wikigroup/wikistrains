package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ASSEMBLYSET{
    @JsonProperty("ASSEMBLY")
    public ASSEMBLY getASSEMBLY() { 
		 return this.aSSEMBLY; } 
    public void setASSEMBLY(ASSEMBLY aSSEMBLY) { 
		 this.aSSEMBLY = aSSEMBLY; } 
    ASSEMBLY aSSEMBLY;
}
