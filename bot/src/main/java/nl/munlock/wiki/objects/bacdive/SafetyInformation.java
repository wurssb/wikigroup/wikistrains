package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SafetyInformation{
    @JsonProperty("risk assessment")
    public List<RiskAssessment> getRiskAssessment() {
        return this.riskAssessment; }
    public void setRiskAssessment(List<RiskAssessment> riskAssessment) {
        this.riskAssessment = riskAssessment; }
    List<RiskAssessment> riskAssessment;
}
