package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APIID32STA {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("URE")
    public String getURE() {
        return this.uRE;
    }

    public void setURE(String uRE) {
        this.uRE = uRE;
    }

    String uRE;

    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg;
    }

    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg;
    }

    String aDHArg;

    @JsonProperty("ODC")
    public String getODC() {
        return this.oDC;
    }

    public void setODC(String oDC) {
        this.oDC = oDC;
    }

    String oDC;

    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC;
    }

    public void setESC(String eSC) {
        this.eSC = eSC;
    }

    String eSC;

    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU;
    }

    public void setGLU(String gLU) {
        this.gLU = gLU;
    }

    String gLU;

    @JsonProperty("FRU")
    public String getFRU() {
        return this.fRU;
    }

    public void setFRU(String fRU) {
        this.fRU = fRU;
    }

    String fRU;

    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE;
    }

    public void setMNE(String mNE) {
        this.mNE = mNE;
    }

    String mNE;

    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL;
    }

    public void setMAL(String mAL) {
        this.mAL = mAL;
    }

    String mAL;

    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC;
    }

    public void setLAC(String lAC) {
        this.lAC = lAC;
    }

    String lAC;

    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE;
    }

    public void setTRE(String tRE) {
        this.tRE = tRE;
    }

    String tRE;

    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN;
    }

    public void setMAN(String mAN) {
        this.mAN = mAN;
    }

    String mAN;

    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF;
    }

    public void setRAF(String rAF) {
        this.rAF = rAF;
    }

    String rAF;

    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB;
    }

    public void setRIB(String rIB) {
        this.rIB = rIB;
    }

    String rIB;

    @JsonProperty("CEL")
    public String getCEL() {
        return this.cEL;
    }

    public void setCEL(String cEL) {
        this.cEL = cEL;
    }

    String cEL;

    @JsonProperty("NIT")
    public String getNIT() {
        return this.nIT;
    }

    public void setNIT(String nIT) {
        this.nIT = nIT;
    }

    String nIT;

    @JsonProperty("VP")
    public String getVP() {
        return this.vP;
    }

    public void setVP(String vP) {
        this.vP = vP;
    }

    String vP;

    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL;
    }

    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL;
    }

    String betaGAL;

    @JsonProperty("ArgA")
    public String getArgA() {
        return this.argA;
    }

    public void setArgA(String argA) {
        this.argA = argA;
    }

    String argA;

    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL;
    }

    public void setPAL(String pAL) {
        this.pAL = pAL;
    }

    String pAL;

    @JsonProperty("PyrA")
    public String getPyrA() {
        return this.pyrA;
    }

    public void setPyrA(String pyrA) {
        this.pyrA = pyrA;
    }

    String pyrA;

    @JsonProperty("NOVO")
    public String getNOVO() {
        return this.nOVO;
    }

    public void setNOVO(String nOVO) {
        this.nOVO = nOVO;
    }

    String nOVO;

    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC;
    }

    public void setSAC(String sAC) {
        this.sAC = sAC;
    }

    String sAC;

    @JsonProperty("NAG")
    public String getNAG() {
        return this.nAG;
    }

    public void setNAG(String nAG) {
        this.nAG = nAG;
    }

    String nAG;

    @JsonProperty("TUR")
    public String getTUR() {
        return this.tUR;
    }

    public void setTUR(String tUR) {
        this.tUR = tUR;
    }

    String tUR;

    @JsonProperty("ARA")
    public String getARA() {
        return this.aRA;
    }

    public void setARA(String aRA) {
        this.aRA = aRA;
    }

    String aRA;

    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR;
    }

    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR;
    }

    String betaGUR;
}
