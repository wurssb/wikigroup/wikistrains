package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonProperty;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString, Root.class); */
public class Medium{
    @JsonProperty("id")
    public String getId() {
		 return this.id; } 
    public void setId(String id) {
		 this.id = id; } 
    String id;
    @JsonProperty("name") 
    public String getName() { 
		 return this.name; } 
    public void setName(String name) { 
		 this.name = name; } 
    String name;

    @JsonProperty("description")
    public String getDescription() {
        return this.description; }
    public void setDescription(String description) {
        this.description = description; }
    String description;
    @JsonProperty("complex_medium") 
    public String getComplex_medium() { 
		 return this.complex_medium; } 
    public void setComplex_medium(String complex_medium) { 
		 this.complex_medium = complex_medium; } 
    String complex_medium;
    @JsonProperty("min_pH") 
    public int getMin_pH() { 
		 return this.min_pH; } 
    public void setMin_pH(int min_pH) { 
		 this.min_pH = min_pH; } 
    int min_pH;
    @JsonProperty("max_pH") 
    public int getMax_pH() { 
		 return this.max_pH; } 
    public void setMax_pH(int max_pH) { 
		 this.max_pH = max_pH; } 
    int max_pH;
    @JsonProperty("source") 
    public String getSource() { 
		 return this.source; } 
    public void setSource(String source) { 
		 this.source = source; } 
    String source;
    @JsonProperty("link") 
    public String getLink() { 
		 return this.link; } 
    public void setLink(String link) { 
		 this.link = link; } 
    String link;
}


