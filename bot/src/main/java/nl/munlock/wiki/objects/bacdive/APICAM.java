package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APICAM{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("NIT")
    public String getNIT() {
        return this.nIT; }
    public void setNIT(String nIT) {
        this.nIT = nIT; }
    String nIT;
    @JsonProperty("EST")
    public String getEST() {
        return this.eST; }
    public void setEST(String eST) {
        this.eST = eST; }
    String eST;
    @JsonProperty("HIP")
    public String getHIP() {
        return this.hIP; }
    public void setHIP(String hIP) {
        this.hIP = hIP; }
    String hIP;
    @JsonProperty("GGT")
    public String getGGT() {
        return this.gGT; }
    public void setGGT(String gGT) {
        this.gGT = gGT; }
    String gGT;
    @JsonProperty("TTC")
    public String getTTC() {
        return this.tTC; }
    public void setTTC(String tTC) {
        this.tTC = tTC; }
    String tTC;
    @JsonProperty("PYRA")
    public String getPYRA() {
        return this.pYRA; }
    public void setPYRA(String pYRA) {
        this.pYRA = pYRA; }
    String pYRA;
    @JsonProperty("ArgA")
    public String getArgA() {
        return this.argA; }
    public void setArgA(String argA) {
        this.argA = argA; }
    String argA;
    @JsonProperty("AspA")
    public String getAspA() {
        return this.aspA; }
    public void setAspA(String aspA) {
        this.aspA = aspA; }
    String aspA;
    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL; }
    public void setPAL(String pAL) {
        this.pAL = pAL; }
    String pAL;
    @JsonProperty("H2S")
    public String getH2S() {
        return this.h2S; }
    public void setH2S(String h2S) {
        this.h2S = h2S; }
    String h2S;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("SUT")
    public String getSUT() {
        return this.sUT; }
    public void setSUT(String sUT) {
        this.sUT = sUT; }
    String sUT;
    @JsonProperty("NAL")
    public String getNAL() {
        return this.nAL; }
    public void setNAL(String nAL) {
        this.nAL = nAL; }
    String nAL;
    @JsonProperty("CFZ")
    public String getCFZ() {
        return this.cFZ; }
    public void setCFZ(String cFZ) {
        this.cFZ = cFZ; }
    String cFZ;
    @JsonProperty("ACE")
    public String getACE() {
        return this.aCE; }
    public void setACE(String aCE) {
        this.aCE = aCE; }
    String aCE;
    @JsonProperty("PROP")
    public String getPROP() {
        return this.pROP; }
    public void setPROP(String pROP) {
        this.pROP = pROP; }
    String pROP;
    @JsonProperty("MLT")
    public String getMLT() {
        return this.mLT; }
    public void setMLT(String mLT) {
        this.mLT = mLT; }
    String mLT;
    @JsonProperty("CIT")
    public String getCIT() {
        return this.cIT; }
    public void setCIT(String cIT) {
        this.cIT = cIT; }
    String cIT;
    @JsonProperty("ERO")
    public String getERO() {
        return this.eRO; }
    public void setERO(String eRO) {
        this.eRO = eRO; }
    String eRO;
    @JsonProperty("CAT")
    public String getCAT() {
        return this.cAT; }
    public void setCAT(String cAT) {
        this.cAT = cAT; }
    String cAT;
}