package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class API50CHac {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("Q")
    public String getQ() {
        return this.q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    String q;

    @JsonProperty("GLY")
    public String getGLY() {
        return this.gLY;
    }

    public void setGLY(String gLY) {
        this.gLY = gLY;
    }

    String gLY;

    @JsonProperty("ERY")
    public String getERY() {
        return this.eRY;
    }

    public void setERY(String eRY) {
        this.eRY = eRY;
    }

    String eRY;

    @JsonProperty("DARA")
    public String getDARA() {
        return this.dARA;
    }

    public void setDARA(String dARA) {
        this.dARA = dARA;
    }

    String dARA;

    @JsonProperty("LARA")
    public String getLARA() {
        return this.lARA;
    }

    public void setLARA(String lARA) {
        this.lARA = lARA;
    }

    String lARA;

    @JsonProperty("RIB")
    public String getRIB() {
        return this.rIB;
    }

    public void setRIB(String rIB) {
        this.rIB = rIB;
    }

    String rIB;

    @JsonProperty("DXYL")
    public String getDXYL() {
        return this.dXYL;
    }

    public void setDXYL(String dXYL) {
        this.dXYL = dXYL;
    }

    String dXYL;

    @JsonProperty("LXYL")
    public String getLXYL() {
        return this.lXYL;
    }

    public void setLXYL(String lXYL) {
        this.lXYL = lXYL;
    }

    String lXYL;

    @JsonProperty("ADO")
    public String getADO() {
        return this.aDO;
    }

    public void setADO(String aDO) {
        this.aDO = aDO;
    }

    String aDO;

    @JsonProperty("MDX")
    public String getMDX() {
        return this.mDX;
    }

    public void setMDX(String mDX) {
        this.mDX = mDX;
    }

    String mDX;

    @JsonProperty("GAL")
    public String getGAL() {
        return this.gAL;
    }

    public void setGAL(String gAL) {
        this.gAL = gAL;
    }

    String gAL;

    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU;
    }

    public void setGLU(String gLU) {
        this.gLU = gLU;
    }

    String gLU;

    @JsonProperty("FRU")
    public String getFRU() {
        return this.fRU;
    }

    public void setFRU(String fRU) {
        this.fRU = fRU;
    }

    String fRU;

    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE;
    }

    public void setMNE(String mNE) {
        this.mNE = mNE;
    }

    String mNE;

    @JsonProperty("SBE")
    public String getSBE() {
        return this.sBE;
    }

    public void setSBE(String sBE) {
        this.sBE = sBE;
    }

    String sBE;

    @JsonProperty("RHA")
    public String getRHA() {
        return this.rHA;
    }

    public void setRHA(String rHA) {
        this.rHA = rHA;
    }

    String rHA;

    @JsonProperty("DUL")
    public String getDUL() {
        return this.dUL;
    }

    public void setDUL(String dUL) {
        this.dUL = dUL;
    }

    String dUL;

    @JsonProperty("INO")
    public String getINO() {
        return this.iNO;
    }

    public void setINO(String iNO) {
        this.iNO = iNO;
    }

    String iNO;

    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN;
    }

    public void setMAN(String mAN) {
        this.mAN = mAN;
    }

    String mAN;

    @JsonProperty("SOR")
    public String getSOR() {
        return this.sOR;
    }

    public void setSOR(String sOR) {
        this.sOR = sOR;
    }

    String sOR;

    @JsonProperty("MDM")
    public String getMDM() {
        return this.mDM;
    }

    public void setMDM(String mDM) {
        this.mDM = mDM;
    }

    String mDM;

    @JsonProperty("MDG")
    public String getMDG() {
        return this.mDG;
    }

    public void setMDG(String mDG) {
        this.mDG = mDG;
    }

    String mDG;

    @JsonProperty("NAG")
    public String getNAG() {
        return this.nAG;
    }

    public void setNAG(String nAG) {
        this.nAG = nAG;
    }

    String nAG;

    @JsonProperty("AMY")
    public String getAMY() {
        return this.aMY;
    }

    public void setAMY(String aMY) {
        this.aMY = aMY;
    }

    String aMY;

    @JsonProperty("ARB")
    public String getARB() {
        return this.aRB;
    }

    public void setARB(String aRB) {
        this.aRB = aRB;
    }

    String aRB;

    @JsonProperty("ESC")
    public String getESC() {
        return this.eSC;
    }

    public void setESC(String eSC) {
        this.eSC = eSC;
    }

    String eSC;

    @JsonProperty("SAL")
    public String getSAL() {
        return this.sAL;
    }

    public void setSAL(String sAL) {
        this.sAL = sAL;
    }

    String sAL;

    @JsonProperty("CEL")
    public String getCEL() {
        return this.cEL;
    }

    public void setCEL(String cEL) {
        this.cEL = cEL;
    }

    String cEL;

    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL;
    }

    public void setMAL(String mAL) {
        this.mAL = mAL;
    }

    String mAL;

    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC;
    }

    public void setLAC(String lAC) {
        this.lAC = lAC;
    }

    String lAC;

    @JsonProperty("MEL")
    public String getMEL() {
        return this.mEL;
    }

    public void setMEL(String mEL) {
        this.mEL = mEL;
    }

    String mEL;

    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC;
    }

    public void setSAC(String sAC) {
        this.sAC = sAC;
    }

    String sAC;

    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE;
    }

    public void setTRE(String tRE) {
        this.tRE = tRE;
    }

    String tRE;

    @JsonProperty("INU")
    public String getINU() {
        return this.iNU;
    }

    public void setINU(String iNU) {
        this.iNU = iNU;
    }

    String iNU;

    @JsonProperty("MLZ")
    public String getMLZ() {
        return this.mLZ;
    }

    public void setMLZ(String mLZ) {
        this.mLZ = mLZ;
    }

    String mLZ;

    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF;
    }

    public void setRAF(String rAF) {
        this.rAF = rAF;
    }

    String rAF;

    @JsonProperty("AMD")
    public String getAMD() {
        return this.aMD;
    }

    public void setAMD(String aMD) {
        this.aMD = aMD;
    }

    String aMD;

    @JsonProperty("GLYG")
    public String getGLYG() {
        return this.gLYG;
    }

    public void setGLYG(String gLYG) {
        this.gLYG = gLYG;
    }

    String gLYG;

    @JsonProperty("XLT")
    public String getXLT() {
        return this.xLT;
    }

    public void setXLT(String xLT) {
        this.xLT = xLT;
    }

    String xLT;

    @JsonProperty("GEN")
    public String getGEN() {
        return this.gEN;
    }

    public void setGEN(String gEN) {
        this.gEN = gEN;
    }

    String gEN;

    @JsonProperty("TUR")
    public String getTUR() {
        return this.tUR;
    }

    public void setTUR(String tUR) {
        this.tUR = tUR;
    }

    String tUR;

    @JsonProperty("LYX")
    public String getLYX() {
        return this.lYX;
    }

    public void setLYX(String lYX) {
        this.lYX = lYX;
    }

    String lYX;

    @JsonProperty("TAG")
    public String getTAG() {
        return this.tAG;
    }

    public void setTAG(String tAG) {
        this.tAG = tAG;
    }

    String tAG;

    @JsonProperty("DFUC")
    public String getDFUC() {
        return this.dFUC;
    }

    public void setDFUC(String dFUC) {
        this.dFUC = dFUC;
    }

    String dFUC;

    @JsonProperty("LFUC")
    public String getLFUC() {
        return this.lFUC;
    }

    public void setLFUC(String lFUC) {
        this.lFUC = lFUC;
    }

    String lFUC;

    @JsonProperty("DARL")
    public String getDARL() {
        return this.dARL;
    }

    public void setDARL(String dARL) {
        this.dARL = dARL;
    }

    String dARL;

    @JsonProperty("LARL")
    public String getLARL() {
        return this.lARL;
    }

    public void setLARL(String lARL) {
        this.lARL = lARL;
    }

    String lARL;

    @JsonProperty("GNT")
    public String getGNT() {
        return this.gNT;
    }

    public void setGNT(String gNT) {
        this.gNT = gNT;
    }

    String gNT;

    @JsonProperty("2KG")
    public String get_2KG() {
        return this._2KG;
    }

    public void set_2KG(String _2KG) {
        this._2KG = _2KG;
    }

    String _2KG;

    @JsonProperty("5KG")
    public String get_5KG() {
        return this._5KG;
    }

    public void set_5KG(String _5KG) {
        this._5KG = _5KG;
    }

    String _5KG;
}