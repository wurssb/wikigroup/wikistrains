package nl.munlock.wiki.objects.bacdive.media;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Solution {
    @JsonProperty("id")
    public int getId() {
        return this.id; }
    public void setId(int id) {
        this.id = id; }
    int id;
    @JsonProperty("name")
    public String getName() {
        return this.name; }
    public void setName(String name) {
        this.name = name; }
    String name;
    @JsonProperty("volume")
    public int getVolume() {
        return this.volume; }
    public void setVolume(int volume) {
        this.volume = volume; }
    int volume;
    @JsonProperty("recipe")
    public ArrayList<Recipe> getRecipe() {
        return this.recipe; }
    public void setRecipe(ArrayList<Recipe> recipe) {
        this.recipe = recipe; }
    ArrayList<Recipe> recipe;
    @JsonProperty("steps")
    public ArrayList<Step> getSteps() {
        return this.steps; }
    public void setSteps(ArrayList<Step> steps) {
        this.steps = steps; }
    ArrayList<Step> steps;
    @JsonProperty("main solution")
    public String getMainSolution() {
        return this.mainSolution; }
    public void setMainSolution(String mainSolution) {
        this.mainSolution = mainSolution; }
    String mainSolution;
    @JsonProperty("equipment")
    public Equipment getEquipment() {
        return this.equipment; }
    public void setEquipment(Equipment equipment) {
        this.equipment = equipment; }
    Equipment equipment;
    @JsonProperty("Sol. No.")
    public int getSolNo() {
        return this.solNo; }
    public void setSolNo(int solNo) {
        this.solNo = solNo; }
    int solNo;

}

