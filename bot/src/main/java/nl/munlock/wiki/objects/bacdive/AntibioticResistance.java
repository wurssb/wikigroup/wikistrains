package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AntibioticResistance{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("ChEBI")
    public int getChEBI() {
        return this.chEBI; }
    public void setChEBI(int chEBI) {
        this.chEBI = chEBI; }
    int chEBI;
    @JsonProperty("metabolite")
    public String getMetabolite() {
        return this.metabolite; }
    public void setMetabolite(String metabolite) {
        this.metabolite = metabolite; }
    String metabolite;
    @JsonProperty("is antibiotic")
    public String getIsAntibiotic() {
        return this.isAntibiotic; }
    public void setIsAntibiotic(String isAntibiotic) {
        this.isAntibiotic = isAntibiotic; }
    String isAntibiotic;
    @JsonProperty("is sensitive")
    public String getIsSensitive() {
        return this.isSensitive; }
    public void setIsSensitive(String isSensitive) {
        this.isSensitive = isSensitive; }
    String isSensitive;
    @JsonProperty("sensitivity conc.")
    public String getSensitivityConc() {
        return this.sensitivityConc; }
    public void setSensitivityConc(String sensitivityConc) {
        this.sensitivityConc = sensitivityConc; }
    String sensitivityConc;
    @JsonProperty("is resistant")
    public String getIsResistant() {
        return this.isResistant; }
    public void setIsResistant(String isResistant) {
        this.isResistant = isResistant; }
    String isResistant;
    @JsonProperty("resistance conc.")
    public String getResistanceConc() {
        return this.resistanceConc; }
    public void setResistanceConc(String resistanceConc) {
        this.resistanceConc = resistanceConc; }
    String resistanceConc;

    @JsonProperty("group ID")
    public int getGroupID() {
        return this.groupID; }
    public void setGroupID(int groupID) {
        this.groupID = groupID; }
    int groupID;

    @JsonProperty("is intermediate")
    public String getIsIntermediate() {
        return this.isIntermediate; }
    public void setIsIntermediate(String isIntermediate) {
        this.isIntermediate = isIntermediate; }
    String isIntermediate;

    @JsonProperty("intermediate conc.")
    public String getIntermediateConc() {
        return this.intermediateConc; }
    public void setIntermediateConc(String intermediateConc) {
        this.intermediateConc = intermediateConc; }
    String intermediateConc;

}