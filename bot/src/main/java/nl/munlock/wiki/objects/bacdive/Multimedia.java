package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Multimedia{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("multimedia content")
    public String getMultimediaContent() {
        return this.multimediaContent; }
    public void setMultimediaContent(String multimediaContent) {
        this.multimediaContent = multimediaContent; }
    String multimediaContent;
    @JsonProperty("caption")
    public String getCaption() {
        return this.caption; }
    public void setCaption(String caption) {
        this.caption = caption; }
    String caption;
    @JsonProperty("license or copyright")
    public String getLicenseOrCopyright() {
        return this.licenseOrCopyright; }
    public void setLicenseOrCopyright(String licenseOrCopyright) {
        this.licenseOrCopyright = licenseOrCopyright; }
    String licenseOrCopyright;
    @JsonProperty("intellectual property rights")
    public String getIntellectualPropertyRights() {
        return this.intellectualPropertyRights; }
    public void setIntellectualPropertyRights(String intellectualPropertyRights) {
        this.intellectualPropertyRights = intellectualPropertyRights; }
    String intellectualPropertyRights;
}