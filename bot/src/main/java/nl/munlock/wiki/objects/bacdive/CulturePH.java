package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CulturePH{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("ability")
    public String getAbility() {
        return this.ability; }
    public void setAbility(String ability) {
        this.ability = ability; }
    String ability;
    @JsonProperty("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
    @JsonProperty("pH")
    public String getPH() {
        return this.pH; }
    public void setPH(String pH) {
        this.pH = pH; }
    String pH;
}