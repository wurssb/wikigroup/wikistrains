package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APIID32E{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("ODC")
    public String getODC() {
        return this.oDC; }
    public void setODC(String oDC) {
        this.oDC = oDC; }
    String oDC;
    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg; }
    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg; }
    String aDHArg;
    @JsonProperty("LDC Lys")
    public String getLDCLys() {
        return this.lDCLys; }
    public void setLDCLys(String lDCLys) {
        this.lDCLys = lDCLys; }
    String lDCLys;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("LARL")
    public String getLARL() {
        return this.lARL; }
    public void setLARL(String lARL) {
        this.lARL = lARL; }
    String lARL;
    @JsonProperty("GAT")
    public String getGAT() {
        return this.gAT; }
    public void setGAT(String gAT) {
        this.gAT = gAT; }
    String gAT;
    @JsonProperty("5KG")
    public String get_5KG() {
        return this._5KG; }
    public void set_5KG(String _5KG) {
        this._5KG = _5KG; }
    String _5KG;
    @JsonProperty("LIP")
    public String getLIP() {
        return this.lIP; }
    public void setLIP(String lIP) {
        this.lIP = lIP; }
    String lIP;
    @JsonProperty("RP")
    public String getRP() {
        return this.rP; }
    public void setRP(String rP) {
        this.rP = rP; }
    String rP;
    @JsonProperty("beta GLU")
    public String getBetaGLU() {
        return this.betaGLU; }
    public void setBetaGLU(String betaGLU) {
        this.betaGLU = betaGLU; }
    String betaGLU;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("ADO")
    public String getADO() {
        return this.aDO; }
    public void setADO(String aDO) {
        this.aDO = aDO; }
    String aDO;
    @JsonProperty("PLE")
    public String getPLE() {
        return this.pLE; }
    public void setPLE(String pLE) {
        this.pLE = pLE; }
    String pLE;
    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR; }
    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR; }
    String betaGUR;
    @JsonProperty("MNT")
    public String getMNT() {
        return this.mNT; }
    public void setMNT(String mNT) {
        this.mNT = mNT; }
    String mNT;
    @JsonProperty("IND")
    public String getIND() {
        return this.iND; }
    public void setIND(String iND) {
        this.iND = iND; }
    String iND;
    @JsonProperty("beta NAG")
    public String getBetaNAG() {
        return this.betaNAG; }
    public void setBetaNAG(String betaNAG) {
        this.betaNAG = betaNAG; }
    String betaNAG;
    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL; }
    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL; }
    String betaGAL;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("LARA")
    public String getLARA() {
        return this.lARA; }
    public void setLARA(String lARA) {
        this.lARA = lARA; }
    String lARA;
    @JsonProperty("DARL")
    public String getDARL() {
        return this.dARL; }
    public void setDARL(String dARL) {
        this.dARL = dARL; }
    String dARL;
    @JsonProperty("alpha GLU")
    public String getAlphaGLU() {
        return this.alphaGLU; }
    public void setAlphaGLU(String alphaGLU) {
        this.alphaGLU = alphaGLU; }
    String alphaGLU;
    @JsonProperty("alpha GAL")
    public String getAlphaGAL() {
        return this.alphaGAL; }
    public void setAlphaGAL(String alphaGAL) {
        this.alphaGAL = alphaGAL; }
    String alphaGAL;
    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE; }
    public void setTRE(String tRE) {
        this.tRE = tRE; }
    String tRE;
    @JsonProperty("RHA")
    public String getRHA() {
        return this.rHA; }
    public void setRHA(String rHA) {
        this.rHA = rHA; }
    String rHA;
    @JsonProperty("INO")
    public String getINO() {
        return this.iNO; }
    public void setINO(String iNO) {
        this.iNO = iNO; }
    String iNO;
    @JsonProperty("CEL")
    public String getCEL() {
        return this.cEL; }
    public void setCEL(String cEL) {
        this.cEL = cEL; }
    String cEL;
    @JsonProperty("SOR")
    public String getSOR() {
        return this.sOR; }
    public void setSOR(String sOR) {
        this.sOR = sOR; }
    String sOR;
    @JsonProperty("alphaMAL")
    public String getAlphaMAL() {
        return this.alphaMAL; }
    public void setAlphaMAL(String alphaMAL) {
        this.alphaMAL = alphaMAL; }
    String alphaMAL;
    @JsonProperty("AspA")
    public String getAspA() {
        return this.aspA; }
    public void setAspA(String aspA) {
        this.aspA = aspA; }
    String aspA;
}