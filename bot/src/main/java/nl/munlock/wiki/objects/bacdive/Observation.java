package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Observation {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("observation")
    public String getObservation() {
        return this.observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    String observation;
}
