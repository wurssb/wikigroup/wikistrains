package nl.munlock.wiki.objects;

public class Genome {
    private String accession;
    private String taxonID;
    private String assemblyTitle;
    private String assemblyName;
    private String assemblyLevel;
    private String assemblyType;
    private int genomeSize;
    private String sampleAccession;
    private String scientificName;
    private String studyName;
    private String studyTitle;
    private String version;
    private String studyAccession;
    private String studyDescription;
    private String genomeRepresentation;
    private String DSM;
    private String ATCC;

    public void setAccession(String accession) {
        this.accession = accession;
    }

    public String getAccession() {
        return accession;
    }

    public void setTaxonID(String taxonID) {
        this.taxonID = taxonID;
    }

    public String getTaxonID() {
        return taxonID;
    }

    public void setAssemblyTitle(String assemblyTitle) {

        this.assemblyTitle = assemblyTitle;
    }

    public String getAssemblyTitle() {
        return assemblyTitle;
    }

    public void setAssemblyName(String assemblyName) {

        this.assemblyName = assemblyName;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public void setAssemblyLevel(String assemblyLevel) {
        this.assemblyLevel = assemblyLevel;
    }

    public String getAssemblyLevel() {
        return assemblyLevel;
    }

    public void setAssemblyType(String assemblyType) {

        this.assemblyType = assemblyType;
    }

    public String getAssemblyType() {
        return assemblyType;
    }

    public void setGenomeSize(int genomeSize) {
        this.genomeSize = genomeSize;
    }

    public int getGenomeSize() {
        return genomeSize;
    }

    public void setSampleAccession(String sampleAccession) {
        this.sampleAccession = sampleAccession;
    }

    public String getSampleAccession() {
        return sampleAccession;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getStudyName() {
        return studyName;
    }

    public void setStudyTitle(String studyTitle) {
        this.studyTitle = studyTitle;
    }

    public String getStudyTitle() {
        return studyTitle;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setStudyAccession(String studyAccession) {
        this.studyAccession = studyAccession;
    }

    public String getStudyAccession() {
        return studyAccession;
    }

    public void setStudyDescription(String studyDescription) {
        this.studyDescription = studyDescription;
    }

    public String getStudyDescription() {
        return studyDescription;
    }

    public void setGenomeRepresentation(String genomeRepresentation) {
        this.genomeRepresentation = genomeRepresentation;
    }

    public String getGenomeRepresentation() {
        return genomeRepresentation;
    }

    public String getDSM() {
        return DSM;
    }

    public void setDSM(String dsm) {
        this.DSM = dsm;
    }

    public void setATCC(String atcc) {
        this.ATCC = atcc;
    }

    public String getATCC() {
        return ATCC;
    }
}
