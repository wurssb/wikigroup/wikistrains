package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenomeSequence{
    @JsonProperty("@ref")
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("description") 
    public String getDescription() { 
		 return this.description; } 
    public void setDescription(String description) { 
		 this.description = description; } 
    String description;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("assembly level") 
    public String getAssemblyLevel() { 
		 return this.assemblyLevel; } 
    public void setAssemblyLevel(String assemblyLevel) { 
		 this.assemblyLevel = assemblyLevel; } 
    String assemblyLevel;
    @JsonProperty("database") 
    public String getDatabase() { 
		 return this.database; } 
    public void setDatabase(String database) { 
		 this.database = database; } 
    String database;
    @JsonProperty("NCBI tax ID") 
    public int getNCBITaxID() { 
		 return this.nCBITaxID; } 
    public void setNCBITaxID(int nCBITaxID) { 
		 this.nCBITaxID = nCBITaxID; } 
    int nCBITaxID;
}
