package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LPSN{
    @JsonProperty("@ref") 
    public int getRef() { 
		 return this.ref; } 
    public void setRef(int ref) { 
		 this.ref = ref; } 
    int ref;
    @JsonProperty("description") 
    public String getDescription() { 
		 return this.description; } 
    public void setDescription(String description) { 
		 this.description = description; } 
    String description;
    @JsonProperty("keyword")
    public String getKeyword() { 
		 return this.keyword; } 
    public void setKeyword(String keyword) { 
		 this.keyword = keyword; } 
    String keyword;
    @JsonProperty("domain") 
    public String getDomain() { 
		 return this.domain; } 
    public void setDomain(String domain) { 
		 this.domain = domain; } 
    String domain;
    @JsonProperty("phylum") 
    public String getPhylum() { 
		 return this.phylum; } 
    public void setPhylum(String phylum) { 
		 this.phylum = phylum; } 
    String phylum;
    @JsonProperty("class")
    public String getClazz() {
		 return this.clazz; }
    public void setClass(String clazz) {
		 this.clazz = clazz; }
    String clazz;
    @JsonProperty("order")
    public String getOrder() { 
		 return this.order; } 
    public void setOrder(String order) { 
		 this.order = order; } 
    String order;
    @JsonProperty("family") 
    public String getFamily() { 
		 return this.family; } 
    public void setFamily(String family) { 
		 this.family = family; } 
    String family;
    @JsonProperty("genus") 
    public String getGenus() { 
		 return this.genus; } 
    public void setGenus(String genus) { 
		 this.genus = genus; } 
    String genus;
    @JsonProperty("species") 
    public String getSpecies() { 
		 return this.species; } 
    public void setSpecies(String species) { 
		 this.species = species; } 
    String species;
    @JsonProperty("full scientific name") 
    public String getFullScientificName() { 
		 return this.fullScientificName; } 
    public void setFullScientificName(String fullScientificName) { 
		 this.fullScientificName = fullScientificName; } 
    String fullScientificName;
    @JsonProperty("synonyms")
    public List<Synonym> getSynonyms() {
        return this.synonyms; }
    public void setSynonyms(List<Synonym> synonyms) {
        this.synonyms = synonyms; }
    List<Synonym> synonyms;

}
