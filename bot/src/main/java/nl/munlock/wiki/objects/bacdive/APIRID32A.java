package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APIRID32A {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("URE")
    public String getURE() {
        return this.uRE;
    }

    public void setURE(String uRE) {
        this.uRE = uRE;
    }

    String uRE;

    @JsonProperty("ADH Arg")
    public String getADHArg() {
        return this.aDHArg;
    }

    public void setADHArg(String aDHArg) {
        this.aDHArg = aDHArg;
    }

    String aDHArg;

    @JsonProperty("alpha GAL")
    public String getAlphaGAL() {
        return this.alphaGAL;
    }

    public void setAlphaGAL(String alphaGAL) {
        this.alphaGAL = alphaGAL;
    }

    String alphaGAL;

    @JsonProperty("beta GAL")
    public String getBetaGAL() {
        return this.betaGAL;
    }

    public void setBetaGAL(String betaGAL) {
        this.betaGAL = betaGAL;
    }

    String betaGAL;

    @JsonProperty("beta GP")
    public String getBetaGP() {
        return this.betaGP;
    }

    public void setBetaGP(String betaGP) {
        this.betaGP = betaGP;
    }

    String betaGP;

    @JsonProperty("alpha GLU")
    public String getAlphaGLU() {
        return this.alphaGLU;
    }

    public void setAlphaGLU(String alphaGLU) {
        this.alphaGLU = alphaGLU;
    }

    String alphaGLU;

    @JsonProperty("beta GLU")
    public String getBetaGLU() {
        return this.betaGLU;
    }

    public void setBetaGLU(String betaGLU) {
        this.betaGLU = betaGLU;
    }

    String betaGLU;

    @JsonProperty("alpha ARA")
    public String getAlphaARA() {
        return this.alphaARA;
    }

    public void setAlphaARA(String alphaARA) {
        this.alphaARA = alphaARA;
    }

    String alphaARA;

    @JsonProperty("beta GUR")
    public String getBetaGUR() {
        return this.betaGUR;
    }

    public void setBetaGUR(String betaGUR) {
        this.betaGUR = betaGUR;
    }

    String betaGUR;

    @JsonProperty("beta NAG")
    public String getBetaNAG() {
        return this.betaNAG;
    }

    public void setBetaNAG(String betaNAG) {
        this.betaNAG = betaNAG;
    }

    String betaNAG;

    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE;
    }

    public void setMNE(String mNE) {
        this.mNE = mNE;
    }

    String mNE;

    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF;
    }

    public void setRAF(String rAF) {
        this.rAF = rAF;
    }

    String rAF;

    @JsonProperty("GDC")
    public String getGDC() {
        return this.gDC;
    }

    public void setGDC(String gDC) {
        this.gDC = gDC;
    }

    String gDC;

    @JsonProperty("alpha FUC")
    public String getAlphaFUC() {
        return this.alphaFUC;
    }

    public void setAlphaFUC(String alphaFUC) {
        this.alphaFUC = alphaFUC;
    }

    String alphaFUC;

    @JsonProperty("NIT")
    public String getNIT() {
        return this.nIT;
    }

    public void setNIT(String nIT) {
        this.nIT = nIT;
    }

    String nIT;

    @JsonProperty("IND")
    public String getIND() {
        return this.iND;
    }

    public void setIND(String iND) {
        this.iND = iND;
    }

    String iND;

    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL;
    }

    public void setPAL(String pAL) {
        this.pAL = pAL;
    }

    String pAL;

    @JsonProperty("ArgA")
    public String getArgA() {
        return this.argA;
    }

    public void setArgA(String argA) {
        this.argA = argA;
    }

    String argA;

    @JsonProperty("ProA")
    public String getProA() {
        return this.proA;
    }

    public void setProA(String proA) {
        this.proA = proA;
    }

    String proA;

    @JsonProperty("LGA")
    public String getLGA() {
        return this.lGA;
    }

    public void setLGA(String lGA) {
        this.lGA = lGA;
    }

    String lGA;

    @JsonProperty("PheA")
    public String getPheA() {
        return this.pheA;
    }

    public void setPheA(String pheA) {
        this.pheA = pheA;
    }

    String pheA;

    @JsonProperty("LeuA")
    public String getLeuA() {
        return this.leuA;
    }

    public void setLeuA(String leuA) {
        this.leuA = leuA;
    }

    String leuA;

    @JsonProperty("PyrA")
    public String getPyrA() {
        return this.pyrA;
    }

    public void setPyrA(String pyrA) {
        this.pyrA = pyrA;
    }

    String pyrA;

    @JsonProperty("TyrA")
    public String getTyrA() {
        return this.tyrA;
    }

    public void setTyrA(String tyrA) {
        this.tyrA = tyrA;
    }

    String tyrA;

    @JsonProperty("AlaA")
    public String getAlaA() {
        return this.alaA;
    }

    public void setAlaA(String alaA) {
        this.alaA = alaA;
    }

    String alaA;

    @JsonProperty("GlyA")
    public String getGlyA() {
        return this.glyA;
    }

    public void setGlyA(String glyA) {
        this.glyA = glyA;
    }

    String glyA;

    @JsonProperty("HisA")
    public String getHisA() {
        return this.hisA;
    }

    public void setHisA(String hisA) {
        this.hisA = hisA;
    }

    String hisA;

    @JsonProperty("GGA")
    public String getGGA() {
        return this.gGA;
    }

    public void setGGA(String gGA) {
        this.gGA = gGA;
    }

    String gGA;

    @JsonProperty("SerA")
    public String getSerA() {
        return this.serA;
    }

    public void setSerA(String serA) {
        this.serA = serA;
    }

    String serA;
}
