package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Murein{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("murein short key")
    public String getMureinShortKey() {
        return this.mureinShortKey; }
    public void setMureinShortKey(String mureinShortKey) {
        this.mureinShortKey = mureinShortKey; }
    String mureinShortKey;
    @JsonProperty("type")
    public String getType() {
        return this.type; }
    public void setType(String type) {
        this.type = type; }
    String type;
}
