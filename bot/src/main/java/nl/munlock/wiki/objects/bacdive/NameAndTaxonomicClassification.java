package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NameAndTaxonomicClassification {
    @JsonProperty("LPSN")
    public LPSN getLPSN() {
        return this.lPSN;
    }

    public void setLPSN(LPSN lPSN) {
        this.lPSN = lPSN;
    }

    LPSN lPSN;

    @JsonProperty("strain designation")
    public String getStrainDesignation() {
        return this.strainDesignation; }
    public void setStrainDesignation(String strainDesignation) {
        this.strainDesignation = strainDesignation; }
    String strainDesignation;

    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("domain")
    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    String domain;

    @JsonProperty("phylum")
    public String getPhylum() {
        return this.phylum;
    }

    public void setPhylum(String phylum) {
        this.phylum = phylum;
    }

    String phylum;

    @JsonProperty("class")
    public String getClazz() {
        return this.clazz;
    }

    public void setClass(String clazz) {
        this.clazz = clazz;
    }

    String clazz;

    @JsonProperty("order")
    public String getOrder() {
        return this.order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    String order;

    @JsonProperty("family")
    public String getFamily() {
        return this.family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    String family;

    @JsonProperty("genus")
    public String getGenus() {
        return this.genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    String genus;

    @JsonProperty("species")
    public String getSpecies() {
        return this.species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    String species;

    @JsonProperty("full scientific name")
    public String getFullScientificName() {
        return this.fullScientificName;
    }

    public void setFullScientificName(String fullScientificName) {
        this.fullScientificName = fullScientificName;
    }

    String fullScientificName;

    @JsonProperty("type strain")
    public String getTypeStrain() {
        return this.typeStrain;
    }

    public void setTypeStrain(String typeStrain) {
        this.typeStrain = typeStrain;
    }

    String typeStrain;

    @JsonProperty("variant")
    public String getVariant() {
        return this.variant; }
    public void setVariant(String variant) {
        this.variant = variant; }
    String variant;

}
