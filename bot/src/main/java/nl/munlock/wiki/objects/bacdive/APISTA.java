package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class APISTA{
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref; }
    public void setRef(int ref) {
        this.ref = ref; }
    int ref;
    @JsonProperty("GLU")
    public String getGLU() {
        return this.gLU; }
    public void setGLU(String gLU) {
        this.gLU = gLU; }
    String gLU;
    @JsonProperty("FRU")
    public String getFRU() {
        return this.fRU; }
    public void setFRU(String fRU) {
        this.fRU = fRU; }
    String fRU;
    @JsonProperty("MNE")
    public String getMNE() {
        return this.mNE; }
    public void setMNE(String mNE) {
        this.mNE = mNE; }
    String mNE;
    @JsonProperty("MAL")
    public String getMAL() {
        return this.mAL; }
    public void setMAL(String mAL) {
        this.mAL = mAL; }
    String mAL;
    @JsonProperty("LAC")
    public String getLAC() {
        return this.lAC; }
    public void setLAC(String lAC) {
        this.lAC = lAC; }
    String lAC;
    @JsonProperty("TRE")
    public String getTRE() {
        return this.tRE; }
    public void setTRE(String tRE) {
        this.tRE = tRE; }
    String tRE;
    @JsonProperty("MAN")
    public String getMAN() {
        return this.mAN; }
    public void setMAN(String mAN) {
        this.mAN = mAN; }
    String mAN;
    @JsonProperty("XLT")
    public String getXLT() {
        return this.xLT; }
    public void setXLT(String xLT) {
        this.xLT = xLT; }
    String xLT;
    @JsonProperty("MEL")
    public String getMEL() {
        return this.mEL; }
    public void setMEL(String mEL) {
        this.mEL = mEL; }
    String mEL;
    @JsonProperty("NIT")
    public String getNIT() {
        return this.nIT; }
    public void setNIT(String nIT) {
        this.nIT = nIT; }
    String nIT;
    @JsonProperty("PAL")
    public String getPAL() {
        return this.pAL; }
    public void setPAL(String pAL) {
        this.pAL = pAL; }
    String pAL;
    @JsonProperty("VP")
    public String getVP() {
        return this.vP; }
    public void setVP(String vP) {
        this.vP = vP; }
    String vP;
    @JsonProperty("RAF")
    public String getRAF() {
        return this.rAF; }
    public void setRAF(String rAF) {
        this.rAF = rAF; }
    String rAF;
    @JsonProperty("XYL")
    public String getXYL() {
        return this.xYL; }
    public void setXYL(String xYL) {
        this.xYL = xYL; }
    String xYL;
    @JsonProperty("SAC")
    public String getSAC() {
        return this.sAC; }
    public void setSAC(String sAC) {
        this.sAC = sAC; }
    String sAC;
    @JsonProperty("MDG")
    public String getMDG() {
        return this.mDG; }
    public void setMDG(String mDG) {
        this.mDG = mDG; }
    String mDG;
    @JsonProperty("NAG")
    public String getNAG() {
        return this.nAG; }
    public void setNAG(String nAG) {
        this.nAG = nAG; }
    String nAG;
    @JsonProperty("ADH")
    public String getADH() {
        return this.aDH; }
    public void setADH(String aDH) {
        this.aDH = aDH; }
    String aDH;
    @JsonProperty("URE")
    public String getURE() {
        return this.uRE; }
    public void setURE(String uRE) {
        this.uRE = uRE; }
    String uRE;
    @JsonProperty("LSTR")
    public String getLSTR() {
        return this.lSTR; }
    public void setLSTR(String lSTR) {
        this.lSTR = lSTR; }
    String lSTR;

    @JsonProperty("Control")
    public String getControl() {
        return this.control; }
    public void setControl(String control) {
        this.control = control; }
    String control;

}