package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ASSEMBLY{
    @JsonProperty("WGS_SET")
    public List<WGSSET> getWGS_SET() {
        return this.wGS_SET; }
    public void setWGS_SET(List<WGSSET> wGS_SET) {
        this.wGS_SET = wGS_SET; }
    List<WGSSET> wGS_SET;
    @JsonProperty("GENOME_REPRESENTATION")
    public String getGENOME_REPRESENTATION() { 
		 return this.gENOME_REPRESENTATION; } 
    public void setGENOME_REPRESENTATION(String gENOME_REPRESENTATION) { 
		 this.gENOME_REPRESENTATION = gENOME_REPRESENTATION; } 
    String gENOME_REPRESENTATION;
    @JsonProperty("IDENTIFIERS") 
    public IDENTIFIERS getIDENTIFIERS() {
		 return this.iDENTIFIERS; } 
    public void setIDENTIFIERS(IDENTIFIERS iDENTIFIERS) {
		 this.iDENTIFIERS = iDENTIFIERS; } 
    IDENTIFIERS iDENTIFIERS;
    @JsonProperty("accession") 
    public String getAccession() { 
		 return this.accession; } 
    public void setAccession(String accession) { 
		 this.accession = accession; } 
    String accession;
    @JsonProperty("CHROMOSOMES") 
    public CHROMOSOMES getCHROMOSOMES() {
		 return this.cHROMOSOMES; } 
    public void setCHROMOSOMES(CHROMOSOMES cHROMOSOMES) {
		 this.cHROMOSOMES = cHROMOSOMES; } 
    CHROMOSOMES cHROMOSOMES;
    @JsonProperty("ASSEMBLY_ATTRIBUTES") 
    public ASSEMBLYATTRIBUTES getASSEMBLY_ATTRIBUTES() {
		 return this.aSSEMBLY_ATTRIBUTES; } 
    public void setASSEMBLY_ATTRIBUTES(ASSEMBLYATTRIBUTES aSSEMBLY_ATTRIBUTES) { 
		 this.aSSEMBLY_ATTRIBUTES = aSSEMBLY_ATTRIBUTES; } 
    ASSEMBLYATTRIBUTES aSSEMBLY_ATTRIBUTES;
    @JsonProperty("NAME") 
    public String getNAME() { 
		 return this.nAME; } 
    public void setNAME(String nAME) { 
		 this.nAME = nAME; } 
    String nAME;
    @JsonProperty("center_name") 
    public String getCenter_name() { 
		 return this.center_name; } 
    public void setCenter_name(String center_name) { 
		 this.center_name = center_name; } 
    String center_name;
    @JsonProperty("TAXON") 
    public TAXON getTAXON() {
		 return this.tAXON; } 
    public void setTAXON(TAXON tAXON) { 
		 this.tAXON = tAXON; } 
    TAXON tAXON;
    @JsonProperty("DESCRIPTION") 
    public String getDESCRIPTION() { 
		 return this.dESCRIPTION; } 
    public void setDESCRIPTION(String dESCRIPTION) { 
		 this.dESCRIPTION = dESCRIPTION; } 
    String dESCRIPTION;
    @JsonProperty("ASSEMBLY_LINKS") 
    public ASSEMBLYLINKS getASSEMBLY_LINKS() { 
		 return this.aSSEMBLY_LINKS; } 
    public void setASSEMBLY_LINKS(ASSEMBLYLINKS aSSEMBLY_LINKS) { 
		 this.aSSEMBLY_LINKS = aSSEMBLY_LINKS; } 
    ASSEMBLYLINKS aSSEMBLY_LINKS;
    @JsonProperty("ASSEMBLY_LEVEL") 
    public String getASSEMBLY_LEVEL() { 
		 return this.aSSEMBLY_LEVEL; } 
    public void setASSEMBLY_LEVEL(String aSSEMBLY_LEVEL) { 
		 this.aSSEMBLY_LEVEL = aSSEMBLY_LEVEL; } 
    String aSSEMBLY_LEVEL;
    @JsonProperty("alias") 
    public String getAlias() { 
		 return this.alias; } 
    public void setAlias(String alias) { 
		 this.alias = alias; } 
    String alias;
    @JsonProperty("TITLE") 
    public String getTITLE() { 
		 return this.tITLE; } 
    public void setTITLE(String tITLE) { 
		 this.tITLE = tITLE; } 
    String tITLE;
    @JsonProperty("SAMPLE_REF") 
    public SAMPLEREF getSAMPLE_REF() {
		 return this.sAMPLE_REF; } 
    public void setSAMPLE_REF(SAMPLEREF sAMPLE_REF) { 
		 this.sAMPLE_REF = sAMPLE_REF; } 
    SAMPLEREF sAMPLE_REF;
    @JsonProperty("STUDY_REF") 
    public STUDYREF getSTUDY_REF() { 
		 return this.sTUDY_REF; } 
    public void setSTUDY_REF(STUDYREF sTUDY_REF) { 
		 this.sTUDY_REF = sTUDY_REF; } 
    STUDYREF sTUDY_REF;
}
