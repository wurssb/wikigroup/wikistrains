package nl.munlock.wiki.objects.bacdive;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tolerance {
    @JsonProperty("@ref")
    public int getRef() {
        return this.ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    int ref;

    @JsonProperty("compound")
    public String getCompound() {
        return this.compound;
    }

    public void setCompound(String compound) {
        this.compound = compound;
    }

    String compound;

    @JsonProperty("percentage")
    public String getPercentage() {
        return this.percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    String percentage;

    @JsonProperty("concentration")
    public String getConcentration() {
        return this.concentration; }
    public void setConcentration(String concentration) {
        this.concentration = concentration; }
    String concentration;

}
