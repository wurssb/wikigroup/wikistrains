package nl.munlock.wiki.objects.ena;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TAXON{
    @JsonProperty("TAXON_ID")
    public int getTAXON_ID() {
        return this.tAXON_ID; }
    public void setTAXON_ID(int tAXON_ID) {
        this.tAXON_ID = tAXON_ID; }
    int tAXON_ID;
    @JsonProperty("SCIENTIFIC_NAME")
    public String getSCIENTIFIC_NAME() {
        return this.sCIENTIFIC_NAME; }
    public void setSCIENTIFIC_NAME(String sCIENTIFIC_NAME) {
        this.sCIENTIFIC_NAME = sCIENTIFIC_NAME; }
    String sCIENTIFIC_NAME;
    @JsonProperty("COMMON_NAME")
    public String getCOMMON_NAME() {
        return this.cOMMON_NAME; }
    public void setCOMMON_NAME(String cOMMON_NAME) {
        this.cOMMON_NAME = cOMMON_NAME; }
    String cOMMON_NAME;
    @JsonProperty("STRAIN")
    public String getSTRAIN() {
        return this.sTRAIN; }
    public void setSTRAIN(String sTRAIN) {
        this.sTRAIN = sTRAIN; }
    String sTRAIN;
}
