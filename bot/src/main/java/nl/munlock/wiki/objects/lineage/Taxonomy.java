package nl.munlock.wiki.objects.lineage;

public class Taxonomy {
    private String name;
    private int taxid;
    private Rank rank;
    private int parentTaxID;
    private String EMBLCode;
    private String divisionID;
    private String inheritedDIVFlag;
    private String geneticCodeID;
    private String inheritedGCFlag;
    private String mitochondrialGeneticCodeID;
    private String inheritedMGCFlag;
    private String genBankHiddenFlag;
    private String hiddenSubtreeRootFlag;
    private String comments;
    private String scientificName;
    private String wikiBaseItemId;

    public void setRank(String rank) {
        rank = rank.toUpperCase().replaceAll(" ","_").trim().strip();
        this.rank = Rank.valueOf(rank);
    }

    public void setParentTaxID(int parentTaxID) {
        this.parentTaxID = parentTaxID;
    }

    public int getParentTaxID() {
        return parentTaxID;
    }

    public void setEMBLCode(String emblCode) {
        this.EMBLCode = emblCode;
    }

    public String getEMBLCode() {
        return EMBLCode;
    }

    public void setDivisionID(String divisionID) {
        this.divisionID = divisionID;
    }

    public String getDivisionID() {
        return divisionID;
    }

    public void setInheritedDIVFlag(String inheritedDIVFlag) {
        this.inheritedDIVFlag = inheritedDIVFlag;
    }

    public String getInheritedDIVFlag() {
        return inheritedDIVFlag;
    }

    public void setGeneticCodeID(String geneticCodeID) {

        this.geneticCodeID = geneticCodeID;
    }

    public String getGeneticCodeID() {
        return geneticCodeID;
    }

    public void setInheritedGCFlag(String inheritedGCFlag) {
        this.inheritedGCFlag = inheritedGCFlag;
    }

    public String getInheritedGCFlag() {
        return inheritedGCFlag;
    }

    public void setMitochondrialGeneticCodeID(String mitochondrialGeneticCodeID) {
        this.mitochondrialGeneticCodeID = mitochondrialGeneticCodeID;
    }

    public String getMitochondrialGeneticCodeID() {
        return mitochondrialGeneticCodeID;
    }

    public void setInheritedMGCFlag(String inheritedMGCFlag) {
        this.inheritedMGCFlag = inheritedMGCFlag;
    }

    public String getInheritedMGCFlag() {
        return inheritedMGCFlag;
    }

    public void setGenBankHiddenFlag(String genBankHiddenFlag) {
        this.genBankHiddenFlag = genBankHiddenFlag;
    }

    public String getGenBankHiddenFlag() {
        return genBankHiddenFlag;
    }

    public void setHiddenSubtreeRootFlag(String hiddenSubtreeRootFlag) {

        this.hiddenSubtreeRootFlag = hiddenSubtreeRootFlag;
    }

    public String getHiddenSubtreeRootFlag() {
        return hiddenSubtreeRootFlag;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setWikiBaseItemId(String wikiBaseItemId) {
        this.wikiBaseItemId = wikiBaseItemId;
    }

    public String getWikiBaseItemId() {
        return wikiBaseItemId;
    }

    public enum Rank {
        BIOTYPE,
        CLADE, //
        CLASS, //
        COHORT, //
        FAMILY, //
        FORMA, //
        GENOTYPE, //
        GENUS, //
        INFRACLASS, //
        INFRAORDER, //
        ISOLATE, //
        KINGDOM, //
        MORPH,
        NO_RANK,
        ORDER, //
        PARVORDER, //
        PATHOGROUP,
        PHYLUM, //
        SECTION, //
        SERIES, //
        SEROGROUP, // same as serotype
        SEROTYPE, //
        SPECIES,
        STRAIN, //
        SUBCLASS, //
        SUBCOHORT, //
        SUBFAMILY, //
        SUBGENUS, //
        SUBKINGDOM, //
        SUBORDER, //
        SUBPHYLUM, //
        SUBSECTION, //
        SUBSPECIES, //
        SUBTRIBE, //
        SUBVARIETY, //
        SUPERCLASS, //
        SUPERFAMILY, //
        SUPERKINGDOM, //
        SUPERORDER, //
        SUPERPHYLUM, //
        TRIBE, //
        VARIETAS,
        SPECIES_GROUP,
        FORMA_SPECIALIS,
        SPECIES_SUBGROUP,
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTaxid(int taxid) {
        this.taxid = taxid;
    }

    public int getTaxid() {
        return taxid;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Rank getRank() {
        return rank;
    }
}
