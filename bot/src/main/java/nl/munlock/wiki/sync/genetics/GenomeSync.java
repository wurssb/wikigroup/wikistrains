package nl.munlock.wiki.sync.genetics;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.munlock.wiki.App;
import nl.munlock.wiki.CommandOptions;
import nl.munlock.wiki.objects.ena.ASSEMBLYATTRIBUTE;
import nl.munlock.wiki.objects.ena.Root;
import nl.munlock.wiki.objects.lineage.Taxonomy;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;
import org.json.JSONObject;
import org.json.XML;
import org.wikidata.wdtk.datamodel.helpers.Datamodel;
import org.wikidata.wdtk.datamodel.helpers.ItemDocumentBuilder;
import org.wikidata.wdtk.datamodel.helpers.ReferenceBuilder;
import org.wikidata.wdtk.datamodel.helpers.StatementBuilder;
import org.wikidata.wdtk.datamodel.interfaces.*;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

import static nl.munlock.wiki.App.*;
import static nl.munlock.wiki.Generic.downloadURL;
import static nl.munlock.wiki.iRODS.*;
import static nl.munlock.wiki.sync.genetics.TaxonomySync.checkPageByLabel;
import static nl.munlock.wiki.sync.genetics.TaxonomySync.taxonomies;
import static nl.munlock.wiki.wikibase.General.propertyLookup;
import static nl.munlock.wiki.wikibase.General.wdPageLookup;

public class GenomeSync {
    public static HashMap<String, WikiGenome> genomeLookup = new HashMap<String, WikiGenome>();
    private static Logger logger = Logger.getLogger(GenomeSync.class);

    public GenomeSync(CommandOptions commandOptions) throws Exception {

        // NCBI tax dump
        taxonomies = TaxonomySync.taxDumpLoader();

        // Get genomes from wikibase
        getGenomes();

        // Anonymous irods unlock query?
        getUNLOCK();

        syncGenomes(null);
    }

    /**
     * @param localXMLfiles list of xml files to be parsed or downloaded from ENA using an unlock path
     */
    private void syncGenomes(HashSet<File> localXMLfiles) {
        if (localXMLfiles == null)
            return;

        // Create genome pages in parallel
        ForkJoinPool customThreadPool = new ForkJoinPool(commandOptions.threads);

        // Try for thread pool
//        try {
            logger.info("Processing " + localXMLfiles.size() + " xml files");
//            customThreadPool.submit(() -> localXMLfiles.parallelStream().forEach(localXMLfile -> {
            localXMLfiles.forEach(localXMLfile -> {
                try {
                    // check XML file
                    localXMLfile.getParentFile().mkdirs();
                    String name = localXMLfile.getName().replaceAll(".xml", "");
                    String xmlURL = "https://www.ebi.ac.uk/ena/browser/api/xml/" + name;
                    // Obtain xml file if not exists
                    if (!localXMLfile.exists()) {
                        downloadURL(xmlURL, localXMLfile.getAbsolutePath());
                    }

                    // Load xml file
                    Root root = loadXML(localXMLfile.getAbsolutePath());
                    if (root == null) {
                        if (localXMLfile.length() < 20) {
                            System.err.println("Deleting failed xml file of size: " + localXMLfile.length());
                            localXMLfile.delete();
                        }
                        return;
                    }

                    // Taxa lookup
                    if (taxonomies.get(root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getTAXON_ID()) == null) {
                        logger.error("No taxonomy info found for " + root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getTAXON_ID() + " " + root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getSCIENTIFIC_NAME());
                        return;
                    }

                    // Genome lookup
                    WikiGenome wikiGenome = null;
                    if (genomeLookup.containsKey(root.getASSEMBLY_SET().getASSEMBLY().getAccession())) {
                        wikiGenome = genomeLookup.get(root.getASSEMBLY_SET().getASSEMBLY().getAccession());
                    }

                    ItemDocument genomeDocument;
                    if (wikiGenome == null) {
                        logger.info("Parsing genome " + localXMLfile.getName());
                        genomeDocument = createGenomeEntry(root);
                        // Create all pages for the lineage of this genome
                        TaxonomySync.createLineage(genomeDocument);
                        // Couple genome page to the parent taxon page after lineage creation
                        coupleGenomeLineage(genomeDocument);
                    } else if (wikiGenome.getParent() == null) {
                        logger.info("No parent for " + root.getASSEMBLY_SET().getASSEMBLY().getAccession());
                        // If there is a genome but without parent
                        genomeDocument = (ItemDocument) wbdf.getEntityDocument(wikiGenome.getItem());
                        // Create all pages for the lineage of this genome
                        TaxonomySync.createLineage(genomeDocument);
                        // Couple genome page to the parent taxon page after lineage creation
                        coupleGenomeLineage(genomeDocument);
                    }

                    logger.debug("Genome processed " + root.getASSEMBLY_SET().getASSEMBLY().getAccession());

                } catch (MediaWikiApiErrorException | IOException | InterruptedException | NullPointerException e) {
                    logger.error(e);
                }
//            })).get();
//        } catch (ExecutionException | InterruptedException e) {
//            logger.error(e.getMessage());
//        } finally {
//            customThreadPool.shutdown();
//        }
        });
        logger.info("Finished processing " + localXMLfiles.size() + " xml files");

//            while (!customThreadPool.isQuiescent()) {
//                 logger.info("Current threads active " + customThreadPool.getActiveThreadCount());
//            try {
//                SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                logger.error(e.getMessage());
//            }
//        }
    }


    /**
     * Couples the genome instance with the taxon instance of the same taxonomy identifier
     */
    public static void coupleGenomeLineage(ItemDocument genomeDocument) throws IOException, MediaWikiApiErrorException, InterruptedException {
        boolean parentTaxonLookup = genomeDocument.hasStatement(propertyLookup.get("parent taxon"));
        if (parentTaxonLookup)
            return;

        if (!genomeDocument.hasStatement(propertyLookup.get("GenBank assembly accession number"))) {
            System.err.println("Statement not found for " + genomeDocument.getEntityId().getId());
        }

        // Find all documents with statement NCBI Taxonomy id of the genome page
        int taxon = Integer.parseInt(genomeDocument.findStatement(propertyLookup.get("NCBI taxonomy ID")).getValue().toString().replaceAll("\"", ""));
        Taxonomy parentTaxon = taxonomies.get(taxon);
        String label = parentTaxon.getScientificName() + " (" + parentTaxon.getTaxid() + ")";
        ItemDocument parentDocument = checkPageByLabel(label, false);

        try {
            // Create statement
            EntityIdValue itemIdValue = genomeDocument.getEntityId();
            PropertyIdValue parentProperty = Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon").replaceAll(".*/", ""), commandOptions.wiki);
            ItemIdValue parentTaxonIdValue = Datamodel.makeItemIdValue(parentDocument.getEntityId().getId(), commandOptions.wiki);
            Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, parentProperty).withValue(parentTaxonIdValue).build();
            ItemIdValue genomeItemId = Datamodel.makeItemIdValue(genomeDocument.getEntityId().getId(), commandOptions.wiki);
            wbde.updateStatements(genomeItemId, Arrays.asList(statement), Collections.emptyList(), "Parent taxon added", Collections.emptyList());
            logger.info("Coupled " + itemIdValue.getId() + " to " + parentTaxonIdValue.getId());
        } catch (MediaWikiApiErrorException | IOException e) {
            e.printStackTrace();
        }

//
//        String accession = genomeDocument.findStatement(propertyLookup.get("GenBank assembly accession number")).getValue().toString().replaceAll("\"","");
//        logger.info("Starting genome coupling to finalise hanging entries with accession " + accession);
//        // Query to obtain all potential and skip already existing connections
//        String queryString = "" + App.PREFIX +
//                "" +
//                "SELECT DISTINCT ?genomeItem ?parentItem\n" +
//                "WHERE {\n" +
//                "  ?genomeItem wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("genome") + " .\n" +
//                "  ?genomeItem wdt:" + propertyLookup.get("GenBank assembly accession number") + "\"" + accession + "\" .\n" +
//                "  ?genomeItem wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxon .\n" +
//                "  ?parentItem wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
//                "  ?parentItem wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxon .\n" +
//                "}";
//
//        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
//
//        TupleQueryResult result;
//        while (true) {
//            TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
//            result = query.evaluate();
//            int counter = 0;
//            if (!result.hasNext()) {
//                counter++;
//                if (counter > 10)
//                    logger.info("No parent taxon available for " + genomeDocument.getLabels().get("en"));
//                try {
//                    SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    throw new RuntimeException(e);
//                }
//            } else {
//                // Wait until we have results, there is a sync delay
//                break;
//            }
//        }
//
//        while (result.hasNext()) {
//            BindingSet bindingSet = result.next();
//            String genome = bindingSet.getValue("genomeItem").stringValue().replaceAll(".*/", "");
//            String parentTaxon = bindingSet.getValue("parentItem").stringValue().replaceAll(".*/", "");
//
//            // Update genomeDocument
//            try {
//                // Create statement
//                EntityIdValue itemIdValue = genomeDocument.getEntityId();
//                PropertyIdValue parentProperty = Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon").replaceAll(".*/", ""), commandOptions.wiki);
//                ItemIdValue parentTaxonIdValue = Datamodel.makeItemIdValue(parentTaxon, commandOptions.wiki);
//                Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, parentProperty).withValue(parentTaxonIdValue).build();
//                ItemIdValue genomeItemId = Datamodel.makeItemIdValue(genome, commandOptions.wiki);
//                wbde.updateStatements(genomeItemId, Arrays.asList(statement), Collections.emptyList(), "Parent taxon added", Collections.emptyList());
//                logger.info("Coupled " + itemIdValue.getId() + " to " + parentTaxonIdValue.getId());
//            } catch (MediaWikiApiErrorException | IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private Root loadXML(String xmlFile) {
        try {
            String content = FileUtils.readFileToString(new File(xmlFile), Charset.defaultCharset());
            JSONObject json = XML.toJSONObject(content);
            // To string in pretty format for processing and in case we need to save it to temporary file due to mismatch in mapping
            String jsonString = json.toString(4);

            // JSON string mapper to object with several configuration options
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            Root root = objectMapper.readValue(jsonString, Root.class);
            return root;
        } catch (IOException e) {
            logger.error("Failed to load xml file: " + xmlFile);
            return null;
        }
    }

    private void getUNLOCK() throws GenQueryBuilderException, JargonException, JargonQueryException, InterruptedException, ExecutionException {
        // Do the query in parts
        IRODSFile collection = irodsFileSystem.getIRODSFileFactory(irodsAccount).instanceIRODSFile("/unlock/references/genomes/");
        ArrayList<File> collections = new ArrayList<>(List.of(collection.listFiles()));
        Collections.shuffle(collections);
        for (File file : collections) {
            if (file.getName().contains("GCA_")) {
                IRODSFile level1 = irodsFileSystem.getIRODSFileFactory(irodsAccount).instanceIRODSFile(file.getAbsolutePath());
//                for (File level2 : level1.listFiles()) {
                    // if (!level2.getName().contains("GCA_000017")) continue;
                    logger.info("Processing " + level1.getAbsolutePath());
                    // Reset curlFile to only genomes in UNLOCK
                    IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

                    queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, level1.getAbsolutePath() + "%");
                    queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "GCA%.yaml");

                    queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
                    queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

                    // Set limit?
                    IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

                    IRODSGenQueryExecutor irodsGenQueryExecutor = accessObjectFactory.getIRODSGenQueryExecutor(irodsAccount);
                    IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

                    List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                    HashSet<File> localXMLfiles = new HashSet<>();
                    for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                        // Yaml file... obtain, parse, check destination... if already exists don't do it?
                        File localXML = new File("./" + irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1).replaceAll(".yaml$", ".xml"));
                        localXMLfiles.add(localXML);
                    }
                    syncGenomes(localXMLfiles);
                }
            }
        }
//    }

    private ItemDocument createGenomeEntry(Root root) {
        String label = root.getASSEMBLY_SET().getASSEMBLY().getAccession() + " " + root.getASSEMBLY_SET().getASSEMBLY().getTITLE();

        ItemDocumentBuilder itemDocumentBuilder = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                withLabel(label, "en").
                withDescription(root.getASSEMBLY_SET().getASSEMBLY().getTITLE(), "en");

        String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
        PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
        Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(wdPageLookup.get("European Nucleotide Archive"), App.commandOptions.wiki)).build();

        // Instance of element
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("instance of").replaceAll(".*/", ""), commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeItemIdValue(wdPageLookup.get("genome"), commandOptions.wiki)).withReference(reference).build());

        // Genbank info
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("GenBank assembly accession number").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getAccession())).withReference(reference).build());

        // Genome size statement
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("genome size").replaceAll(".*/", ""), App.commandOptions.wiki);
        for (ASSEMBLYATTRIBUTE assemblyattribute : root.getASSEMBLY_SET().getASSEMBLY().getASSEMBLY_ATTRIBUTES().getASSEMBLY_ATTRIBUTE()) {
            if (assemblyattribute.getTAG().contains("total-length"))
                itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeQuantityValue(BigDecimal.valueOf(Long.parseLong(assemblyattribute.getVALUE().toString())))).withReference(reference).build());
        }

        // NCBI taxon statement
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("NCBI taxonomy ID").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(String.valueOf(root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getTAXON_ID()))).withReference(reference).build());

        // Assembly level information
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("Assembly Level").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeItemIdValue(wdPageLookup.get(root.getASSEMBLY_SET().getASSEMBLY().getASSEMBLY_LEVEL()), commandOptions.wiki)).withReference(reference).build());

        // Genome version
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("edition number").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getAccession().split("\\.")[1])).withReference(reference).build());

        // Genome representation
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("Genome Representation").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getGENOME_REPRESENTATION())).withReference(reference).build());

        // Sample accession
        if (root.getASSEMBLY_SET().getASSEMBLY().getSAMPLE_REF() != null) {
            propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("BioSample").replaceAll(".*/", ""), App.commandOptions.wiki);
            itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getSAMPLE_REF().getIDENTIFIERS().getPRIMARY_ID())).withReference(reference).build());
        }
        // Bio project identifier
//        if (root.getASSEMBLY_SET().getASSEMBLY().getSAMPLE_REF().getIDENTIFIERS().getPRIMARY_ID() != null) {
//            propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("BioProject").replaceAll(".*/", ""), App.commandOptions.wiki);
//            itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getSAMPLE_REF().getIDENTIFIERS().getPRIMARY_ID())).withReference(reference).build());
//        }

        // Taxon name
        propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("taxon name").replaceAll(".*/", ""), App.commandOptions.wiki);
        itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getSCIENTIFIC_NAME())).withReference(reference).build());

        // Optional DSM number
        if (root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getSCIENTIFIC_NAME().matches(".*DSM [0-9]+.*")) {
            String dsm = root.getASSEMBLY_SET().getASSEMBLY().getTAXON().getSCIENTIFIC_NAME().replaceAll(".*(DSM [0-9]+).*", "$1");
            System.err.println("DSM: " + dsm);
            propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("DSM code").replaceAll(".*/", ""), App.commandOptions.wiki);
            itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(dsm)).withReference(reference).build());
        }
//        if (genome.getATCC() != null) {
//            propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("ATCC code").replaceAll(".*/", ""), App.commandOptions.wiki);
//            itemDocumentBuilder = itemDocumentBuilder.withStatement(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(genome.getATCC())).withReference(reference).build());
//        }

        try {
            ItemDocument itemDocument = itemDocumentBuilder.build();
            itemDocument = wbde.createItemDocument(itemDocument, "Item synchronisation", null);
            return itemDocument;
        } catch (IOException | MediaWikiApiErrorException e) {
            logger.warn(e.getMessage()); //printStackTrace();
            // Write failed
            return null;
        }
    }

    public static HashMap<String, String> getPagesWithParentTaxons() {
        logger.info("Obtaining pages with parent taxon");
        String queryString = "" + App.PREFIX +
                "SELECT ?item ?parent\n" +
                "WHERE {\n" +
                "?item wdt:" + propertyLookup.get("parent taxon") + " ?parent\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            HashMap<String, String> pageParentTaxon = new HashMap<>();
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet solution = result.next();
                String item = solution.getValue("item").stringValue().replaceAll(".*/", "");
                String parent = solution.getValue("parent").stringValue().replaceAll(".*/", "");
                pageParentTaxon.put(item, parent);
            }
            return pageParentTaxon;
        }
    }

    public static void getGenomes() {
        logger.info("Obtaining genomes");
        // Obtains all genomes with parent taxon ID
        String queryString = "" + App.PREFIX +
                "SELECT ?item ?accession ?parent\n" +
                "WHERE {\n" +
                "?item wdt:" + propertyLookup.get("GenBank assembly accession number") + " ?accession .\n" +
                "OPTIONAL { ?item wdt:" + propertyLookup.get("parent taxon") + " ?parent .}\n " +
                "}";
        System.err.println("Query: " + queryString);
        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet solution = result.next();
                String item = solution.getValue("item").stringValue().replaceAll(".*/", "");
                String accession = solution.getValue("accession").stringValue().replaceAll(".*/", "");
                org.eclipse.rdf4j.model.Value parentValue = solution.getValue("parent");
                // Set the items
                WikiGenome wikiGenome = new WikiGenome();
                wikiGenome.setItem(item);
                wikiGenome.setAccession(accession);
                if (parentValue != null) {
                    String parent = solution.getValue("parent").stringValue().replaceAll(".*/", "");
                    wikiGenome.setParent(parent);
                }
                genomeLookup.put(accession, wikiGenome);
            }
        }
        logger.info("Obtained " + genomeLookup.size() + " genomes");
    }
}
