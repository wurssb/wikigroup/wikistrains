package nl.munlock.wiki.sync.bacdive;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jsonldjava.shaded.com.google.common.io.CharStreams;
import nl.munlock.wiki.App;
import nl.munlock.wiki.CommandOptions;
import nl.munlock.wiki.objects.bacdive.*;
import nl.munlock.wiki.objects.bacdive.media.Recipe;
import nl.munlock.wiki.objects.bacdive.media.Solution;
import nl.munlock.wiki.objects.lineage.Taxonomy;
import nl.munlock.wiki.sync.genetics.TaxonomySync;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.wikidata.wdtk.datamodel.helpers.Datamodel;
import org.wikidata.wdtk.datamodel.helpers.ItemDocumentBuilder;
import org.wikidata.wdtk.datamodel.helpers.ReferenceBuilder;
import org.wikidata.wdtk.datamodel.helpers.StatementBuilder;
import org.wikidata.wdtk.datamodel.interfaces.*;
import org.wikidata.wdtk.datamodel.interfaces.Reference;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static nl.munlock.wiki.App.*;
import static nl.munlock.wiki.sync.genetics.GenomeSync.genomeLookup;
import static nl.munlock.wiki.sync.genetics.GenomeSync.getGenomes;
import static nl.munlock.wiki.wikibase.General.propertyLookup;
import static nl.munlock.wiki.wikibase.General.wdPageLookup;

public class BacDiveSync {
    private static Logger logger = Logger.getLogger(BacDiveSync.class);
    private HashMap<String, HashMap<String, String>> bacdiveLookup = new HashMap<>();

    // TODO connect interface from java
    public BacDiveSync(CommandOptions commandOptions) throws InterruptedException, IOException {
        // Obtain all bacdive entries
        getAllBacDiveEntries();

        ArrayList<Path> files = new ArrayList<>(Files.list(new File("BacDive/entry").toPath()).sorted().toList());
        Collections.shuffle(files);

        // Obtain all genome entries
        getGenomes();

        int genomeCounter = 0;
        for (int i = 0; i < files.size(); i++) {
            if (i % 100 == 0) {
                logger.info(i + " " + files.get(i).toFile().getName() + " with " + genomeCounter + " genomes so far...");
            }
            // Skip empty files
            if (files.get(i).toFile().length() == 0) continue;
            // Load into root object of bacdive
            Root root = loadJSON(files.get(i).toFile());
            // Minimally needed?
            if (root.getCultureAndGrowthConditions() == null) {
//                logger.error("No culture and growth info found for " + root.getGeneral().getBacDiveID());
            }
            if (root.getIsolationSamplingAndEnvironmentalInformation() == null) {
//                logger.error("No sampling and environmental information available");
            }

            if (root.getSequenceInformation().getGenomeSequences().size() == 0) {
                // logger.error("Skipping " + root.getGeneral().getBacDiveID() + " as no genome info is present");
//                continue;
            }

            // Create a page
            try {
                createPhenotypeEntry(root);
            } catch (Exception e) {
                System.err.println("e failed " + e.getMessage() );
            }
        }
    }

    private ItemDocument createPhenotypeEntry(Root root) throws IOException, MediaWikiApiErrorException {
        if (bacdiveLookup.get(propertyLookup.get("BacDive ID")).containsKey(root.getGeneral().getBacDiveID())) {
            // TODO
              logger.info("Disable me if you want to prevent the updates of bacdive ids");
//            return null;
        }
        // Stated in bacdive
        String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
        PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
        Reference reference = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(wdPageLookup.get("BacDive"), App.commandOptions.wiki)).build();

        // Focus on media for now
        if (root.getCultureAndGrowthConditions().getCultureMedium().size() == 0)
            return null;

        // Other elements
        for (CultureMedium cultureMedium : root.getCultureAndGrowthConditions().getCultureMedium()) {
            createMediaPage(cultureMedium, reference);
        }

        // Obtain species or genome linkage
        if (root.getGeneral().getNCBITaxId() == null) {
            return null;
        }

        if (root.getGeneral().getNCBITaxId().size() > 1) {
            System.err.println("Multiple taxids detected???");
            return null;
        }

        // Get page with ncbi Tax id
        NCBITaxId ncbiTaxId = root.getGeneral().getNCBITaxId().get(0);
        Taxonomy taxonomy = TaxonomySync.getTaxa(ncbiTaxId.getNCBITaxId());

        // TODO Not sure if we should skip these?
        if (taxonomy == null)
            return null;

        // If this is not a species
        if (!taxonomy.getRank().equals(Taxonomy.Rank.SPECIES)) {
            System.err.println("This is not a species, this is a " + taxonomy.getRank().name());
            return null;
        }

        String label = "Phenotype information: " + root.getGeneral().getDescription();

        ArrayList<Statement> statements = new ArrayList<>();

        // Instance of phenotype
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get("instance of").replaceAll(".*/", ""), commandOptions.wiki);
        statements.add(StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeItemIdValue(wdPageLookup.get("phenotype"), commandOptions.wiki)).withReference(reference).build());

        // parent linkage (Species) linkage added
        statements.add(addPage("parent taxon", taxonomy.getWikiBaseItemId(), reference));

        // Bacdive linkage
        statements.add(addValue("BacDive ID", root.getGeneral().getBacDiveID(), reference));

        // DSM code
        statements.add(addValue("DSM code", root.getGeneral().getDSMNumber(), reference));

        // Genome page?
        if (root.getSequenceInformation() !=null) {
            for (GenomeSequence genomeSequence : root.getSequenceInformation().getGenomeSequences()) {
                if (genomeLookup.containsKey(genomeSequence.getAccession())) {
                    System.err.println("we have a genome hit?");
                }
            }
        }

        // Culture temperature
        for (CultureTemp cultureTemp : root.getCultureAndGrowthConditions().getCultureTemp()) {
            if (cultureTemp.getGrowth().equalsIgnoreCase("positive")) {
                if (cultureTemp.getType().equalsIgnoreCase("growth")) {
                    statements.add()
                }
            }
        }

        // Media composition
        for (CultureMedium cultureMedium : root.getCultureAndGrowthConditions().getCultureMedium()) {
            if (cultureMedium.getName() != null) {
                String name = cultureMedium.getName();
                // Get id from name?

                List<WbSearchEntitiesResult> wbSearchEntitiesResults = wbdf.searchEntities(name, "en");
                if (wbSearchEntitiesResults.size() > 0) {
                    String entityID = wbSearchEntitiesResults.get(0).getEntityId();
                    statements.add(addPage("media composition", entityID, reference));
                }
            }
        }


        try {
            // Only if the entry does not exist
            if (bacdiveLookup.get(propertyLookup.get("BacDive ID")).containsKey(root.getGeneral().getBacDiveID())) {
                logger.info("Using existing bacdive page for " + root.getGeneral().getBacDiveID() + " " + bacdiveLookup.get(root.getGeneral().getBacDiveID()));
                // Get document
                EntityDocument entityDocument = wbdf.getEntityDocument(bacdiveLookup.get(propertyLookup.get("BacDive ID")).get(root.getGeneral().getBacDiveID()));
                ItemIdValue itemIdValue = Datamodel.makeItemIdValue(entityDocument.getEntityId().getId(), commandOptions.wiki);
                wbde.updateStatements(itemIdValue, statements, Collections.emptyList(), "Update document", Collections.emptyList());
                return (ItemDocument) entityDocument;
            } else {
                logger.info("Creating new bacdive page for " + root.getGeneral().getBacDiveID());
                ItemDocument itemDocument = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                        withLabel(label, "en").
                        withDescription(root.getGeneral().getDescription(), "en").build();
                for (Statement statement : statements) {
                    itemDocument = itemDocument.withStatement(statement);
                }
                itemDocument = wbde.createItemDocument(itemDocument, "Phenotype page creation", null);
                System.err.println(itemDocument.getEntityId() + " created on the wiki instance");
                return itemDocument;
            }
        } catch (IOException | MediaWikiApiErrorException e) {
            // System.err.println(bacdiveLookup.get(propertyLookup.get("BacDive ID")));
            System.err.println(bacdiveLookup.get(propertyLookup.get("BacDive ID")).containsKey(root.getGeneral().getBacDiveID()));
            logger.warn(e.getMessage());
            // printStackTrace();
            // Write failed
            return null;
        }
    }

    private void createMediaPage(CultureMedium cultureMedium, Reference reference) {
        // No growth no storage needed
        if (cultureMedium.getGrowth() == null || !cultureMedium.getGrowth().equalsIgnoreCase("yes"))
            return;
        if (cultureMedium.getLink() == null) {
            return;
        } else {
            if (cultureMedium.getLink().endsWith(".pdf"))
                return;
        }

        // Obtain json
        String json = cultureMedium.getLink().replace("/medium/", "/download/medium/") + "/json";

        // Download json
        File jsonFile = new File("./BacDive/media/" + cultureMedium.getLink().replaceAll(".*/", ""));
        if (!jsonFile.exists()) {
            System.err.println("Json file to be downloaded: " + json);
            try (BufferedInputStream in = new BufferedInputStream(new URL(json).openStream());
                 FileOutputStream fileOutputStream = new FileOutputStream("./BacDive/media/" + cultureMedium.getLink().replaceAll(".*/", ""))) {
                byte dataBuffer[] = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                }
            } catch (IOException e) {
                // handle exception
            }
        }

        try {
            // Load json file
            nl.munlock.wiki.objects.bacdive.media.Root root = loadMediaJSON(jsonFile);

            if (root == null) {
                System.err.println("Loading json failed for " + jsonFile);
                return;
            }

            // Creates all compound objects
            createAllCompounds(root, reference);

            // Document creation if it does not exists
            List<WbSearchEntitiesResult> wbSearchEntitiesResults = wbdf.searchEntities(cultureMedium.getName(), "en", Long.valueOf(10));
            ItemDocumentBuilder itemDocumentBuilder;
            if (wbSearchEntitiesResults.size() > 0) {
                String entityId = wbSearchEntitiesResults.get(0).getEntityId();
                EntityDocument entityDocument = wbdf.getEntityDocument(wbSearchEntitiesResults.get(0).getEntityId());
                itemDocumentBuilder = ItemDocumentBuilder.fromItemDocument((ItemDocument) entityDocument);
            } else {
                itemDocumentBuilder = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                        withLabel(cultureMedium.getName(), "en").
                        withDescription("Media composition for " + cultureMedium.getName(), "en");
            }
            // List of statements
            ArrayList<Statement> statements = new ArrayList<>();
            // Add instance of
            statements.add(addPage("instance of", wdPageLookup.get("culture media"), reference));

            // Connect all compounds with an extended reference element
            for (Solution solution : root.getSolutions()) {
                if (!solution.getMainSolution().equalsIgnoreCase("yes")) {
                    // Explain?
                    continue;
                }
                if (solution.getRecipe() == null) continue;
                for (Recipe compound : solution.getRecipe()) {
                    // Add additional elements to the reference object
                    String refPropertyID = propertyLookup.get("stated in").replaceAll(".*/", "");
                    PropertyIdValue statedIn = Datamodel.makePropertyIdValue(refPropertyID, App.commandOptions.wiki);
                    ReferenceBuilder referenceBuilder = ReferenceBuilder.newInstance().withPropertyValue(statedIn, Datamodel.makeItemIdValue(wdPageLookup.get("BacDive"), commandOptions.wiki));
                    // Set quantity
                    QuantityValue quantityValue = Datamodel.makeQuantityValue(BigDecimal.valueOf(compound.getAmount()));
                    PropertyIdValue quantityPropertyValue = Datamodel.makePropertyIdValue(propertyLookup.get("quantity").replaceAll(".*/", ""), App.commandOptions.wiki);
                    referenceBuilder.withPropertyValue(quantityPropertyValue, quantityValue);
                    // Set
                    if (compound.getUnit() != null) {
                        StringValue unitValue = Datamodel.makeStringValue(compound.getUnit());
                        PropertyIdValue unitPropertyValue = Datamodel.makePropertyIdValue(propertyLookup.get("recommended unit of measurement label").replaceAll(".*/", ""), App.commandOptions.wiki);
                        referenceBuilder.withPropertyValue(unitPropertyValue, unitValue);
                    }
                    // Add compound parts
                    String compoundPage = bacdiveLookup.get(propertyLookup.get("BacDive Compound ID")).get(compound.getCompound_id());
                    // TODO why is it null?
                    if (compoundPage != null) {
                        // System.err.println("Compound page: " + compoundPage);
                        statements.add(addPage("has part(s)", compoundPage, referenceBuilder.build()));
                    } else {
                        // System.err.println(compound.getCompound_id() + " is null");
                    }
                }
            }
            // Add other info
            statements.add(addValue("BacDive Media ID", root.getMedium().getId(), reference));
            if (root.getMedium().getMax_pH() == root.getMedium().getMin_pH()) {
                statements.add(addQuantity("pH value", root.getMedium().getMax_pH(), reference));
            }

            // Create document
            try {
                // This is an update
                 if (wbSearchEntitiesResults.size() > 0) {
                     ItemIdValue itemIdValue = Datamodel.makeItemIdValue(wbSearchEntitiesResults.get(0).getEntityId(), commandOptions.wiki);
                     wbde.updateStatements(itemIdValue, statements, Collections.emptyList(), "Update", Collections.emptyList());
                 } else {
                     for (Statement statement : statements) {
                         itemDocumentBuilder = itemDocumentBuilder.withStatement(statement);
                     }
                     ItemDocument itemDocument = itemDocumentBuilder.build();
                     itemDocument = wbde.createItemDocument(itemDocument, "Phenotype page creation", null);
                     System.err.println(itemDocument.getEntityId() + " created on the wiki instance");
                 }
            } catch (IOException | MediaWikiApiErrorException e) {
//             Write failed
             logger.warn("Error... " + e.getMessage() + " " + e.getLocalizedMessage()); //printStackTrace();
            } catch (Exception e) {
                logger.error("Failed for " + e.getMessage());
                System.err.println(StringUtils.join(e.getStackTrace(), "\n"));
                System.err.println("stop...");
            }
        } catch (IOException | MediaWikiApiErrorException e) {
            throw new RuntimeException(e);
        }
    }

    private void createAllCompounds(nl.munlock.wiki.objects.bacdive.media.Root root, Reference reference) throws IOException, MediaWikiApiErrorException {
        for (Solution solution : root.getSolutions()) {
            if (solution.getRecipe() != null && solution.getMainSolution().equalsIgnoreCase("yes")) {
                // Create solution entry
                for (Recipe compound : solution.getRecipe()) {
                    // Document creation
                    if (!bacdiveLookup.get(propertyLookup.get("BacDive Compound ID")).containsKey(compound.getCompound_id())) {
                        if (compound.getCompound() == null)
                            continue;
                        System.err.println("Compound: " + compound.getCompound());
                        ItemDocumentBuilder itemDocumentBuilder = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                                withLabel(compound.getCompound(), "en").
                                withDescription("chemical compound", "en");
                        addPage("instance of", wdPageLookup.get("chemical compound"), reference);
                        addValue("BacDive Compound ID", compound.getCompound_id(), reference);

                        ItemDocument itemDocument = itemDocumentBuilder.build();
                        itemDocument = wbde.createItemDocument(itemDocument, "Chemical compound creation", null);
                        String qid = itemDocument.getEntityId().getId().replaceAll(".*.php", "");
                        bacdiveLookup.get(propertyLookup.get("BacDive Compound ID")).put(compound.getCompound_id(), qid);
                        System.err.println(qid + " created on the wiki instance");
                    }
                }
            }
        }
    }

    private Statement addPage(String property, String page, Reference reference) {
        // Instance of phenotype
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get(property).replaceAll(".*/", ""), commandOptions.wiki);
        Statement statement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeItemIdValue(page, commandOptions.wiki)).withReference(reference).build();
        return statement;
    }

    private Statement addValue(String property, int value, Reference reference) {
        return addValue(property, Integer.toString(value), reference);
    }
    private Statement addQuantity(String property, int value, Reference reference) {
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get(property).replaceAll(".*/", ""), App.commandOptions.wiki);
        QuantityValue quantityValue = Datamodel.makeQuantityValue(BigDecimal.valueOf(value));
        Statement statement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(quantityValue).build();
        return statement;
    }
    private Statement addValue(String property, String value, Reference reference) {
        // Instance of phenotype
        PropertyIdValue propertyIdValue = Datamodel.makePropertyIdValue(propertyLookup.get(property).replaceAll(".*/", ""), commandOptions.wiki);
        Statement statement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, propertyIdValue).withValue(Datamodel.makeStringValue(value)).withReference(reference).build();
        return statement;
    }

    private void getAllBacDiveEntries() {
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?entry ?property ?value\n" +
                "WHERE {\n" +
                "VALUES ?property { wdt:"+propertyLookup.get("BacDive Compound ID") + " wdt:"+propertyLookup.get("BacDive ID") + "} \n" +
                "  ?entry ?property ?value .\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        HashMap<String, String> childParent = new HashMap<>();
        TupleQueryResult result = query.evaluate();

        // No results, try again after a coupling check
        while (result.hasNext()) {
            BindingSet bindingSet = result.next();
            String entry = bindingSet.getValue("entry").stringValue().replaceAll(".*/","");
            String value = bindingSet.getValue("value").stringValue().replaceAll(".*/","");
            String property = bindingSet.getValue("property").stringValue().replaceAll(".*/","");
            if (!bacdiveLookup.containsKey(property))
                bacdiveLookup.put(property, new HashMap<>());
            bacdiveLookup.get(property).put(value, entry);
        }
    }

    private Root loadJSON(File json) {
        // Load json into object
        String text;
        try (Reader reader = new InputStreamReader(new FileInputStream(json))) {
            text = CharStreams.toString(reader);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            // DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY
            Root root = objectMapper.readValue(text, Root.class);
            return root;
        } catch (IOException e) {
            System.err.println("JSON: " + json);
             e.printStackTrace();
             System.exit(1);
        }
        return null;
    }

    private nl.munlock.wiki.objects.bacdive.media.Root loadMediaJSON(File json) {
        // Load json into object
        String text;
        try (Reader reader = new InputStreamReader(new FileInputStream(json))) {
            text = CharStreams.toString(reader);
            if (text.contains("Medium not found"))
                return null;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            // DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY
            nl.munlock.wiki.objects.bacdive.media.Root root = objectMapper.readValue(text, nl.munlock.wiki.objects.bacdive.media.Root.class);
            return root;
        } catch (IOException e) {
            System.err.println("JSON: " + json);
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}