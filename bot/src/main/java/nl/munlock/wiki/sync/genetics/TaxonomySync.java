package nl.munlock.wiki.sync.genetics;

import com.google.common.collect.Lists;
import nl.munlock.wiki.App;
import nl.munlock.wiki.objects.lineage.Taxonomy;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.wikidata.wdtk.datamodel.helpers.Datamodel;
import org.wikidata.wdtk.datamodel.helpers.ItemDocumentBuilder;
import org.wikidata.wdtk.datamodel.helpers.StatementBuilder;
import org.wikidata.wdtk.datamodel.interfaces.*;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;
import static nl.munlock.wiki.App.*;
import static nl.munlock.wiki.wikibase.General.propertyLookup;
import static nl.munlock.wiki.wikibase.General.wdPageLookup;

public class TaxonomySync {
    //    SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
    public static HashMap<Integer, Taxonomy> taxonomies = new HashMap<>();
    private static HashMap<Integer, String> finishedTaxa = new HashMap<>();
    private static HashSet<Integer> hasParent = new HashSet<>();
    private static HashMap<Integer, Taxonomy> taxonLookup = new HashMap<>();
    private static Logger logger = Logger.getLogger(TaxonomySync.class);
//
//    public TaxonomySync(CommandOptions commandOptions) throws FileNotFoundException {
//        logger.info("Starting taxonomy synchronisation");
//
//        // NCBI tax dump
//        taxonomies = taxDumpLoader();
//
//        // Get all genomes in wikibase
//        GenomeSync.getGenomes();
//
//        // Couple genomes to strains via taxon from previous runs if failed
//        coupleGenomeLineage();
//        GenomeSync.genomeLookup = new HashMap<>();
//
//        // Obtain accessions without parent taxon
//        Set<String> accessions = getGenbankAccessionsWithoutParent();
//        for (String accession : accessions) {
//            createLineage(accession);
//        }
//    }
//
//    /**
//     * Removes genbank accession numbers from global list that already have a parent taxon identifier
//     *
//     * @return
//     */
//    private Set<String> getGenbankAccessionsWithoutParent() {
//        HashSet<String> accessions = new HashSet<>();
//
//        String queryString = "" +
//                "SELECT DISTINCT ?genbank WHERE {\n" +
//                "  ?item wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("genome") + " .\n" +
//                "  ?item wdt:" + propertyLookup.get("GenBank assembly accession number") + " ?genbank .\n" +
//                "  MINUS { ?item wdt:" + propertyLookup.get("parent taxon") + " ?parent . }\n" +
//                "}";
//
//        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
//        try (TupleQueryResult result = query.evaluate()) {
//            // we just iterate over all solutions in the result...
//            while (result.hasNext()) {
//                BindingSet bindingSet = result.next();
//                String genbank = bindingSet.getValue("genbank").stringValue().replaceAll(".*/", "");
//                accessions.add(genbank);
//            }
//        }
//        return accessions;
//    }
//

    /**
     * Couples the instance of taxon with the corresponding parent taxon ID item
     */
    public static void coupleLineages(int parentTaxonID) throws InterruptedException {
        logger.info("Coupling lineages together");
        // Obtain child and parent taxon ids that do not have a parent taxon
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?child ?parent\n" +
                "WHERE {\n" +
                " VALUES ?parentID {\"" + parentTaxonID + "\"}\n" +
                "  ?child wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                "  MINUS { ?child wdt:" + propertyLookup.get("parent taxon") + " ?parentTaxon . }\n" +
                "  ?child wdt:" + propertyLookup.get("parent taxon ID") + " ?parentID .\n" +
                "  ?parent wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?parentID .\n" +
                "  ?parent wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        HashMap<String, String> childParent = new HashMap<>();
        TupleQueryResult result = query.evaluate();

        // No results, try again after a coupling check
        while (!result.hasNext()) {
            // Check if coupling was already made
            String queryString2 = "" + PREFIX +
                    "SELECT DISTINCT ?child ?parent\n" +
                    "WHERE {\n" +
                    " VALUES ?parentID {\"" + parentTaxonID + "\"}\n" +
                    "  ?child wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                    "  ?child wdt:" + propertyLookup.get("parent taxon") + " ?parentTaxon . \n" +
                    "  ?child wdt:" + propertyLookup.get("parent taxon ID") + " ?parentID .\n" +
                    "  ?parent wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?parentID .\n" +
                    "  ?parent wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                    "} LIMIT 100";
            query = repo.getConnection().prepareTupleQuery(queryString2);
            result = query.evaluate();
            // if coupling made we can stop
            if (result.hasNext())
                return;
            else {
                // Try the query again...
                TimeUnit.SECONDS.sleep(1);
                query = repo.getConnection().prepareTupleQuery(queryString);
                // Update result information
                result = query.evaluate();
            }
        }

        // we just iterate over all solutions in the result...
        while (result.hasNext()) {
            BindingSet bindingSet = result.next();
            String child = bindingSet.getValue("child").stringValue().replaceAll(".*/", "");
            String parent = bindingSet.getValue("parent").stringValue().replaceAll(".*/", "");
            childParent.put(child, parent);
        }

        HashSet<String> childs = new HashSet<>();

        childParent.keySet().forEach(child -> {
            String parent = childParent.get(child);
            // Create connection
            System.err.println("Child and parent: " + child + "\t" + parent);
            try {
                EntityDocument genomeDocument = wbdf.getEntityDocument(child);
                // Create statement
                EntityIdValue itemIdValue = genomeDocument.getEntityId();
                PropertyIdValue parentProperty = Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon").replaceAll(".*/", ""), commandOptions.wiki);
                ItemIdValue parentTaxonIdValue = Datamodel.makeItemIdValue(parent, commandOptions.wiki);
                Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, parentProperty).withValue(parentTaxonIdValue).build();

                ItemIdValue childItemIdValue = Datamodel.makeItemIdValue(child, commandOptions.wiki);
                wbde.updateStatements(childItemIdValue, Arrays.asList(statement), Collections.emptyList(), "Parent taxon added", Collections.emptyList());
                childs.add(child);
                if (childs.size() % 100 == 0)
                    System.err.println("Processed " + childs.size() + " childs");
            } catch (IOException | MediaWikiApiErrorException ex) {
                ex.printStackTrace();
            }
        });
    }

//
//    /**
//     * Couples the genome instance with the taxon instance of the same taxonomy identifier
//     */
//    public static void coupleGenomeLineage() {
//        logger.info("Starting genome coupling to finalise hanging entries");
//        // Query to obtain all potential and skip already existing connections
//        String queryString = "" + App.PREFIX +
//                "" +
//                "SELECT DISTINCT ?genomeItem ?parentItem\n" +
//                "WHERE {\n" +
//                "  ?genomeItem wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("genome") + " .\n" +
//                "  ?genomeItem wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxon .\n" +
//                "  MINUS { ?genomeItem wdt:"+propertyLookup.get("parent taxon")+"  ?parentItem . }\n" +
//                "  ?parentItem wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
//                "  ?parentItem wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxon .\n" +
//                "}";
//        System.err.println(queryString);
//
//        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
//
//        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
//        TupleQueryResult result = query.evaluate();
//
//        // we just iterate over all solutions in the result...
//        HashMap<String, String> collector = new HashMap<>();
//
//        while (result.hasNext()) {
//            BindingSet bindingSet = result.next();
//            String genome = bindingSet.getValue("genomeItem").stringValue().replaceAll(".*/", "");
//            String parentTaxon = bindingSet.getValue("parentItem").stringValue().replaceAll(".*/", "");
//            collector.put(genome, parentTaxon);
//        }
//
//        // Do in parallel - Update genome pages
//        AtomicInteger count = new AtomicInteger();
//
//        ForkJoinPool customThreadPool = new ForkJoinPool(commandOptions.threads);
//
//        AtomicInteger finalCount = count;
//        try {
//            // Obtain pages with a parent taxon coupled to it
//            HashMap<String, String> pagesWithParentTaxons = getPagesWithParentTaxons();
//            // Iterate over each genome page and add parent taxon if needed
//            customThreadPool.submit(() -> collector.keySet().parallelStream().forEach(genome -> {
//                finalCount.set(finalCount.get() + 1);
//
//                // Check if parent taxon is not already present for genome?
//                if (pagesWithParentTaxons.containsKey(genome)) return;
//
//                if (finalCount.get() % 100 == 0) {
//                    logger.info("Parsing genome " + finalCount + " " + collector.size());
//                }
//                String parentTaxon = collector.get(genome);
//
//                // Run
//                EntityDocument genomeDocument;
//                try {
//                    genomeDocument = wbdf.getEntityDocument(genome);
//                    // Create statement
//                    EntityIdValue itemIdValue = genomeDocument.getEntityId();
//                    PropertyIdValue parentProperty = Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon").replaceAll(".*/", ""), commandOptions.wiki);
//                    ItemIdValue parentTaxonIdValue = Datamodel.makeItemIdValue(parentTaxon, commandOptions.wiki);
//                    Statement statement = StatementBuilder.forSubjectAndProperty(itemIdValue, parentProperty).withValue(parentTaxonIdValue).build();
//                    ItemIdValue genomeItemId = Datamodel.makeItemIdValue(genome, commandOptions.wiki);
//                    wbde.updateStatements(genomeItemId, Arrays.asList(statement), Collections.emptyList(), "Parent taxon added", Collections.emptyList());
//                    logger.info("Coupled " + itemIdValue.getId() + " to " + parentTaxonIdValue.getId());
//                } catch (MediaWikiApiErrorException | IOException e) {
//                    e.printStackTrace();
//                }
//            })).get();
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }
//
//        while (!customThreadPool.isQuiescent()) {
//            logger.info("Current threads active " + customThreadPool.getActiveThreadCount());
//            try {
//                SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//

    /**
     * Loads the tax dump files from NCBI
     *
     * @return
     */
    static HashMap<Integer, Taxonomy> taxDumpLoader() throws FileNotFoundException {
        logger.info("Loading taxonomy dump files");
        ;
        Scanner scanner;
        scanner = new Scanner(new File("./taxdump/nodes.dmp"));

        while (scanner.hasNextLine()) {
            Taxonomy taxonomy = new Taxonomy();
            String line = scanner.nextLine();
            String[] lineSplit = line.split("\\|");
            int taxid = Integer.parseInt(lineSplit[0].trim().strip());
            taxonomy.setTaxid(taxid); //					-- node id in GenBank taxonomy database
            taxonomy.setRank(lineSplit[2]); //       				-- rank of this node (superkingdom, kingdom, ...)
            taxonomy.setParentTaxID(Integer.parseInt(lineSplit[1].trim().strip())); //             -- parent node id in GenBank taxonomy database
//                taxonomy.setEMBLCode(lineSplit[3]); //      			-- locus-name prefix; not unique
//                taxonomy.setDivisionID(lineSplit[4]); //				-- see division.dmp file
//                taxonomy.setInheritedDIVFlag(lineSplit[5]); //  (1 or 0)		-- 1 if node inherits division from parent
//                taxonomy.setGeneticCodeID(lineSplit[6]); //			-- see gencode.dmp file
//                taxonomy.setInheritedGCFlag(lineSplit[7]); //  (1 or 0)		-- 1 if node inherits genetic code from parent
//                taxonomy.setMitochondrialGeneticCodeID(lineSplit[8]); //		-- see gencode.dmp file
//                taxonomy.setInheritedMGCFlag(lineSplit[9]); //  (1 or 0)		-- 1 if node inherits mitochondrial gencode from parent
//                taxonomy.setGenBankHiddenFlag(lineSplit[10]); // (1 or 0)            -- 1 if name is suppressed in GenBank entry lineage
//                taxonomy.setHiddenSubtreeRootFlag(lineSplit[11]); // (1 or 0)       -- 1 if this subtree has no sequence data yet
//                taxonomy.setComments(lineSplit[12]); //				-- free-text comments and citations
            taxonomies.put(taxid, taxonomy);
            if (taxonomies.size() % 1000000 == 0) {
                logger.info("Processed " + taxonomies.size() + " taxonomies");
            }
        }
        scanner.close();

        // Parse the names document
        scanner = new Scanner(new File("./taxdump/names.dmp"));
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.contains("scientific name")) continue;
            String[] lineSplit = line.split(" *\\| *");
            int taxid = Integer.parseInt(lineSplit[0].trim().strip());
            Taxonomy taxonomy = taxonomies.get(taxid);
            taxonomy.setScientificName(lineSplit[1].strip().trim());
        }

        // Get all Q pages for all the taxa by SPARQL?
        updateAllTaxa();

        return taxonomies;
    }

    /**
     * Creates the lineage for a genome object based on taxon id using the NCBI dumps
     *
     * @param itemDocument
     */
    public static void createLineage(ItemDocument itemDocument) throws MediaWikiApiErrorException {
        Statement statement = itemDocument.findStatement(propertyLookup.get("NCBI taxonomy ID"));
        int taxonid = Integer.parseInt(statement.getValue().toString().replaceAll("\"", ""));

        // Create the pages?...
        if (taxonid > 0) {
            // Obtain taxa
            List<String> taxons = new ArrayList<>();
            // Start taxon id and now we chain it up all the way to the root
            taxons.add(String.valueOf(taxonid));
            // Obtain all parents and fill the array
            while (true) {
                if (!taxonomies.containsKey(taxonid)) {
                    System.err.println("Missing?");
                    break;
                }
                Taxonomy taxonomy = taxonomies.get(taxonid);
                logger.debug("Taxon id: " + taxonid + " and scientific name " + taxonomy.getScientificName());
                taxons.add(String.valueOf(taxonomy.getParentTaxID())); // nul pointer?
                taxonid = taxonomy.getParentTaxID();
                if (taxonomy.getTaxid() == taxonomy.getParentTaxID())
                    break;
            }
            // Reverse list to go from root >>> strain
            taxons = Lists.reverse(taxons);
            logger.debug("Taxa: " + itemDocument.getLabels().get("en") + " " + StringUtils.join(taxons, " "));
//            ItemDocument childDocument = null;
            // List of taxa, replace all by Q identifier?
            for (int i = 0; i < taxons.size(); i++) {
                // Obtain Q page or create it if it does not exist
                Taxonomy taxonomy = taxonomies.get(Integer.parseInt(taxons.get(i)));
                taxons.set(i, taxonomy.getWikiBaseItemId());
            }

            // Create all non Q pages and link them
            for (int i = 0; i < taxons.size(); i++) {
                String taxon = taxons.get(i);
                // Check if this is still a numeric value meaning there is no taxa page
                Taxonomy childTaxon = taxonomies.get(taxons.get(i));
                if (StringUtils.isNumeric(taxon)) {
                    try {
                        if (i == 0) {
                            String childPageID = createLineagePage(null, childTaxon).getEntityId().getId();
                            // Update page entry
                            childTaxon.setWikiBaseItemId(childPageID);
                            taxons.set(i, childPageID);
                        } else {
                            String parentPageID = taxons.get(i - 1);
                            createLineagePage(parentPageID, childTaxon);
                        }
                    } catch (IOException | ExecutionException | InterruptedException e) {
                        logger.error("Failure on taxonomy: " + e.getMessage());
                    }
                }
            }
//
//            System.err.println("Lineage " + StringUtils.join(taxons, " "));
//
//
//            for (int i = 0; i < taxons.size(); i++) {
//
//
//                try {
//                    Integer childTaxonId = Integer.valueOf(taxons.get(i));
//                    // Skip if entry has parent
//                    if (hasParent.contains(childTaxonId)) {
//                        continue;
//                    }
//
//                    // Check taxid...
//                    if (taxonomies.get(childTaxonId) != null) {
//                        // If the document exists it should already have a parent coupling?
//                        String label = taxonomies.get(childTaxonId).getScientificName() + " (" + taxonomies.get(childTaxonId).getTaxid() + ")";
//                        childDocument = checkPageByLabel(label, false);
//                        // Check if it has a parent value which it should except for the root which is taxon 1
//                        if (childDocument == null && i != 0) {
//                            // Take one step back and create it
//                            logger.error("Take one step back!?");
//                            i = i - 1;
//                        } else if (childTaxonId != 1 && childDocument.findStatement(propertyLookup.get("parent taxon")) == null) {
//                            // No parent? should be there
//                            System.err.println("No parent detected for " + childDocument.getEntityId());
//                        } else {
//                            // Add to finished list?
//                            hasParent.add(childTaxonId);
//                            continue;
//                        }
//                    }
//
//                    if (i == 0) {
//                        // First element has no parent thus child needs to be created which becomes the parent in the next iteration
//                        Taxonomy childTaxon = taxonomies.get(taxonid);
//                        if (childTaxon != null) {
//                            createLineagePage(null, childTaxon);
//                        } else {
//                            logger.error("Creating lineage failed");
//                            logger.error("No taxon info in taxonomies object for " + taxonid);
//                            logger.error("Taxa: " + itemDocument.getLabels().get("en") + " " + StringUtils.join(taxons, " "));
//                            return;
//                        }
//                    } else if (i + 1 < taxons.size()) {
//                        // Equal taxon values due to whatever reason are ignored
//                        if (taxons.get(i).equals(taxons.get(i + 1))) continue;
//                        // Check document taxon of child equal to parent
//                        if (childDocument == null) {
//                            // System.err.println("null??");
//                            // System.err.println(itemDocument.getEntityId().getId() + " has document which is null for " + taxid + " " + taxonomies.get(taxid).getScientificName());
//                        } else if (childDocument.findStatement(propertyLookup.get("NCBI taxonomy ID")) != null) {
//                            int testTaxon = Integer.parseInt(childDocument.findStatement(propertyLookup.get("NCBI taxonomy ID")).getValue().toString().replaceAll("\"", ""));
//                            System.err.println("Test taxon:" + testTaxon + " and " + taxons.get(i));
//                        } else {
//                            System.err.println("No taxonomy ID for " + childDocument.getEntityId().getId());
//                        }
//                        // First element is the parent
//                        Taxonomy parentTaxon = taxonomies.get(taxons.get(i));
//                        // Second element is the child
//                        Taxonomy childTaxon = taxonomies.get(taxons.get(i + 1));
//                        // Couple them together
//                        createLineagePage(parentTaxon, childTaxon);
//                    }
//                } catch (ExecutionException | InterruptedException | IOException e) {
//                    System.err.println(">" + e);
//                    throw new RuntimeException(e);
//                }
//            }
        } else {
            System.err.println("No taxon obtained");
        }
    }

    private static ItemDocument createLineagePage(String parentPageId, Taxonomy childTaxon) throws IOException, ExecutionException, InterruptedException, MediaWikiApiErrorException {
        if (finishedTaxa.containsKey(childTaxon.getTaxid())) {
//            logger.error("null... Taxon already processed");
//            ItemDocument childDocument = (ItemDocument) wbdf.getEntityDocument(finishedTaxa.get(childTaxon.getTaxid()));
            return null; // childDocument;
        }

        if (parentPageId != null)
            logger.info("Creating lineage page with parent " + parentPageId + " and child " + childTaxon.getTaxid() + " with taxon lookup size of " + taxonomies.size());
        else
            logger.info("Creating lineage page of taxon " + childTaxon.getTaxid() + " with taxon lookup size of " + taxonomies.size());

        // For each level create the page and link them?
        Taxonomy taxon = taxonomies.get(childTaxon.getTaxid());

        String childLabel = childTaxon.getScientificName() + " (" + childTaxon.getTaxid() + ")";
        ItemDocument childDocument = checkPageByLabel(childLabel, false);

        // Create item if it does not exist
        if (childDocument == null) {
//            ItemDocument parentPage = null;
//            if (parentTaxon != null) {
//                String parentLabel = parentTaxon.getScientificName() + " (" + parentTaxon.getTaxid() + ")";
//                parentPage = checkPageByLabel(parentLabel, true);
//                logger.debug(">>>>FOUND parent page: " + parentPage.getEntityId().getId());
//            }

            // Create taxon entry
            int taxid = childTaxon.getTaxid();
            String scientificName = childTaxon.getScientificName();

            // Fixing naming issues with taxon ranks
            String rank = childTaxon.getRank().toString().toLowerCase().replaceAll("_", " ").
                    replaceAll("serogroup", "serotype").
                    replaceAll("species subgroup", "subspecies").
                    replaceAll("varietas", "variety").
                    replaceAll("^forma$", "forma specialis");

            String rankID = wdPageLookup.get(rank);

            if (rankID == null) {
                logger.error("Create rank page!...  " + rank + " " + rankID);
                return childDocument;
            } else {
                logger.info("Creating lineage " + rank + " " + scientificName);
            }

            ItemDocumentBuilder itemDocumentBuilder = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).withLabel(scientificName + " (" + taxid + ")", "en").withDescription(StringUtils.capitalize(taxon.getRank().name().toLowerCase()), "en");

            // NCBI Taxonomy statement
            Statement ncbiTaxonomyIdStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("NCBI taxonomy ID").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeStringValue(String.valueOf(taxid))).build();
            itemDocumentBuilder = itemDocumentBuilder.withStatement(ncbiTaxonomyIdStatement);
            // Taxon name statement
            Statement strainNameStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("taxon name").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeStringValue(String.valueOf(taxon.getScientificName()))).build();
            itemDocumentBuilder = itemDocumentBuilder.withStatement(strainNameStatement);
            // instance of statement
            Statement instanceOfStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("instance of").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeItemIdValue(wdPageLookup.get("taxon"), commandOptions.wiki)).build();
            itemDocumentBuilder = itemDocumentBuilder.withStatement(instanceOfStatement);
            // taxon rank statement
            Statement rankStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("taxon rank").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeItemIdValue(rankID, commandOptions.wiki)).build();
            itemDocumentBuilder = itemDocumentBuilder.withStatement(rankStatement);
            // parent taxon statement
//            if (parentTaxon.getTaxid() > 0) {
//                Statement parentStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon ID").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeStringValue(parentTaxon.getTaxid() + "")).build();
//                itemDocumentBuilder = itemDocumentBuilder.withStatement(parentStatement);
//            }
            if (parentPageId != null) {
                Statement parentStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, Datamodel.makePropertyIdValue(propertyLookup.get("parent taxon").replaceAll(".*/", ""), commandOptions.wiki)).withValue(Datamodel.makeItemIdValue(parentPageId.replaceAll(".*/", ""), commandOptions.wiki)).build();
                itemDocumentBuilder = itemDocumentBuilder.withStatement(parentStatement);
            }
            // Build the document
            ItemDocument itemDocument = itemDocumentBuilder.build();

            try {
                childDocument = wbde.createItemDocument(itemDocument, "Parent taxon linking", null);
                logger.info("Document created " + childDocument.getEntityId().getId());
            } catch (IOException | MediaWikiApiErrorException e) {
                logger.error(e.getMessage());
                if (e.getMessage().contains("already has label")) {
                    String id = e.getMessage().split("Item:Q")[1].split("\\|")[0];
                    ItemIdValue itemIdValue = Datamodel.makeWikidataItemIdValue("Q" + id);
                    childDocument = (ItemDocument) wbdf.getEntityDocument(itemIdValue.getId());
                }
                logger.error(e.getMessage()); // e.printStackTrace();
            }
        }
        // Add to finished taxa
        finishedTaxa.put(taxon.getTaxid(), childDocument.getEntityId().getId());
        return childDocument;
    }

    public static Taxonomy getTaxa(int taxon) {
        if (taxonLookup.containsKey(taxon))
            return taxonLookup.get(taxon);

        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?item ?label \n" +
                "WHERE {\n" +
                "  ?item wdt:" + propertyLookup.get("NCBI taxonomy ID") + " \"" + taxon + "\" .\n" +
                "  ?item wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                "  ?item wdt:" + propertyLookup.get("taxon rank") + " ?rank .\n" +
                "  ?rank rdfs:label ?label ." +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        TupleQueryResult result = query.evaluate();
        if (result.hasNext()) {
            BindingSet bindingSet = result.next();
            String itemPage = bindingSet.getValue("item").stringValue().replaceAll(".*/", "");
            String rankLabel = bindingSet.getValue("label").stringValue();
            // Taxon object
            Taxonomy taxonomy = new Taxonomy();
            taxonomy.setWikiBaseItemId(itemPage);
            taxonomy.setRank(rankLabel);
            taxonLookup.put(taxon, taxonomy);
            if (result.hasNext())
                System.err.println("Multiple taxa hits detected...");
        }
        // Flush results
        while(result.hasNext())
            result.next();

        result.close();
        repo.getConnection().close();

        return taxonLookup.get(taxon);
    }

    private static void updateAllTaxa() {
        // Get parent page id
        // Get taxon id
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?item ?taxon\n" +
                "WHERE {\n" +
                "  ?item wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxon .\n" +
                "  ?item wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        TupleQueryResult result = query.evaluate();

        while (result.hasNext()) {
            BindingSet bindingSet = result.next();
            org.eclipse.rdf4j.model.Value itemValue = bindingSet.getValue("item");
            org.eclipse.rdf4j.model.Value taxonValue = bindingSet.getValue("taxon");
            Integer taxonID = Integer.valueOf(taxonValue.stringValue());


            Taxonomy taxonomy = taxonomies.get(taxonID);
            if (taxonomy == null)
                System.err.println("Deprecated? for " + taxonID + "\t" + itemValue.stringValue());
            else
                taxonomy.setWikiBaseItemId(itemValue.stringValue());

        }
    }
    private static String getPageByTaxID(int parentTaxonID) {
        // Get parent page id
        logger.info("Get page for " + parentTaxonID);
        // Get taxon id
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?parent\n" +
                "WHERE {\n" +
                " VALUES ?parentID {\"" + parentTaxonID + "\"}\n" +
                "  ?parent wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?parentID .\n" +
                "  ?parent wdt:" + propertyLookup.get("instance of") + " wd:" + wdPageLookup.get("taxon") + " .\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);
        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        TupleQueryResult result = query.evaluate();

        while (!result.hasNext()) {
            logger.info("No page found for " + parentTaxonID);
            try {
                SECONDS.sleep(1);
                result = query.evaluate();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return result.next().getValue("parent").stringValue();
    }

    static ItemDocument checkPageByLabel(String label, boolean forceCheck) throws IOException, MediaWikiApiErrorException, InterruptedException {
        List<WbSearchEntitiesResult> wbSearchEntitiesResults = wbdf.searchEntities(label);
        for (WbSearchEntitiesResult wbSearchEntitiesResult : wbSearchEntitiesResults) {
            logger.debug(wbSearchEntitiesResult.getLabel());
        }

        if (wbSearchEntitiesResults.size() == 1) {
            WbSearchEntitiesResult wbSearchEntitiesResult = wbSearchEntitiesResults.get(0);
            String entityId = wbSearchEntitiesResult.getEntityId();
            ItemIdValue itemIdValue = Datamodel.makeWikidataItemIdValue(entityId);
            return (ItemDocument) wbdf.getEntityDocument(itemIdValue.getId());
        } else if (wbSearchEntitiesResults.size() > 1) {
            // Multi threading check failure?
            int qValue = Integer.parseInt(wbSearchEntitiesResults.get(0).getEntityId().replaceAll("Q",""));
            System.err.println("Value now " + qValue);
            for (WbSearchEntitiesResult wbSearchEntitiesResult : wbSearchEntitiesResults) {
                logger.error("Multiple hits found " + wbSearchEntitiesResult.getEntityId() + " " + wbSearchEntitiesResult.getLabel());
                int qValue2 = Integer.parseInt(wbSearchEntitiesResult.getEntityId().replaceAll("Q", ""));
                if (qValue2 < qValue)
                    qValue = qValue2;
            }
            ItemIdValue itemIdValue = Datamodel.makeWikidataItemIdValue("Q" + qValue);
            logger.error("Multiple hits found, returning " + itemIdValue.getId());
            return (ItemDocument) wbdf.getEntityDocument(itemIdValue.getId());
        } else {
            if (!forceCheck)
                return null;
            else {
                // It should be there... lets sleep on it and try again?
                ItemDocument itemDocument = null;
                int counter = 0;
                while (itemDocument == null) {
                    counter++;
                    if (counter > 10)
                        logger.info("Force check running for " + counter + " iterations using taxon " + label);

                    SECONDS.sleep(1);
                    itemDocument = checkPageByLabel(label, false);
                }
                return itemDocument;
            }
        }
    }

    private HashMap<String, String> getTaxonInstances() {
        HashMap<String, String> taxonInstances = new HashMap<>();

        // Get taxon id
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?item ?taxonid\n" +
                "WHERE {\n" +
                "?item wdt:" + propertyLookup.get("instance of") + " \"" + wdPageLookup.get("taxon") + "\".\n" +
                "?item wdt:" + propertyLookup.get("NCBI taxonomy ID") + " ?taxonid . " +
                "}";

        SPARQLRepository repo = new SPARQLRepository(commandOptions.endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);

        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet bindingSet = result.next();
                String item = bindingSet.getValue("item").stringValue();
                String taxonid = bindingSet.getValue("taxonid").stringValue();
                taxonInstances.put(taxonid, item);
            }
        }
        return taxonInstances;
    }
//
//    private static HashMap<Taxonomy.Rank, Taxonomy> getLineage(String endpoint, String taxonid) {
////        ArrayList<String> lineageList = new ArrayList<>();
////
////        lineage.add("Superkingdom");
////        lineage.add("Phylum");
////        lineage.add("Class");
////        lineage.add("Order");
////        lineage.add("Family");
////        lineage.add("Genus");
////        lineage.add("Species");
//
//        String queryString = "" + App.PREFIX +
//                "PREFIX up:<http://purl.uniprot.org/core/> P" +
//                "REFIX taxon:<http://purl.uniprot.org/taxonomy/> " +
//                "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> " +
//                "SELECT DISTINCT ?parentClass ?rank ?className " +
//                "WHERE" + " {" +
//                " VALUES ?taxon{taxon:" + taxonid + "}" +
//                "?taxon up:scientificName ?name." +
//                "{" +
//                "  ?taxon up:rank ?rank ." +
//                "       ?taxon up:scientificName ?className ." +
//                "       BIND(?taxon AS ?parentClass)" + "   }" +
//                "   union" + "   {"
//                + "  ?taxon rdfs:subClassOf+ ?parentClass ." +
//                "       ?parentClass up:rank ?rank ." +
//                "       ?parentClass up:scientificName ?className ." +
//                "   }" + "}";
//
//        System.err.println("Query: " + queryString);
//
//        SPARQLRepository repo = new SPARQLRepository(endpoint);
//
//        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
//
//        try (TupleQueryResult results = query.evaluate()) {
//
//            try {
//                results.hasNext();
//            } catch (NullPointerException e) {
//                System.err.println("Nullpointer for results with taxid: " + taxonid);
//                return null;
//            }
//
//            if (!results.hasNext()) {
//                System.err.println("No results for " + taxonid);
//            }
//
//            HashMap<Taxonomy.Rank, Taxonomy> lineageHashMap = new HashMap<>();
//            while (results.hasNext()) {
//                BindingSet result = results.next();
//                String rank = result.getValue("rank").stringValue();
//                String name = result.getValue("className").stringValue();
//                int taxid = Integer.parseInt(result.getValue("parentClass").stringValue().replaceAll(".*/", ""));
//
//                Taxonomy taxon = new Taxonomy();
//                taxon.setName(name);
//                taxon.setTaxid(taxid);
//
//                switch (rank) {
//                    case "Superkingdom":
//                        taxon.setRank(Taxonomy.Rank.SUPERKINGDOM);
//                        break;
//                    case "Phylum":
//                        taxon.setRank(Taxonomy.Rank.PHYLUM);
//                        break;
//                    case "Class":
//                        taxon.setRank(Taxonomy.Rank.CLASS);
//                        break;
//                    case "Order":
//                        taxon.setRank(Taxonomy.Rank.ORDER);
//                        break;
//                    case "Family":
//                        taxon.setRank(Taxonomy.Rank.FAMILY);
//                        break;
//                    case "Genus":
//                        taxon.setRank(Taxonomy.Rank.GENUS);
//                        break;
//                    case "Species":
//                        taxon.setRank(Taxonomy.Rank.SPECIES);
//                        break;
//                }
//                lineageHashMap.put(taxon.getRank(), taxon);
//            }
//            return lineageHashMap;
//        }
//    }
}
