package nl.munlock.wiki.sync.wikibase;

import nl.munlock.wiki.App;
import nl.munlock.wiki.CommandOptions;
import nl.munlock.wiki.objects.WDPage;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikidata.wdtk.datamodel.helpers.Datamodel;
import org.wikidata.wdtk.datamodel.helpers.ItemDocumentBuilder;
import org.wikidata.wdtk.datamodel.helpers.PropertyDocumentBuilder;
import org.wikidata.wdtk.datamodel.helpers.StatementBuilder;
import org.wikidata.wdtk.datamodel.interfaces.*;
import org.wikidata.wdtk.wikibaseapi.apierrors.MediaWikiApiErrorException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static nl.munlock.wiki.wikibase.General.*;
import static org.wikidata.wdtk.datamodel.interfaces.DatatypeIdValue.*;

public class Synchronise {
    private static Logger logger = LoggerFactory.getLogger(Synchronise.class);

    public static void sync(CommandOptions commandOptions) throws IOException, InterruptedException {
        // Ensure that reference URL is present
        // Property sync
        syncProperties(commandOptions);
        // Item sync
        syncItems(commandOptions);
    }

    private static void syncItems(CommandOptions commandOptions) throws IOException, InterruptedException {

        // Create special WikiBasePropertyValue page for item value pages
        if (getWikiBasePropertyValue(commandOptions.endpoint) == null) {
            WDPage wdPage = new WDPage();
            wdPage.setLabel("WikiBasePropertyValue");
            wdPage.setDescription("A wikibase item that is created to standardise property values");

            ItemDocument itemDocument = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                    withLabel(wdPage.getLabel(), "en").
                    withDescription(wdPage.getDescription(), "en").
                    build();
            try {
                itemDocument = App.wbde.createItemDocument(itemDocument, "Item synchronisation", null);
                logger.debug(itemDocument.getEntityId().getId() + " successfully added.");
            } catch (IOException | MediaWikiApiErrorException e) {
                e.printStackTrace();
            }
            // Sleep so sparql endpoint can update
            TimeUnit.SECONDS.sleep(1);
        }

        wdPageLookup.put("WikiBasePropertyValue", getWikiBasePropertyValue(commandOptions.endpoint));

        // Create wikidata items locally
        Set<WDPage> wdItems = getWikiDataDocuments();
        System.err.println("WDITEMS: " + wdItems.size());
        for (WDPage wdItem : wdItems) {
            System.err.println(">>>" + wdItem.getLabel());
            createItem(wdItem);
        }
        // Create wikibase items from file
        InputStream in = Synchronise.class.getResourceAsStream("/wikibase/items");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        wdItems = new HashSet<>();
        while ((line = reader.readLine()) != null) {
            if (line.startsWith("#")) continue;
            String[] lineSplit = line.split("\t");
            WDPage wdPage = new WDPage();
            wdPage.setLabel(lineSplit[0]);
            wdPage.setDescription(lineSplit[1]);
            wdItems.add(wdPage);
        }

        wdItems.parallelStream().forEach(wdItem -> {
            if (wdItem != null) {
                if (!wdPageLookup.containsKey(wdItem.getLabel()))
                    createItem(wdItem);
            }
        });
    }

    private static String getWikiBasePropertyValue(String endpoint) {
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?item\n" +
                "WHERE {  \n" +
                "  ?item rdfs:label \"WikiBasePropertyValue\"@en\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository(endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            BindingSet solution = result.next();
            return solution.getValue("item").stringValue().replaceAll(".*/Q", "Q");
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    private Collection<? extends WDPage> getWBDocuments() {
        return null;
    }

    private static void syncProperties(CommandOptions commandOptions) throws IOException, InterruptedException {
        // sync wikibase properties
        getWikiBaseProperties(commandOptions.endpoint);

//        System.err.println(propertyLookup.size() + " are present in the wiki environment");
//        System.err.println(StringUtils.join(propertyLookup.keySet(),"\n"));
        ArrayList<String> propertyNames = new ArrayList<>();

        // Check for reference URL
        if (!propertyLookup.containsKey("reference URL")) {
            propertyNames.add("reference URL");
            WDPage wdPage = getWikiDataDocument("P854");
            createReferenceURLProperty(wdPage);
            // Check if properties exists otherwise sleep and wait till synced
            checkProperties(commandOptions, propertyNames);
        }

        if (!propertyLookup.containsKey("formatter URL")) {
            propertyNames.add("formatter URL");
            WDPage wdPage = getWikiDataDocument("P1630");
            createProperty(wdPage);
        }

        // Check if properties exists otherwise sleep and wait till synced
        checkProperties(commandOptions, propertyNames);

        // stream properties that are stored in WIKIDATA property file in resource
        InputStream in = Synchronise.class.getResourceAsStream("/wikidata/properties");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;

        // Line should start with a Pxxx identifier
        while ((line = reader.readLine()) != null) {
            String itemID = line.split(" ")[0];
            if (itemID.matches("P[0-9]+")) {
                // else get from WD and create property
                WDPage wdPage = getWikiDataDocument(itemID);
                if (wdPage == null) {
                    logger.info(line + " already in wikibase instance");
                    continue;
                }
                propertyNames.add(wdPage.getLabel());
                createProperty(wdPage);
            }
        }

        // Stream properties that do not exists in wikidata and only in local file
        in = Synchronise.class.getResourceAsStream("/wikibase/properties");
        reader = new BufferedReader(new InputStreamReader(in));
        // HashMap<String, String> x = propertyLookup;
        while ((line = reader.readLine()) != null) {
            if (!line.contains("\t")) {
                System.err.println("Line does not contain tab separator: " + line);
            }
            String[] lineSplit = line.split("\t");
            String label = lineSplit[0];
            if (propertyLookup.containsKey(label)) {
                logger.info(label + " property already in wikibase");
                continue;
            }
            System.err.println(line);
            String description = lineSplit[1];
            String type = lineSplit[2];

            WDPage wdPage = new WDPage();

            if (line.contains("formatter URL")) {
                for (String element : lineSplit) {
                    if (element.contains("=")) {
                        wdPage.addStatement(element.split("=")[0],element.split("=")[1]);
                    }
                }
            }

            wdPage.setLabel(label);
            wdPage.setDescription(description);
            if (type.matches("DT_ITEM")) {
                wdPage.setType(DT_ITEM);
            } else if (type.matches("DT_EXTERNAL_ID")) {
                wdPage.setType(DT_EXTERNAL_ID);
            } else if (type.matches("DT_STRING")) {
                wdPage.setType(DT_STRING);
            } else {
                throw new IOException("Unknown type of " + type);
            }
            propertyNames.add(wdPage.getLabel());
            createProperty(wdPage);
        }


        // final sync of properties
        checkProperties(commandOptions, propertyNames);
    }

    private static void checkProperties(CommandOptions commandOptions, ArrayList<String> propertyNames) throws InterruptedException, IOException {
        while (true) {
            // assume all elements are present
            boolean present = true;
            for (String propertyName : propertyNames) {
                if (!propertyLookup.containsKey(propertyName)) {
                    // If property not in lookup list get new lookup list after for loop
                    present = false;
                    System.err.println("Missing property " + propertyName);
                    break;
                }
            }
            // Check if all still present then break
            if (present)
                break;
            TimeUnit.SECONDS.sleep(1);
            getWikiBaseProperties(commandOptions.endpoint);
        }
    }

    private static void createProperty(WDPage wdPage) {

        // Base document builder
        PropertyDocumentBuilder propertyDocumentBuilder = PropertyDocumentBuilder.forPropertyIdAndDatatype(PropertyIdValue.NULL, wdPage.getType()).
                withLabel(wdPage.getLabel(), "en").
                withDescription(wdPage.getDescription(), "en");

        if (wdPage.getWikiDataURL() != null) {
            PropertyIdValue referenceUrl = Datamodel.makePropertyIdValue(propertyLookup.get("reference URL").replaceAll(".*/", ""), App.commandOptions.wiki);
            StringValue stringValue = Datamodel.makeStringValue(wdPage.getWikiDataURL());

            // Add reference url statement
            Statement statement = StatementBuilder.forSubjectAndProperty(PropertyIdValue.NULL, referenceUrl).withValue(stringValue).build();

            // Create property document with variable DataType
            propertyDocumentBuilder = propertyDocumentBuilder.withStatement(statement);

        }

        // Add statements
//        ArrayList<Statement> statements = new ArrayList<>();
        for (String key : wdPage.getStatement().keySet()) {
            if (key.contains("formatter URL")) {
                String propertyID = propertyLookup.get(key).replaceAll(".*/", "");
                PropertyIdValue formatterURLProperty = Datamodel.makePropertyIdValue(propertyID, App.commandOptions.wiki);
                StringValue formatterURLValue = Datamodel.makeStringValue(wdPage.getStatement().get(key));
                Statement statement = StatementBuilder.forSubjectAndProperty(PropertyIdValue.NULL, formatterURLProperty).withValue(formatterURLValue).build();
                propertyDocumentBuilder = propertyDocumentBuilder.withStatement(statement);
            } else {
                logger.error("Property " + key + " not captured");
            }
        }

        // Build document
        PropertyDocument propertyDocument = propertyDocumentBuilder.build();

        try {
            propertyDocument = App.wbde.createPropertyDocument(propertyDocument, "document synchronisation", null);
            logger.debug(propertyDocument + " successfully added..");
        } catch (IOException | MediaWikiApiErrorException e) {
            // e.printStackTrace();
        }
    }

    private static void createItem(WDPage wdItem) {
        // Create item if it does not exists...
        ItemDocument itemDocument;

        PropertyIdValue instanceOf = Datamodel.makePropertyIdValue(propertyLookup.get("instance of").replaceAll(".*/", ""), App.commandOptions.wiki);
        ItemIdValue itemIdValue = Datamodel.makeItemIdValue(wdPageLookup.get("WikiBasePropertyValue"), App.commandOptions.wiki);
        Statement wikiBasePropertyValueStatement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, instanceOf).
                withValue(itemIdValue).
                build();

        // Without wikidata reference indicating a local item page
        if (wdItem.getWikiDataURL() == null) {
            itemDocument = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                    withLabel(wdItem.getLabel(), "en").
                    withDescription(wdItem.getDescription(), "en").
                    withStatement(wikiBasePropertyValueStatement).
                    build();
        } else {
            PropertyIdValue referenceUrl = Datamodel.makePropertyIdValue(propertyLookup.get("reference URL").replaceAll(".*/", ""), App.commandOptions.wiki);
            StringValue stringValue = Datamodel.makeStringValue(wdItem.getWikiDataURL());
            Statement statement = StatementBuilder.forSubjectAndProperty(ItemIdValue.NULL, referenceUrl).
                    withValue(stringValue).
                    build();

            itemDocument = ItemDocumentBuilder.forItemId(ItemIdValue.NULL).
                    withLabel(wdItem.getLabel(), "en").
                    withDescription(wdItem.getDescription(), "en").
                    withStatement(statement).withStatement(wikiBasePropertyValueStatement).
                    build();
        }

        try {
            itemDocument = App.wbde.createItemDocument(itemDocument, "Item synchronisation", null);
            logger.info(itemDocument.getEntityId().getId() + " " + wdItem.getLabel() + " successfully added...");
        } catch (IOException | MediaWikiApiErrorException e) {
            if (!e.getMessage().contains("already has label"))
                e.printStackTrace();
        }
    }

    private static Set<WDPage> getWikiDataDocuments() throws IOException {
        URL url = Synchronise.class.getResource("/wikidata/items");
        logger.info("Parsing " + url.getFile());
        InputStream in = url.openStream(); //getClass().getResourceAsStream("/wikidata/items");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        Set<WDPage> wdPageSet = new HashSet<>();
        while ((line = reader.readLine()) != null) {
            String itemID = line.split(" ")[0];
            if (itemID.matches("Q[0-9]+")) {
                WDPage wdPage = getWikiDataDocument(itemID);
                if (wdPage == null) {
                 logger.info(line + " already in wikibase");
                } else {
                    wdPageSet.add(wdPage);
                }
            }
        }
        return wdPageSet;
    }

    /**
     * Obtains WDPage from wikidata using properties (P) or items (Q).
     *
     * @param item
     * @return
     */
    private static WDPage getWikiDataDocument(String item) {

        // Parse document with additional items from WD
        String queryString = "" + App.PREFIX +
                "SELECT ?item ?description ?label ?propType ?formatterURL" +
                "  WHERE {\n" +
                "  VALUES ?item { wd:" + item + " } \n" +
                "  ?item rdfs:label ?label .\n" +
                "  ?item schema:description ?description .\n" +
                "  OPTIONAL { ?item wikibase:propertyType ?propType . }\n" +
                "  OPTIONAL { ?item wdt:P1630 ?formatterURL }\n"+
                "  FILTER (lang(?label) = 'en')\n" +
                "  FILTER (lang(?description) = 'en')\n" +
                "}";

        SPARQLRepository repo = new SPARQLRepository("http://query.wikidata.org/sparql");

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet solution = result.next();
                WDPage wdPage = new WDPage();
                wdPage.setLabel(solution.getValue("label").stringValue());
                wdPage.setDescription(solution.getValue("description").stringValue());
                wdPage.setWikiDataURL(solution.getValue("item").stringValue());
                if (solution.getValue("formatterURL") != null)
                    wdPage.addStatement("formatter URL", solution.getValue("formatterURL").stringValue());
                if (solution.getValue("propType") != null) {
                    String type = solution.getValue("propType").stringValue().replaceAll(".*#", "");
                    if (type.matches("ExternalId")) {
                        wdPage.setType(DT_EXTERNAL_ID);
                    } else if (type.matches("Url")) {
                        wdPage.setType(DT_URL);
                    } else if (type.matches("Quantity")) {
                        wdPage.setType(DT_QUANTITY);
                    } else if (type.matches("WikibaseItem")) {
                        wdPage.setType(DT_ITEM);
                    } else if (type.matches("String")) {
                        wdPage.setType(DT_STRING);
                    } else {
                        throw new IllegalArgumentException("Type " + type + " not captured");
                    }
                }
                // URL matching of Pxxx identifier with WikiBase properties and the corresponding WikiData URL
                if (!wikiDataURLs.contains(wdPage.getWikiDataURL())) {
                    return wdPage;
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    private static void createReferenceURLProperty(WDPage wdPage) {
        // Create property document
        PropertyDocument propertyDocument = PropertyDocumentBuilder.forPropertyIdAndDatatype(PropertyIdValue.NULL, wdPage.getType()).
                withLabel(wdPage.getLabel(), "en").
                withDescription(wdPage.getDescription(), "en").
                build();

        try {
            propertyDocument = App.wbde.createPropertyDocument(propertyDocument, "document synchronisation", null);
            logger.debug(propertyDocument + " successfully added....");
        } catch (IOException | MediaWikiApiErrorException e) {
            //
        }
    }
}
