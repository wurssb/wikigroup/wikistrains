package nl.munlock.wiki.wikibase;

import nl.munlock.wiki.App;
import nl.munlock.wiki.objects.WDPage;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class General {
    public static Set<String> wikiDataURLs = new HashSet<>();
    public static HashMap<String, String> wdPageLookup = new HashMap<>();
    public static HashMap<String, String> propertyLookup = new HashMap<>();
    private static Logger logger = LoggerFactory.getLogger(General.class);

    public static Set<WDPage> getLocalItems(String endpoint) {
        Set<WDPage> wdPageSet = new HashSet<>();

        // Obtain all items with a WD url
        String queryString = "" + App.PREFIX +
                "SELECT DISTINCT ?page ?pageLabel ?pageDescription ?reference\n" +
                "WHERE { \n" +
                "  ?page  schema:description ?pageDescription ; \n" +
                "          rdfs:label ?pageLabel . \n" +
                "  MINUS { ?page  wikibase:propertyType ?propType . }\n" +
                "  FILTER (lang(?pageLabel) = 'en') \n" +
                "  FILTER (lang(?pageDescription) = 'en') \n" +
                "    { ?page wdt:P1 ?reference . } UNION { ?page wdt:"+propertyLookup.get("instance of")+"/rdfs:label \"WikiBasePropertyValue\"@en } .\n" +
                "}";


        SPARQLRepository repo = new SPARQLRepository(endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet solution = result.next();
                // ... and print out the value of the variable binding for ?s and ?n
                WDPage wdPage = new WDPage();
                wdPage.setURL(solution.getValue("page").stringValue());
                wdPage.setLabel(solution.getValue("pageLabel").stringValue());
                wdPage.setDescription(solution.getValue("pageDescription").stringValue());
                if (solution.getValue("reference") != null)
                    wdPage.setWikiDataURL(solution.getValue("reference").stringValue());
                wdPageSet.add(wdPage);
            }
        }

        // Sync properties to global object for lookups
        wdPageSet.parallelStream().forEach(property -> {
            wikiDataURLs.add(property.getWikiDataURL());
            // Ensure both casing to be implemented
            wdPageLookup.put(property.getLabel().toLowerCase(Locale.ROOT), property.getURL().replaceAll(".*/",""));
            wdPageLookup.put(property.getLabel(), property.getURL().replaceAll(".*/",""));
        });
        return wdPageSet;
    }

    /**
     * Obtains all properties from WB instance
     * @param endpoint url of wikibase SPARQL endpoint
     * @return
     */
    public static Set<WDPage> getWikiBaseProperties(String endpoint) {
        logger.info("Getting wikibase properties");

        Set<WDPage> propertiesSet = new HashSet<>();

        String queryString = "" + App.PREFIX + "\n" +
                "SELECT DISTINCT ?property ?propertyLabel ?propertyDescription ?propType ?reference \n" +
                "WHERE { \n" +
                "?property wikibase:directClaim ?p ; \n" +
                "wikibase:propertyType ?propType ; \n" +
                "schema:description ?propertyDescription ; \n" +
                "rdfs:label ?propertyLabel . \n" +
                "FILTER (lang(?propertyLabel) = 'en') \n" +
                "FILTER (lang(?propertyDescription) = 'en') \n" +
                "OPTIONAL { ?property wdt:P1 ?reference . } \n" +
                "}\n";

        // System.err.println(queryString);

        SPARQLRepository repo = new SPARQLRepository(endpoint);

        TupleQuery query = repo.getConnection().prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            // we just iterate over all solutions in the result...
            while (result.hasNext()) {
                BindingSet solution = result.next();
                // ... and print out the value of the variable binding for ?s and ?n
                WDPage property = new WDPage();
                property.setURL(solution.getValue("property").stringValue());
                property.setLabel(solution.getValue("propertyLabel").stringValue());
                property.setDescription(solution.getValue("propertyDescription").stringValue());
                property.setType(solution.getValue("propType").stringValue());
                if (solution.getValue("reference") != null)
                    property.setWikiDataURL(solution.getValue("reference").stringValue());
                propertiesSet.add(property);
            }
        }
        repo.getConnection().close();

        // Sync properties to global object for lookups
        propertiesSet.parallelStream().forEach(property -> {
            wikiDataURLs.add(property.getWikiDataURL());
            propertyLookup.put(property.getLabel(), property.getURL().replaceAll(".*/",""));
        });

        return propertiesSet;
    }
}
