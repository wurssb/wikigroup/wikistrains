package nl.munlock.wiki;

import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;

import static nl.munlock.wiki.App.commandOptions;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

public class iRODS {
    public static IRODSFileFactory fileFactory;
    public static IRODSAccessObjectFactory accessObjectFactory;
    public static IRODSFileSystem irodsFileSystem;
    public static IRODSAccount irodsAccount;

    public static void connect(String host, int port, String username, String password, String zone) throws JargonException {
        // Initialize account object
        irodsAccount = IRODSAccount.instance(host, port, username, password, "", zone, "");

        // set SSL settings
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy = CS_NEG_REQUIRE;

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        irodsFileSystem = IRODSFileSystem.instance();

        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
    }
}
