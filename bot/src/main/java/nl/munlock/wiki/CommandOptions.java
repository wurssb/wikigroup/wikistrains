package nl.munlock.wiki;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class CommandOptions {

    @Parameter(names = { "-e", "-endpoint" }, description = "Wiki endpoint")
    public String endpoint;
    @Parameter(names = { "-g", "-genome" }, description = "ENA genome sync")
    public boolean genome;

    @Parameter(names = { "-b", "-bacdive" }, description = "BacDive sync")
    public boolean bacdive;
    @Parameter(names = { "-threads" }, description = "Number of threads to use")
    public int threads = 20;


    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = { "-s", "-sync" }, description = "Wikidata property sync")
    boolean sync;

    @Parameter(names = { "-w", "-wiki" }, description = "Wiki URL", required = true)
    public
    String wiki;

    @Parameter(names = { "-u", "-username" }, description = "Wiki username", required = true)
    String username;

        @Parameter(names = "-password", description = "Wiki password", required = true)
        String password;

        @Parameter(names = "-debug", description = "Debug mode")
        private boolean debug = false;


    public CommandOptions(String[] args) {
        JCommander.newBuilder()
                .addObject(this)
                .build()
                .parse(args);
    }
}
